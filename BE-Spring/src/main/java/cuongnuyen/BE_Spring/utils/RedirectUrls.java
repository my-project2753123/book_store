package cuongnuyen.BE_Spring.utils;

import lombok.Data;

@Data
public class RedirectUrls {
    public static final String SUCCESS_URL = "http://localhost:8000/pay/success";
    public static final String CANCEL_URL = "http://localhost:8000/pay/cancel";
}
