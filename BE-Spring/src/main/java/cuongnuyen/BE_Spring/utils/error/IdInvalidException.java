package cuongnuyen.BE_Spring.utils.error;

public class IdInvalidException extends Exception {
    public IdInvalidException(String message) {
        super(message);
    }
}
