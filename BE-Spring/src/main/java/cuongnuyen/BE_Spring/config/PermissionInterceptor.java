package cuongnuyen.BE_Spring.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.HandlerMapping;

import cuongnuyen.BE_Spring.domain.EntryPoint;
import cuongnuyen.BE_Spring.domain.Permission;
import cuongnuyen.BE_Spring.domain.Role;
import cuongnuyen.BE_Spring.domain.User;
import cuongnuyen.BE_Spring.services.UserService;
import cuongnuyen.BE_Spring.utils.SecurityUntil;
import cuongnuyen.BE_Spring.utils.error.PermissionException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PermissionInterceptor implements HandlerInterceptor {
    @Autowired
    UserService userService;

    private final Logger log = LoggerFactory.getLogger(PermissionInterceptor.class);

    @Override
    @Transactional
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response, Object handler)
            throws Exception {

        String path = (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);
        String requestURI = request.getRequestURI();
        String httpMethod = request.getMethod();
        log.error(">>> RUN preHandle");
        log.error(">>> path= {}", path);
        log.error(">>> httpMethod= {}", httpMethod);
        log.error(">>> requestURI= {}", requestURI);

        // check permission
        String email = SecurityUntil.getCurrentUserLogin().isPresent() == true
                ? SecurityUntil.getCurrentUserLogin().get()
                : "";
        if (email != null && !email.isEmpty()) {
            User user = this.userService.handleFetchUserByEmail(email);
            if (user != null) {
                Role role = user.getRole();
                if (role != null) {
                    List<Permission> permissions = role.getPermissions();
                    boolean isAllow = permissions.stream()
                            .anyMatch(item -> item.getApiPath().equals(path)
                                    && item.getMethod().name().equals(httpMethod));
                    if (isAllow == false) {
                        throw new PermissionException("Bạn không có quyền truy cập endpoint này");
                    }
                    System.out.println(isAllow);
                    return true;
                }
                throw new PermissionException("Bạn không có quyền truy cập endpoint này");
            }
        }

        return true;
    }

}
