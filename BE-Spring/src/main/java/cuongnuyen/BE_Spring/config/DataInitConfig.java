package cuongnuyen.BE_Spring.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import cuongnuyen.BE_Spring.domain.Permission;
import cuongnuyen.BE_Spring.domain.Role;
import cuongnuyen.BE_Spring.domain.User;
import cuongnuyen.BE_Spring.repository.PermissionRepository;
import cuongnuyen.BE_Spring.repository.RoleRepository;
import cuongnuyen.BE_Spring.repository.UserRepository;
import cuongnuyen.BE_Spring.utils.enums.GenderEnum;
import cuongnuyen.BE_Spring.utils.enums.MethodEnum;

@Service
public class DataInitConfig implements CommandLineRunner {

        private final PermissionRepository permissionRepository;
        private final RoleRepository roleRepository;
        private final UserRepository userRepository;
        private final PasswordEncoder passwordEncoder;

        public DataInitConfig(PermissionRepository permissionRepository, RoleRepository roleRepository,
                        UserRepository userRepository, PasswordEncoder passwordEncoder) {
                this.permissionRepository = permissionRepository;
                this.roleRepository = roleRepository;
                this.userRepository = userRepository;
                this.passwordEncoder = passwordEncoder;
        }

        @Override
        public void run(String... args) throws Exception {
                System.out.println("Run here");
                long countPermissions = this.permissionRepository.count();
                long countRoles = this.roleRepository.count();
                long countUsers = this.userRepository.count();

                if (countPermissions == 0) {
                        ArrayList<Permission> arr = new ArrayList<>();
                        arr.add(new Permission("Create role", "/api/v1/roles", MethodEnum.POST, "ROLES"));
                        arr.add(new Permission("Update a role", "/api/v1/roles", MethodEnum.PUT, "ROLES"));
                        arr.add(new Permission("Delete a role", "/api/v1/roles/{id}", MethodEnum.DELETE, "ROLES"));
                        arr.add(new Permission("Get a role by id", "/api/v1/roles/{id}", MethodEnum.GET, "ROLES"));
                        arr.add(new Permission("Get roles with pagination", "/api/v1/roles", MethodEnum.GET, "ROLES"));

                        arr.add(new Permission("Create user", "/api/v1/users", MethodEnum.POST, "USERS"));
                        arr.add(new Permission("Update a user", "/api/v1/users", MethodEnum.PUT, "USERS"));
                        arr.add(new Permission("Delete a user", "/api/v1/users/{id}", MethodEnum.DELETE, "USERS"));
                        arr.add(new Permission("Get a user by id", "/api/v1/users/{id}", MethodEnum.GET, "USERS"));
                        arr.add(new Permission("Get users with pagination", "/api/v1/users", MethodEnum.GET, "USERS"));

                        arr.add(new Permission("Create permission", "/api/v1/permissions", MethodEnum.POST,
                                        "PERMISSIONS"));
                        arr.add(new Permission("Update a permission", "/api/v1/permissions", MethodEnum.PUT,
                                        "PERMISSIONS"));
                        arr.add(new Permission("Delete a permission", "/api/v1/permissions/{id}", MethodEnum.DELETE,
                                        "PERMISSIONS"));
                        arr.add(new Permission("Get a permission by id", "/api/v1/permissions/{id}", MethodEnum.GET,
                                        "PERMISSIONS"));
                        arr.add(new Permission("Get permissions with pagination", "/api/v1/permissions", MethodEnum.GET,
                                        "PERMISSIONS"));

                        arr.add(new Permission("Create author", "/api/v1/author", MethodEnum.POST, "AUTHOR"));
                        arr.add(new Permission("Update a author", "/api/v1/author", MethodEnum.PUT, "AUTHOR"));
                        arr.add(new Permission("Get a author by id", "/api/v1/author/{id}", MethodEnum.GET, "AUTHOR"));
                        arr.add(new Permission("Get author with pagination", "/api/v1/author", MethodEnum.GET,
                                        "AUTHOR"));

                        arr.add(new Permission("Create publisher", "/api/v1/publisher", MethodEnum.POST, "PUBLISHER"));
                        arr.add(new Permission("Update a publisher", "/api/v1/publisher", MethodEnum.PUT, "PUBLISHER"));
                        arr.add(new Permission("Get a publisher by id", "/api/v1/publisher/{id}", MethodEnum.GET,
                                        "PUBLISHER"));
                        arr.add(new Permission("Get publisher with pagination", "/api/v1/publisher", MethodEnum.GET,
                                        "PUBLISHER"));

                        arr.add(new Permission("Create a book", "/api/v1/books", MethodEnum.POST, "BOOKS"));
                        arr.add(new Permission("Update a book", "/api/v1/books", MethodEnum.PUT, "BOOKS"));
                        arr.add(new Permission("Get a book by id", "/api/v1/books/{id}", MethodEnum.GET, "BOOKS"));
                        arr.add(new Permission("Get book with pagination", "/api/v1/books", MethodEnum.GET, "BOOKS"));
                        arr.add(new Permission("Get category book ", "/api/v1/books/categories", MethodEnum.GET,
                                        "BOOKS"));

                        arr.add(new Permission("Create order client", "/api/v1/orders", MethodEnum.POST, "ORDERS"));
                        arr.add(new Permission("Update a order", "/api/v1/orders", MethodEnum.PUT, "ORDERS"));
                        arr.add(new Permission("Get order with pagination", "/api/v1/orders", MethodEnum.GET,
                                        "ORDERS"));
                        arr.add(new Permission("Delete order", "/api/v1/orders", MethodEnum.DELETE, "ORDERS"));
                        arr.add(new Permission("order update paid", "/api/v1/orders/paid/{id}", MethodEnum.GET,
                                        "ORDERS"));
                        arr.add(new Permission("update order paid transfer", "/api/v1/orders/paid-transfer/{id}",
                                        MethodEnum.GET,
                                        "ORDERS"));
                        arr.add(new Permission("create order by admin", "/api/v1/orders/admin", MethodEnum.POST,
                                        "ORDERS"));
                        arr.add(new Permission("find user in active", "/api/v1/orders/user-active/{id}", MethodEnum.GET,
                                        "ORDERS"));
                        arr.add(new Permission("find order by user inactive", "/api/v1/orders/user-inactive/{id}",
                                        MethodEnum.GET,
                                        "ORDERS"));

                        arr.add(new Permission("statistical order", "/api/v1/statistical-order", MethodEnum.GET,
                                        "STATISTICAL"));

                        arr.add(new Permission("create transaction", "/api/v1/init", MethodEnum.POST, "PAYPAL"));
                        arr.add(new Permission("action transaction", "/api/v1/capture", MethodEnum.POST, "PAYPAL"));

                        arr.add(new Permission("find order detail", "/api/v1/order-detail/order-id/{id}",
                                        MethodEnum.GET,
                                        "ORDERDETAIL"));

                        this.permissionRepository.saveAll(arr);

                }

                if (countRoles == 0) {
                        List<Permission> allPermissions = this.permissionRepository.findAll();

                        Role adminRole = new Role();
                        adminRole.setName("ADMIN");
                        adminRole.setDescription("Admin thì full permissions");
                        adminRole.setActive(true);
                        adminRole.setPermissions(allPermissions);

                        this.roleRepository.save(adminRole);

                        // customer
                        Role customerRole = new Role();
                        customerRole.setName("CUSTOMER");
                        customerRole.setDescription("Customer permissions");
                        customerRole.setActive(true);

                        List<Permission> permissionsCus = Arrays.asList(
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/users/{id}",
                                                        MethodEnum.GET,
                                                        "USERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/users",
                                                        MethodEnum.PUT, "USERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/author",
                                                        MethodEnum.GET,
                                                        "AUTHOR"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/author/{id}",
                                                        MethodEnum.GET,
                                                        "AUTHOR"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/books",
                                                        MethodEnum.GET, "BOOKS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/books/{id}",
                                                        MethodEnum.GET,
                                                        "BOOKS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/books/categories",
                                                        MethodEnum.GET, "BOOKS"),

                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/publisher",
                                                        MethodEnum.GET,
                                                        "AUTHOR"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/publisher/{id}", MethodEnum.GET,
                                                        "PUBLISHER"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/orders",
                                                        MethodEnum.POST,
                                                        "ORDERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/orders/user-active/{id}",
                                                        MethodEnum.GET, "ORDERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/orders/user-inactive/{id}",
                                                        MethodEnum.GET, "ORDERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/orders/paid-transfer/{id}",
                                                        MethodEnum.GET, "ORDERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/order-detail/order-id/{id}",
                                                        MethodEnum.GET, "ORDERDETAIL"));
                        customerRole.setPermissions(permissionsCus);

                        this.roleRepository.save(customerRole);

                        // STAFF
                        Role staffRole = new Role();
                        staffRole.setName("STAFF");
                        staffRole.setDescription("staff permissions");
                        staffRole.setActive(true);

                        List<Permission> permissionsStaff = Arrays.asList(
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/users/{id}",
                                                        MethodEnum.GET,
                                                        "USERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/users",
                                                        MethodEnum.GET,
                                                        "USERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/users",
                                                        MethodEnum.POST,
                                                        "USERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/users",
                                                        MethodEnum.PUT, "USERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/author",
                                                        MethodEnum.GET,
                                                        "AUTHOR"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/author/{id}",
                                                        MethodEnum.GET,
                                                        "AUTHOR"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/author",
                                                        MethodEnum.PUT,
                                                        "AUTHOR"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/author",
                                                        MethodEnum.POST,
                                                        "AUTHOR"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/books",
                                                        MethodEnum.GET, "BOOKS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/books/{id}",
                                                        MethodEnum.GET,
                                                        "BOOKS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/books/categories",
                                                        MethodEnum.GET, "BOOKS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/books",
                                                        MethodEnum.POST,
                                                        "BOOKS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/books",
                                                        MethodEnum.PUT, "BOOKS"),

                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/publisher",
                                                        MethodEnum.GET,
                                                        "PUBLISHER"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/publisher/{id}", MethodEnum.GET,
                                                        "PUBLISHER"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/publisher",
                                                        MethodEnum.PUT,
                                                        "PUBLISHER"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/publisher",
                                                        MethodEnum.POST,
                                                        "PUBLISHER"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/orders",
                                                        MethodEnum.POST,
                                                        "ORDERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/orders",
                                                        MethodEnum.GET,
                                                        "ORDERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/orders",
                                                        MethodEnum.PUT,
                                                        "ORDERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule("/api/v1/orders",
                                                        MethodEnum.DELETE,
                                                        "ORDERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/orders/paid/{id}",
                                                        MethodEnum.GET,
                                                        "ORDERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/orders/admin", MethodEnum.POST,
                                                        "ORDERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/orders/user-active/{id}",
                                                        MethodEnum.GET, "ORDERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/orders/user-inactive/{id}",
                                                        MethodEnum.GET, "ORDERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/orders/paid-transfer/{id}",
                                                        MethodEnum.GET, "ORDERS"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/statistical-order",
                                                        MethodEnum.GET, "STATISTICAL"),
                                        this.permissionRepository.findByApiPathAndMethodAndModule(
                                                        "/api/v1/order-detail/order-id/{id}",
                                                        MethodEnum.GET, "ORDERDETAIL"));
                        staffRole.setPermissions(permissionsStaff);

                        this.roleRepository.save(staffRole);

                }

                if (countUsers == 0) {
                        User adminUser = new User();
                        adminUser.setEmail("admin@gmail.com");
                        adminUser.setAddress("hn");
                        adminUser.setAge(22);
                        adminUser.setGender(GenderEnum.MALE);
                        adminUser.setName("I'm super admin");
                        adminUser.setPassword(this.passwordEncoder.encode("123456"));

                        Role adminRole = this.roleRepository.findByName("ADMIN");
                        if (adminRole != null) {
                                adminUser.setRole(adminRole);
                        }

                        this.userRepository.save(adminUser);
                }

                if (countPermissions > 0 && countRoles > 0 && countUsers > 0) {
                        System.out.println(">>> SKIP INIT DATABASE ~ ALREADY HAVE DATA...");
                } else
                        System.out.println(">>> END INIT DATABASE");

        }

}
