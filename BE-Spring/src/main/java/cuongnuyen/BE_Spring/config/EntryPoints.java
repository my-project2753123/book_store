package cuongnuyen.BE_Spring.config;

import java.util.Arrays;
import java.util.List;

import cuongnuyen.BE_Spring.domain.EntryPoint;

public class EntryPoints {
    public static final List<EntryPoint> permissionCustomer = Arrays.asList(
            new EntryPoint("/api/v1/books", "GET"),
            new EntryPoint("/api/v1/author", "GET"),
            new EntryPoint("/api/v1/publisher", "GET"),
            new EntryPoint("/api/v1/orders", "GET"),
            new EntryPoint("/api/v1/users/{id}", "GET"),
            new EntryPoint("/api/v1/users", "PUT"));
}
