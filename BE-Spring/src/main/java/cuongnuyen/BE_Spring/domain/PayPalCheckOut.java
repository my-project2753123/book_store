package cuongnuyen.BE_Spring.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PayPalCheckOut {
    private double sum;
    private String currency;
    private String description;
}
