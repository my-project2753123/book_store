package cuongnuyen.BE_Spring.domain;

import java.time.Instant;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cuongnuyen.BE_Spring.utils.SecurityUntil;
import cuongnuyen.BE_Spring.utils.enums.OrderEnum;
import cuongnuyen.BE_Spring.utils.enums.PaymentEnum;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "orders")
@Getter
@Setter
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Enumerated(EnumType.STRING)
    private PaymentEnum payment;
    @Enumerated(EnumType.STRING)
    private OrderEnum status;
    private boolean active;
    private double totalAmount;
    private double totalAmountPlayable;
    private double discount;
    private String address;

    private Instant createdAt;
    private Instant updatedAt;
    private String createdBy;
    private String updatedBy;

    @OneToMany(mappedBy = "order")
    private List<OrderDetail> details;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @PrePersist
    public void handleBeforeCreate() {
        this.createdBy = SecurityUntil.getCurrentUserLogin().isPresent()
                ? SecurityUntil.getCurrentUserLogin().get()
                : "";
        this.createdAt = Instant.now();
    }

    @PreUpdate
    public void handleBeforeUpdate() {
        this.updatedBy = SecurityUntil.getCurrentUserLogin().isPresent()
                ? SecurityUntil.getCurrentUserLogin().get()
                : "";
        this.updatedAt = Instant.now();
    }
}
