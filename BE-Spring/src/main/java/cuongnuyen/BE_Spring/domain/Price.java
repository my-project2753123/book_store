package cuongnuyen.BE_Spring.domain;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cuongnuyen.BE_Spring.utils.SecurityUntil;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "price")
@Getter
@Setter
public class Price {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private double price;
    private boolean active;

    private Instant createdAt;
    private Instant updatedAt;
    private String createdBy;
    private String updatedBy;

    @ManyToOne
    @JoinColumn(name = "book_id")
    @JsonIgnore
    private Book book;

    @PrePersist
    public void handleBeforeCreate() {
        this.createdBy = SecurityUntil.getCurrentUserLogin().isPresent()
                ? SecurityUntil.getCurrentUserLogin().get()
                : "";
        this.createdAt = Instant.now();
    }

    @PreUpdate
    public void handleBeforeUpdate() {
        this.updatedBy = SecurityUntil.getCurrentUserLogin().isPresent()
                ? SecurityUntil.getCurrentUserLogin().get()
                : "";
        this.updatedAt = Instant.now();
    }
}
