package cuongnuyen.BE_Spring.domain.response;

import java.time.Instant;

import cuongnuyen.BE_Spring.utils.enums.GenderEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResRegisterDTO {
    private long id;
    private String name;
    private String email;
    private int age;
    private GenderEnum gender;
    private String address;
    private Instant createdAt;
}
