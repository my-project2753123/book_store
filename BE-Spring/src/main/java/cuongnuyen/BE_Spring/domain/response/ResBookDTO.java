package cuongnuyen.BE_Spring.domain.response;

import java.time.Instant;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public class ResBookDTO {
    private long id;
    private String name;
    private String description;
    private List<BookPrice> prices;
    private String image;
    private String category;
    private String genre;
    private long quantity;
    private Author author;
    private Publisher publisher;
    private boolean active;
    private Instant createdAt;
    private Instant updatedAt;
    private String createdBy;
    private String updatedBy;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class BookPrice {
        private long id;
        private double price;
        private boolean active;

    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Author {
        private long id;
        private String name;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Publisher {
        private long id;
        private String name;
    }
}
