package cuongnuyen.BE_Spring.domain.response;

import java.time.Instant;
import java.util.List;

import cuongnuyen.BE_Spring.domain.OrderDetail;
import cuongnuyen.BE_Spring.utils.enums.OrderEnum;
import cuongnuyen.BE_Spring.utils.enums.PaymentEnum;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResOrderDTO {
    private long id;
    private PaymentEnum payment;
    private OrderEnum status;
    private boolean active;
    private double totalAmount;
    private double totalAmountPlayable;
    private double discount;
    private List<OrderDetail> details;
    private User user;
    private String address;

    private Instant createdAt;
    private Instant updatedAt;
    private String createdBy;
    private String updatedBy;

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class OrderDetail {
        private long id;
        private Integer quantity;
        private double price;
        private double discount;
        private double amount;
        private boolean active;
        private Book book;

        @Getter
        @Setter
        @AllArgsConstructor
        @NoArgsConstructor
        public static class Book {
            private long id;
            private String image;
        }
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class User {
        private long id;
        private String name;
        private String email;
        private String address;
    }
}
