package cuongnuyen.BE_Spring.domain.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public class ResStatisticalOrderDTO {
    private double total;
    private double totalPayable;
    private Integer quantity;
    private List<Month> months;

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Month {
        private String month;
        private double total;
    }
}
