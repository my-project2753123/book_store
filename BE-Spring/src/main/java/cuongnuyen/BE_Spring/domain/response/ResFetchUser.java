package cuongnuyen.BE_Spring.domain.response;

import java.time.Instant;

import cuongnuyen.BE_Spring.utils.enums.GenderEnum;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResFetchUser {
    private long id;
    private String name;
    private String email;
    private int age;
    private GenderEnum gender;
    private String address;
    private boolean isActive;
    private Instant createdAt;
    private Instant updatedAt;
    private String createdBy;
    private String updatedBy;
    private RoleUser role;

    @Getter
    @Setter
    public static class RoleUser {
        private long id;
        private String name;
    }
}
