package cuongnuyen.BE_Spring.domain.response;

import java.time.Instant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResOrderDetailDTO {
    private long id;

    private Integer quantity;
    private double price;
    private double discount;
    private double amount;
    private boolean active;

    private Instant createdAt;
    private Instant updatedAt;
    private String createdBy;
    private String updatedBy;

    private Book book;

    @Getter
    @Setter
    public static class Book {
        private long id;
        private String image;
        private String name;
    }
}
