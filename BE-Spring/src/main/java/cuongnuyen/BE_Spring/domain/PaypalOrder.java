package cuongnuyen.BE_Spring.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaypalOrder {
    private double totalAmount;
    private String currency;
    private String receiver;
}
