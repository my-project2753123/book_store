package cuongnuyen.BE_Spring.domain.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReqLoginDTO {
    @NotBlank(message = "email không để trống")
    private String email;
    @NotBlank(message = "password không để trống")
    private String password;
}
