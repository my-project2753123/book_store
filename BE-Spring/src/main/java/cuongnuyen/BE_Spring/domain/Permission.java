package cuongnuyen.BE_Spring.domain;

import java.time.Instant;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cuongnuyen.BE_Spring.utils.SecurityUntil;
import cuongnuyen.BE_Spring.utils.enums.MethodEnum;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "permissions")
@Getter
@Setter
public class Permission {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private String apiPath;
    @Enumerated(EnumType.STRING)
    private MethodEnum method;
    private String module;

    private Instant createdAt;
    private Instant updatedAt;
    private String createdBy;
    private String updatedBy;

    @ManyToMany(mappedBy = "permissions")
    @JsonIgnore
    private List<Role> roles;

    public Permission(String name, String apiPath, MethodEnum method, String module) {
        this.name = name;
        this.apiPath = apiPath;
        this.method = method;
        this.module = module;
    }

    public Permission() {
    }

    @PrePersist
    public void handleBeforeCreate() {
        this.createdBy = SecurityUntil.getCurrentUserLogin().isPresent()
                ? SecurityUntil.getCurrentUserLogin().get()
                : "";
        this.createdAt = Instant.now();
    }

    @PreUpdate
    public void handleBeforeUpdate() {
        this.updatedBy = SecurityUntil.getCurrentUserLogin().isPresent()
                ? SecurityUntil.getCurrentUserLogin().get()
                : "";
        this.updatedAt = Instant.now();
    }

}
