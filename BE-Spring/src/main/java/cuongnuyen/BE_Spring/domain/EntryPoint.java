package cuongnuyen.BE_Spring.domain;

import cuongnuyen.BE_Spring.utils.enums.MethodEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EntryPoint {
    private String path;
    private String method;
}
