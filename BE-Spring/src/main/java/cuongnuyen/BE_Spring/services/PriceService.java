package cuongnuyen.BE_Spring.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import cuongnuyen.BE_Spring.domain.Book;
import cuongnuyen.BE_Spring.domain.Price;
import cuongnuyen.BE_Spring.repository.PriceRepository;

@Service
public class PriceService {
    private final PriceRepository priceRepository;

    public PriceService(PriceRepository priceRepository) {
        this.priceRepository = priceRepository;
    }

    public Price handleCreatePrice(Book book, Price price) {
        book.getPrices()
                .stream().filter(Price::isActive).map(item -> this.handleDisable(item))
                .collect(Collectors.toList());
        price.setActive(true);
        price.setBook(book);
        return this.priceRepository.save(price);
    }

    public Price handleDisable(Price price) {
        price.setActive(false);
        return this.priceRepository.save(price);
    }

    // public Price handleUpdatePrice(Price priceCurrent, Price price) {
    // priceCurrent.setActive(false);
    // this.priceRepository.save(priceCurrent);
    // return this.handleCreatePrice(price);
    // }
}
