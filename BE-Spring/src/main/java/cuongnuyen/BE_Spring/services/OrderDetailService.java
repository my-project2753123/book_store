package cuongnuyen.BE_Spring.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import cuongnuyen.BE_Spring.domain.Book;
import cuongnuyen.BE_Spring.domain.Order;
import cuongnuyen.BE_Spring.domain.OrderDetail;
import cuongnuyen.BE_Spring.domain.response.ResOrderDetailDTO;
import cuongnuyen.BE_Spring.repository.BookRepository;
import cuongnuyen.BE_Spring.repository.OrderDetailRepository;
import cuongnuyen.BE_Spring.repository.OrderRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class OrderDetailService {
    private final OrderDetailRepository orderDetailRepository;
    private final OrderRepository orderRepository;
    private final BookRepository bookRepository;

    public OrderDetail handleCreateOrderDetail(OrderDetail orderDetail, long orderId) {
        Book book = this.bookRepository.findById(orderDetail.getBook().getId());
        Order order = this.orderRepository.findById(orderId);

        OrderDetail orderDetailcurrent = new OrderDetail();

        orderDetailcurrent.setDiscount(0);
        orderDetailcurrent.setActive(true);

        double price = book.getPrices()
                .stream().filter(item -> item.isActive()).map(item -> item.getPrice())
                .collect(Collectors.toList()).get(0);

        orderDetailcurrent.setPrice(price);

        orderDetailcurrent.setQuantity(orderDetail.getQuantity());

        double amount = price * orderDetail.getQuantity()
                - (price * orderDetail.getQuantity() * orderDetail.getDiscount());

        orderDetailcurrent.setAmount(amount);
        orderDetailcurrent.setOrder(order);
        orderDetailcurrent.setBook(book);

        return this.orderDetailRepository.save(orderDetailcurrent);
    }

    public void handleDeleteOrderDetail(List<OrderDetail> details) {
        details.stream().forEach(item -> {
            OrderDetail orderDetail = this.orderDetailRepository.findById(item.getId());
            if (orderDetail != null) {
                orderDetail.setActive(false);
                this.orderDetailRepository.save(orderDetail);
            }
        });
    }

    public List<OrderDetail> handleFetchOrderDetailByOrder(Order order) {
        List<OrderDetail> details = this.orderDetailRepository.findByOrder(order);

        return details;
    }

    public ResOrderDetailDTO handleConvertOrderDetail(OrderDetail detail) {
        ResOrderDetailDTO detailDTO = new ResOrderDetailDTO();

        detailDTO.setActive(detail.isActive());
        detailDTO.setAmount(detail.getAmount());
        detailDTO.setId(detail.getId());
        detailDTO.setPrice(detail.getPrice());
        detailDTO.setCreatedAt(detail.getCreatedAt());
        detailDTO.setCreatedBy(detail.getCreatedBy());
        detailDTO.setUpdatedAt(detail.getUpdatedAt());
        detailDTO.setUpdatedBy(detail.getUpdatedBy());
        detailDTO.setDiscount(detail.getDiscount());
        detailDTO.setQuantity(detail.getQuantity());

        ResOrderDetailDTO.Book book = new ResOrderDetailDTO.Book();
        book.setId(detail.getBook().getId());
        book.setImage(detail.getBook().getImage());
        book.setName(detail.getBook().getName());
        detailDTO.setBook(book);

        return detailDTO;
    }
}
