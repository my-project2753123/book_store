package cuongnuyen.BE_Spring.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cuongnuyen.BE_Spring.domain.Role;
import cuongnuyen.BE_Spring.domain.User;
import cuongnuyen.BE_Spring.domain.response.ResFetchUser;
import cuongnuyen.BE_Spring.domain.response.ResRegisterDTO;
import cuongnuyen.BE_Spring.domain.response.ResResultPagination;
import cuongnuyen.BE_Spring.repository.UserRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleService roleService;

    public User handleCreateUser(User user) {
        String hashPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(hashPassword);
        user.setActive(true);
        Role role = this.roleService.handleFindByName("CUSTOMER");
        user.setRole(role);
        return this.userRepository.save(user);
    }

    public User fetchUserByEmail(String email) {
        User user = this.userRepository.findByEmail(email);
        if (user != null) {
            return user;
        }
        return null;
    }

    public User fetchUserById(long id) {
        User user = this.userRepository.findById(id);
        if (user != null) {
            return user;
        }
        return null;
    }

    public User handleUpdateUser(User userCurrent, User userBody) {
        if (userBody != null) {
            User user = userCurrent;
            user.setName(userBody.getName());
            user.setAge(userBody.getAge());
            user.setAddress(userBody.getAddress());
            user.setGender(userBody.getGender());
            if (userBody.getRole() != null) {
                user.setRole(userBody.getRole());
            }
            return this.userRepository.save(user);
        }
        return null;
    }

    public void handleDeleteUser(User user) {
        if (user != null) {
            user.setActive(false);
            this.userRepository.save(user);
        }
    }

    public ResResultPagination handleFetchAllUser(Specification<User> spec, Pageable pageable) {
        Page<User> page = this.userRepository.findAll(spec, pageable);

        ResResultPagination rs = new ResResultPagination();
        ResResultPagination.Meta mt = new ResResultPagination.Meta();

        mt.setPage(page.getNumber() + 1);
        mt.setPageSize(page.getSize());
        mt.setPages(page.getTotalPages());

        List<User> userIsActive = page.getContent()
                .stream()
                .filter(item -> item.isActive())
                .collect(Collectors.toList());

        List<ResFetchUser> fetchUsers = userIsActive
                .stream().map(item -> this.convertUser(item))
                .collect(Collectors.toList());

        mt.setTotal(fetchUsers.size());
        rs.setMeta(mt);

        rs.setResult(fetchUsers);

        return rs;

    }

    public ResFetchUser handleFetchUserByIdFormat(User user) {
        if (user != null) {
            return this.convertUser(user);
        }
        return null;
    }

    public ResRegisterDTO convertUserRegister(User user) {
        ResRegisterDTO registerDTO = new ResRegisterDTO();
        registerDTO.setId(user.getId());
        registerDTO.setName(user.getName());
        registerDTO.setEmail(user.getEmail());
        registerDTO.setAddress(user.getAddress());
        registerDTO.setAge(user.getAge());
        registerDTO.setGender(user.getGender());
        registerDTO.setCreatedAt(user.getCreatedAt());

        return registerDTO;
    }

    public ResFetchUser convertUser(User user) {
        ResFetchUser userConvert = new ResFetchUser();
        userConvert.setId(user.getId());
        userConvert.setName(user.getName());
        userConvert.setEmail(user.getEmail());
        userConvert.setAddress(user.getAddress());
        userConvert.setAge(user.getAge());
        userConvert.setGender(user.getGender());
        userConvert.setActive(user.isActive());
        userConvert.setCreatedAt(user.getCreatedAt());
        userConvert.setCreatedBy(user.getCreatedBy());
        userConvert.setUpdatedAt(user.getUpdatedAt());
        userConvert.setUpdatedBy(user.getUpdatedBy());

        ResFetchUser.RoleUser roleUser = new ResFetchUser.RoleUser();
        if (user.getRole() != null) {
            roleUser.setId(user.getRole().getId());
            roleUser.setName(user.getRole().getName());
            userConvert.setRole(roleUser);
        } else {
            userConvert.setRole(null);
        }

        return userConvert;
    }

    public void handleUpdateUserToken(String refreshToken, String email) {
        User user = this.fetchUserByEmail(email);
        if (user != null) {
            user.setRefreshToken(refreshToken);
            this.userRepository.save(user);
        }
    }

    public User handleFetchUserByRefreshAndEmail(String refreshToken, String email) {
        User user = this.userRepository.findByRefreshTokenAndEmail(refreshToken, email);
        if (user != null) {
            return user;
        }
        return null;
    }

    public User handleFetchUserByEmail(String name) {
        User user = this.userRepository.findByEmail(name);
        if (user != null) {
            return user;
        }
        return null;
    }

    public User handleFetchUserById(long id) {
        User user = this.userRepository.findById(id);
        if (user != null) {
            return user;
        }
        return null;
    }
}
