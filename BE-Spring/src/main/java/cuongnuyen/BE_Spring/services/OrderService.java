package cuongnuyen.BE_Spring.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cuongnuyen.BE_Spring.domain.Book;
import cuongnuyen.BE_Spring.domain.Order;
import cuongnuyen.BE_Spring.domain.OrderDetail;
import cuongnuyen.BE_Spring.domain.User;
import cuongnuyen.BE_Spring.domain.response.ResOrderDTO;
import cuongnuyen.BE_Spring.domain.response.ResResultPagination;
import cuongnuyen.BE_Spring.repository.BookRepository;
import cuongnuyen.BE_Spring.repository.OrderRepository;
import cuongnuyen.BE_Spring.repository.UserRepository;
import cuongnuyen.BE_Spring.utils.SecurityUntil;
import cuongnuyen.BE_Spring.utils.enums.OrderEnum;
import cuongnuyen.BE_Spring.utils.enums.PaymentEnum;
import cuongnuyen.BE_Spring.utils.error.IdInvalidException;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final BookRepository bookRepository;
    private final OrderDetailService orderDetailService;

    @Transactional
    public Order handleCreateOrder(Order order) {
        Order orderCurrent = new Order();
        orderCurrent.setActive(true);
        orderCurrent.setStatus(order.getStatus());
        orderCurrent.setPayment(order.getPayment());
        orderCurrent.setDiscount(order.getDiscount());
        orderCurrent.setDetails(order.getDetails());

        String email = SecurityUntil.getCurrentUserLogin().get();
        User user = this.userRepository.findByEmail(email);
        orderCurrent.setUser(user);
        orderCurrent.setAddress(user.getAddress());

        Order orderSave = this.orderRepository.save(orderCurrent);
        List<OrderDetail> details = this.orderDetailNotDuplicate(orderSave.getDetails(), orderSave.getId());
        orderSave.setDetails(details);

        if (orderCurrent.getStatus().equals(OrderEnum.PAID)) {
            for (OrderDetail od : orderSave.getDetails().stream()
                    .filter(item -> item.isActive())
                    .collect(Collectors.toList())) {
                Book book = this.bookRepository.findById(od.getBook().getId());

                book.setQuantity(book.getQuantity() - od.getQuantity());
                this.bookRepository.save(book);
            }
        }

        double total = 0;
        for (OrderDetail orderDetail : details) {
            total += orderDetail.getAmount();
        }
        orderSave.setTotalAmount(total);

        double totalPayable = total - (total * orderSave.getDiscount());
        orderSave.setTotalAmountPlayable(totalPayable);

        return this.orderRepository.save(orderSave);
    }

    public ResResultPagination handleFetchAllOrder(Specification<Order> spec, Pageable pageable) {
        Page<Order> page = this.orderRepository.findAll(spec, pageable);

        ResResultPagination rs = new ResResultPagination();
        ResResultPagination.Meta mt = new ResResultPagination.Meta();

        mt.setPage(page.getNumber() + 1);
        mt.setPageSize(page.getSize());
        mt.setPages(page.getTotalPages());

        mt.setTotal(page.getTotalElements());
        rs.setMeta(mt);

        List<Order> temp = page.getContent();

        temp.forEach(order -> {
            List<OrderDetail> tempDetails = order.getDetails();
            List<OrderDetail> details = tempDetails.stream().filter(detail -> detail.isActive())
                    .collect(Collectors.toList());
            order.setDetails(details);
        });

        List<ResOrderDTO> orderDTOs = temp
                .stream().map(item -> this.handleConvertOrder(item))
                .collect(Collectors.toList());
        rs.setResult(orderDTOs);

        return rs;
    }

    @Transactional
    public Order handleUpdateOrder(Order order, Order orderCurrent) throws IdInvalidException {
        orderCurrent.setActive(order.isActive());
        orderCurrent.setPayment(order.getPayment());
        orderCurrent.setDiscount(order.getDiscount());
        orderCurrent.setAddress(order.getAddress());
        orderCurrent.setStatus(order.getStatus());

        if (order.getDetails().size() > 0) {
            this.orderDetailService.handleDeleteOrderDetail(orderCurrent.getDetails());
            List<OrderDetail> details = this.orderDetailNotDuplicate(order.getDetails(), orderCurrent.getId());
            orderCurrent.setDetails(details);

            if (orderCurrent.getStatus().equals(OrderEnum.PAID)) {
                for (OrderDetail od : orderCurrent.getDetails().stream()
                        .filter(item -> item.isActive())
                        .collect(Collectors.toList())) {
                    Book book = this.bookRepository.findById(od.getBook().getId());
                    book.setQuantity(book.getQuantity() - od.getQuantity());
                    this.bookRepository.save(book);
                }
            }

            double total = 0;
            for (OrderDetail orderDetail : details) {
                total += orderDetail.getAmount();
            }
            orderCurrent.setTotalAmount(total);

            double totalPayable = total - (total * order.getDiscount());
            orderCurrent.setTotalAmountPlayable(totalPayable);
        }
        return this.orderRepository.save(orderCurrent);
    }

    public List<OrderDetail> orderDetailNotDuplicate(List<OrderDetail> details, long orderId) {
        Map<Long, OrderDetail> orderDetailMap = details.stream()
                .collect(Collectors.toMap(item -> item.getBook().getId(), Function.identity(),
                        (existing, replacement) -> {
                            existing.setQuantity(existing.getQuantity() + replacement.getQuantity());
                            return existing;
                        }));

        List<OrderDetail> updatedDetails = orderDetailMap.values().stream()
                .map(orderDetail -> orderDetailService.handleCreateOrderDetail(orderDetail, orderId))
                .collect(Collectors.toList());
        return updatedDetails;
    }

    public ResOrderDTO handleConvertOrder(Order order) {
        ResOrderDTO orderDTO = new ResOrderDTO();

        orderDTO.setId(order.getId());
        orderDTO.setActive(order.isActive());
        orderDTO.setCreatedAt(order.getCreatedAt());
        orderDTO.setCreatedBy(order.getCreatedBy());
        orderDTO.setUpdatedAt(order.getUpdatedAt());
        orderDTO.setUpdatedBy(order.getUpdatedBy());
        orderDTO.setDiscount(order.getDiscount());
        orderDTO.setPayment(order.getPayment());
        orderDTO.setStatus(order.getStatus());
        orderDTO.setTotalAmount(order.getTotalAmount());
        orderDTO.setTotalAmountPlayable(order.getTotalAmountPlayable());
        orderDTO.setAddress(order.getAddress());

        ResOrderDTO.User user = new ResOrderDTO.User(order.getUser().getId(), order.getUser().getName(),
                order.getUser().getEmail(), order.getUser().getAddress());
        orderDTO.setUser(user);

        List<ResOrderDTO.OrderDetail> details = order.getDetails()
                .stream().map(item -> new ResOrderDTO.OrderDetail(
                        item.getId(), item.getQuantity(), item.getPrice(), item.getDiscount(), item.getAmount(),
                        item.isActive(),
                        new ResOrderDTO.OrderDetail.Book(item.getBook().getId(), item.getBook().getImage())))
                .collect(Collectors.toList());
        orderDTO.setDetails(details);

        return orderDTO;
    }

    public Order handleFetchOrderById(long id) {
        Order order = this.orderRepository.findById(id);
        if (order != null) {
            return order;
        }
        return null;
    }

    public void handleDeleteOrder(Order order) {
        order.setActive(false);
        this.orderRepository.save(order);
    }

    @Transactional
    public Order handlePaid(Order order) {
        for (OrderDetail od : order.getDetails().stream()
                .filter(item -> item.isActive())
                .collect(Collectors.toList())) {
            Book book = this.bookRepository.findById(od.getBook().getId());

            book.setQuantity(book.getQuantity() - od.getQuantity());
            this.bookRepository.save(book);
        }
        order.setStatus(OrderEnum.PAID);
        return this.orderRepository.save(order);
    }

    @Transactional
    public Order handleTransFerPaid(Order order) {
        for (OrderDetail od : order.getDetails().stream()
                .filter(item -> item.isActive())
                .collect(Collectors.toList())) {
            Book book = this.bookRepository.findById(od.getBook().getId());

            book.setQuantity(book.getQuantity() - od.getQuantity());
            this.bookRepository.save(book);
        }
        order.setStatus(OrderEnum.PAID);
        order.setPayment(PaymentEnum.TRANSFER);
        return this.orderRepository.save(order);
    }

    public Order handleUnpaid(Order order) {
        order.setStatus(OrderEnum.UNPAID);
        return this.orderRepository.save(order);
    }

    @Transactional
    public Order handleCreateOrderAD(Order order) {
        Order orderCurrent = new Order();
        orderCurrent.setActive(true);
        orderCurrent.setStatus(order.getStatus());
        orderCurrent.setPayment(order.getPayment());
        orderCurrent.setDiscount(order.getDiscount());
        orderCurrent.setDetails(order.getDetails());

        User user = this.userRepository.findById(order.getUser().getId());
        orderCurrent.setUser(user);

        orderCurrent.setAddress(order.getAddress().equals("") ? user.getAddress() : order.getAddress());

        Order orderSave = this.orderRepository.save(orderCurrent);
        List<OrderDetail> details = this.orderDetailNotDuplicate(orderSave.getDetails(), orderSave.getId());
        orderSave.setDetails(details);

        if (orderCurrent.getStatus().equals(OrderEnum.PAID)) {
            for (OrderDetail od : orderSave.getDetails().stream()
                    .filter(item -> item.isActive())
                    .collect(Collectors.toList())) {
                Book book = this.bookRepository.findById(od.getBook().getId());

                book.setQuantity(book.getQuantity() - od.getQuantity());
                this.bookRepository.save(book);
            }
        }

        double total = 0;
        for (OrderDetail orderDetail : details) {
            total += orderDetail.getAmount();
        }
        orderSave.setTotalAmount(total);

        double totalPayable = total - (total * orderSave.getDiscount());
        orderSave.setTotalAmountPlayable(totalPayable);

        return this.orderRepository.save(orderSave);
    }

    public List<ResOrderDTO> handleOrderByUserActive(User user) {
        List<Order> orders = this.orderRepository.findByUserAndActive(user, true);

        List<ResOrderDTO> dtos = orders.stream()
                .map(item -> this.handleConvertOrder(item)).collect(Collectors.toList());

        return dtos;
    }

    public List<ResOrderDTO> handleOrderByUserInactive(User user) {
        List<Order> orders = this.orderRepository.findByUserAndActive(user, false);

        List<ResOrderDTO> dtos = orders.stream()
                .map(item -> this.handleConvertOrder(item)).collect(Collectors.toList());

        return dtos;
    }
}
