package cuongnuyen.BE_Spring.services;

import java.time.YearMonth;
import java.time.ZoneId;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.DoubleAccumulator;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import cuongnuyen.BE_Spring.domain.Order;
import cuongnuyen.BE_Spring.domain.response.ResStatisticalOrderDTO;
import cuongnuyen.BE_Spring.repository.OrderRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class StatisticalService {
    private final OrderRepository orderRepository;

    public List<Order> handleStatisticalOrder(Specification<Order> spec) {
        List<Order> orders = this.orderRepository.findAll(spec);

        return orders;
    }

    public ResStatisticalOrderDTO handleConvertStatisticalOrder(List<Order> orders) {
        ResStatisticalOrderDTO orderDTO = new ResStatisticalOrderDTO();
        orderDTO.setQuantity(orders.size());
        Map<YearMonth, DoubleSummaryStatistics> maps = orders.stream()
                .collect(
                        Collectors.groupingBy(
                                order -> YearMonth
                                        .from(order.getCreatedAt().atZone(ZoneId.systemDefault()).toLocalDate()),
                                Collectors.summarizingDouble(Order::getTotalAmountPlayable)));
        List<ResStatisticalOrderDTO.Month> list = maps.entrySet()
                .stream().map(item -> {
                    return new ResStatisticalOrderDTO.Month(
                            item.getKey().toString(), item.getValue().getSum());
                }).collect(Collectors.toList());
        orderDTO.setMonths(list);
        double total = 0;
        double totalPayable = 0;
        for (Order order : orders) {
            total += order.getTotalAmount();
            totalPayable += order.getTotalAmountPlayable();
        }
        orderDTO.setTotal(total);
        orderDTO.setTotalPayable(totalPayable);

        return orderDTO;
    }
}
