package cuongnuyen.BE_Spring.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import cuongnuyen.BE_Spring.domain.Permission;
import cuongnuyen.BE_Spring.domain.response.ResResultPagination;
import cuongnuyen.BE_Spring.repository.PermissionRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class PermissionService {
    private final PermissionRepository permissionRepository;

    public Permission handleCreatePermission(Permission permission) {
        return this.permissionRepository.save(permission);
    }

    public Permission handleFindPermissionExist(Permission permission) {
        Permission permissionDB = this.permissionRepository.findByApiPathAndMethodAndModule(permission.getApiPath(),
                permission.getMethod(), permission.getModule());
        if (permissionDB != null) {
            return permissionDB;
        }
        return null;
    }

    public ResResultPagination handleFetchAllPermission(
            Specification<Permission> spec, Pageable pageable) {
        Page<Permission> page = this.permissionRepository.findAll(spec, pageable);

        ResResultPagination rs = new ResResultPagination();
        ResResultPagination.Meta mt = new ResResultPagination.Meta();

        mt.setPage(page.getNumber() + 1);
        mt.setPageSize(page.getSize());
        mt.setPages(page.getTotalPages());
        mt.setTotal(page.getTotalElements());
        rs.setMeta(mt);

        rs.setResult(page.getContent());

        return rs;
    }

    public void handleDeletePermission(long id) {
        Permission permission = this.handleFindById(id);
        if (permission != null) {
            permission.getRoles().stream()
                    .forEach(item -> item.getPermissions().remove(permission));
            this.permissionRepository.delete(permission);
        }

    }

    public Permission handleFindById(long id) {
        Optional<Permission> optional = this.permissionRepository.findById(id);
        if (optional.isPresent()) {
            return optional.get();
        }
        return null;
    }

    public Permission handleUpdatePermission(Permission permission, Permission currentPermission) {
        currentPermission.setApiPath(permission.getApiPath());
        currentPermission.setMethod(permission.getMethod());
        currentPermission.setModule(permission.getModule());
        currentPermission.setName(permission.getName());
        return this.permissionRepository.save(currentPermission);
    }
}
