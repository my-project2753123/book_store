package cuongnuyen.BE_Spring.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import cuongnuyen.BE_Spring.domain.Book;
import cuongnuyen.BE_Spring.domain.Price;
import cuongnuyen.BE_Spring.domain.response.ResBookDTO;
import cuongnuyen.BE_Spring.domain.response.ResCategoryDTO;
import cuongnuyen.BE_Spring.domain.response.ResResultPagination;
import cuongnuyen.BE_Spring.repository.BookRepository;

@Service
public class BookService {
    private final BookRepository bookRepository;
    private final PriceService priceService;

    public BookService(BookRepository bookRepository, PriceService priceService) {
        this.bookRepository = bookRepository;
        this.priceService = priceService;
    }

    public Book handleCreateBook(Book book) {
        Book cBook = this.bookRepository.save(book);
        List<Price> prices = cBook.getPrices()
                .stream().map(item -> this.priceService.handleCreatePrice(book, item))
                .collect(Collectors.toList());
        cBook.setPrices(prices);
        cBook.setActive(true);
        return cBook;
    }

    public Book handleUpdateBook(Book currentBook, Book book) {
        if (book.getAuthor() != null) {
            currentBook.setAuthor(book.getAuthor());
        }
        if (book.getPublisher() != null) {
            currentBook.setPublisher(book.getPublisher());
        }
        if (book.getPrices().size() > 0) {
            List<Price> prices = book.getPrices()
                    .stream().map(item -> this.priceService.handleCreatePrice(currentBook, item))
                    .collect(Collectors.toList());
            currentBook.setPrices(prices);
        }
        currentBook.setCategory(book.getCategory());
        currentBook.setGenre(book.getGenre());
        currentBook.setActive(book.isActive());
        currentBook.setName(book.getName());
        currentBook.setImage(book.getImage());
        currentBook.setQuantity(book.getQuantity());
        currentBook.setDescription(book.getDescription());
        return this.bookRepository.save(currentBook);
    }

    public Book handleFetchBookByName(String name) {
        Book book = this.bookRepository.findByName(name);
        if (book != null) {
            return book;
        }
        return null;
    }

    public Book handleFetchBookById(long id) {
        Book book = this.bookRepository.findById(id);
        if (book != null) {
            return book;
        }
        return null;
    }

    public ResResultPagination handleFetchAllBook(Specification<Book> spec, Pageable pageable) {
        Page<Book> page = this.bookRepository.findAll(spec, pageable);

        ResResultPagination rs = new ResResultPagination();
        ResResultPagination.Meta mt = new ResResultPagination.Meta();

        mt.setPage(page.getNumber() + 1);
        mt.setPageSize(page.getSize());
        mt.setPages(page.getTotalPages());

        mt.setTotal(page.getTotalElements());
        rs.setMeta(mt);

        List<ResBookDTO> bookDTOs = page.getContent()
                .stream().map(item -> this.convertBookDTO(item))
                .collect(Collectors.toList());

        rs.setResult(bookDTOs);

        return rs;
    }

    public ResBookDTO convertBookDTO(Book book) {
        ResBookDTO bookDTO = new ResBookDTO();
        ResBookDTO.Author author = new ResBookDTO.Author();
        ResBookDTO.Publisher publisher = new ResBookDTO.Publisher();

        bookDTO.setId(book.getId());
        bookDTO.setName(book.getName());
        author.setId(book.getAuthor().getId());
        author.setName(book.getAuthor().getName());
        bookDTO.setAuthor(author);
        bookDTO.setCreatedAt(book.getCreatedAt());
        bookDTO.setCreatedBy(book.getCreatedBy());
        bookDTO.setUpdatedAt(book.getUpdatedAt());
        bookDTO.setUpdatedBy(book.getUpdatedBy());
        bookDTO.setDescription(book.getDescription());
        bookDTO.setImage(book.getImage());
        bookDTO.setQuantity(book.getQuantity());
        bookDTO.setActive(book.isActive());
        bookDTO.setCategory(book.getCategory());
        bookDTO.setGenre(book.getGenre());

        List<ResBookDTO.BookPrice> prices = book.getPrices()
                .stream().map(item -> new ResBookDTO.BookPrice(item.getId(), item.getPrice(), item.isActive()))
                .collect(Collectors.toList());
        bookDTO.setPrices(prices);

        publisher.setId(book.getPublisher().getId());
        publisher.setName(book.getPublisher().getName());
        bookDTO.setPublisher(publisher);

        return bookDTO;

    }

    public List<ResCategoryDTO> handleFetchAllCategory() {
        List<Book> books = this.bookRepository.findAll();
        Map<String, List<String>> categories = books.stream()
                .collect(Collectors.groupingBy(Book::getCategory,
                        Collectors.mapping(Book::getGenre,
                                Collectors.collectingAndThen(Collectors.toSet(), ArrayList::new))));
        List<ResCategoryDTO> categoryList = categories.entrySet().stream()
                .map(entry -> new ResCategoryDTO(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());

        return categoryList;

    }

}
