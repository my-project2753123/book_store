package cuongnuyen.BE_Spring.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import cuongnuyen.BE_Spring.domain.Permission;
import cuongnuyen.BE_Spring.domain.Role;
import cuongnuyen.BE_Spring.domain.response.ResResultPagination;
import cuongnuyen.BE_Spring.domain.response.ResRoleDTO;
import cuongnuyen.BE_Spring.repository.PermissionRepository;
import cuongnuyen.BE_Spring.repository.RoleRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class RoleService {
    private final RoleRepository roleRepository;
    private final PermissionRepository permissionRepository;

    public Role handleCreateRole(Role role) {
        role.setActive(true);
        List<Long> listId = role.getPermissions()
                .stream().map(item -> item.getId()).collect(Collectors.toList());
        List<Permission> permissions = this.permissionRepository.findByIdIn(listId);
        role.setPermissions(permissions);
        return this.roleRepository.save(role);
    }

    public Role handleFindByName(String name) {
        Role role = this.roleRepository.findByName(name);
        if (role != null) {
            return role;
        }
        return null;
    }

    public ResResultPagination handleFetchAll(Specification<Role> spec, Pageable pageable) {
        Page<Role> page = this.roleRepository.findAll(spec, pageable);

        ResResultPagination rs = new ResResultPagination();
        ResResultPagination.Meta mt = new ResResultPagination.Meta();

        mt.setPage(page.getNumber() + 1);
        mt.setPageSize(page.getSize());
        mt.setPages(page.getTotalPages());

        // List<Role> roles = page.getContent()
        // .stream().filter(item -> item.isActive()).collect(Collectors.toList());
        mt.setTotal(page.getTotalElements());
        rs.setMeta(mt);
        rs.setResult(page.getContent());

        return rs;
    }

    public void handleDeleteRole(long id) {
        Role role = this.roleRepository.findById(id);
        if (role != null) {
            role.setActive(false);
            this.roleRepository.save(role);
        }
    }

    public Role handleFetchRoleById(long id) {
        Role role = this.roleRepository.findById(id);
        if (role != null) {
            return role;
        }
        return null;
    }

    public Role handleUpdateRole(Role roleCurrent, Role role) {
        roleCurrent.setId(role.getId());
        if (role.getName() != null) {
            roleCurrent.setName(role.getName());
        }
        roleCurrent.setActive(role.isActive());
        roleCurrent.setDescription(role.getDescription());
        roleCurrent.setPermissions(role.getPermissions());
        return this.roleRepository.save(roleCurrent);
    }

    public ResRoleDTO handleConvertRole(Role role) {
        ResRoleDTO resRoleDTO = new ResRoleDTO();

        resRoleDTO.setId(role.getId());
        resRoleDTO.setName(role.getName());
        resRoleDTO.setActive(role.isActive());
        resRoleDTO.setCreatedAt(role.getCreatedAt());
        resRoleDTO.setCreatedBy(role.getCreatedBy());
        resRoleDTO.setDescription(role.getDescription());
        resRoleDTO.setUpdatedAt(role.getUpdatedAt());
        resRoleDTO.setUpdatedBy(role.getUpdatedBy());

        List<ResRoleDTO.PermissionRole> permissions = role.getPermissions()
                .stream().map(item -> new ResRoleDTO.PermissionRole(item.getId(), item.getName(), item.getModule()))
                .collect(Collectors.toList());

        resRoleDTO.setPermissions(permissions);

        return resRoleDTO;
    }

}
