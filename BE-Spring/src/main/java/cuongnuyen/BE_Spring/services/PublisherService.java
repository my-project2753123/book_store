package cuongnuyen.BE_Spring.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import cuongnuyen.BE_Spring.domain.Publisher;
import cuongnuyen.BE_Spring.domain.response.ResResultPagination;
import cuongnuyen.BE_Spring.repository.PublisherRepository;

@Service
public class PublisherService {
    private final PublisherRepository publisherRepository;

    public PublisherService(PublisherRepository publisherRepository) {
        this.publisherRepository = publisherRepository;
    }

    public Publisher handleCreatePublisher(Publisher publisher) {
        return this.publisherRepository.save(publisher);
    }

    public Publisher handleFetchPublisherById(long id) {
        Publisher publisher = this.publisherRepository.findById(id);
        if (publisher != null) {
            return publisher;
        }
        return null;
    }

    public Publisher handleUpdatePublisher(Publisher currentPublisher, Publisher publisher) {
        currentPublisher.setName(publisher.getName());
        currentPublisher.setDescription(publisher.getDescription());
        currentPublisher.setLogo(publisher.getLogo());
        return this.publisherRepository.save(currentPublisher);
    }

    public ResResultPagination handleFetchAllPublisher(Specification<Publisher> spec, Pageable pageable) {
        Page<Publisher> page = this.publisherRepository.findAll(spec, pageable);

        ResResultPagination rs = new ResResultPagination();
        ResResultPagination.Meta mt = new ResResultPagination.Meta();

        mt.setPage(page.getNumber() + 1);
        mt.setPageSize(page.getSize());
        mt.setPages(page.getTotalPages());

        mt.setTotal(page.getTotalElements());
        rs.setMeta(mt);

        rs.setResult(page.getContent());

        return rs;
    }
}
