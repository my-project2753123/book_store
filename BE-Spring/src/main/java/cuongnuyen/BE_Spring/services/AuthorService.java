package cuongnuyen.BE_Spring.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import cuongnuyen.BE_Spring.domain.Author;

import cuongnuyen.BE_Spring.domain.response.ResResultPagination;
import cuongnuyen.BE_Spring.repository.AuthorRepository;

@Service
public class AuthorService {
    private final AuthorRepository authorRepository;

    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public Author handleCreateAuthor(Author author) {
        return this.authorRepository.save(author);
    }

    public Author handleFetchAuthorById(long id) {
        Author author = this.authorRepository.findById(id);
        if (author != null) {
            return author;
        }
        return null;
    }

    public Author handleUpdateAuthor(Author currentAuthor, Author author) {
        currentAuthor.setName(author.getName());
        currentAuthor.setDescription(author.getDescription());
        currentAuthor.setLogo(author.getLogo());
        return this.authorRepository.save(currentAuthor);
    }

    public ResResultPagination handleFetchAllAuthor(Specification<Author> spec, Pageable pageable) {
        Page<Author> page = this.authorRepository.findAll(spec, pageable);

        ResResultPagination rs = new ResResultPagination();
        ResResultPagination.Meta mt = new ResResultPagination.Meta();

        mt.setPage(page.getNumber() + 1);
        mt.setPageSize(page.getSize());
        mt.setPages(page.getTotalPages());

        mt.setTotal(page.getTotalElements());
        rs.setMeta(mt);

        rs.setResult(page.getContent());

        return rs;
    }
}
