package cuongnuyen.BE_Spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import cuongnuyen.BE_Spring.domain.Order;
import cuongnuyen.BE_Spring.domain.OrderDetail;
import java.util.List;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Long>, JpaSpecificationExecutor<OrderDetail> {
    OrderDetail findById(long id);

    List<OrderDetail> findByOrder(Order order);
}
