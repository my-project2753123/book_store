package cuongnuyen.BE_Spring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import cuongnuyen.BE_Spring.domain.Permission;
import cuongnuyen.BE_Spring.utils.enums.MethodEnum;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long>, JpaSpecificationExecutor<Permission> {
    boolean existsByApiPathAndMethodAndModule(String apiPath, MethodEnum method, String module);

    Permission findByApiPathAndMethodAndModule(String apiPath, MethodEnum method, String module);

    List<Permission> findByIdIn(List<Long> id);

}
