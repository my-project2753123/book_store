package cuongnuyen.BE_Spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import cuongnuyen.BE_Spring.domain.Order;
import cuongnuyen.BE_Spring.domain.User;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long>, JpaSpecificationExecutor<Order> {
    Order findById(long id);

    List<Order> findByUserAndActive(User user, Boolean active);

}
