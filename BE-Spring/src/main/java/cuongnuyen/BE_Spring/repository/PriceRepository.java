package cuongnuyen.BE_Spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cuongnuyen.BE_Spring.domain.Price;

@Repository
public interface PriceRepository extends JpaRepository<Price, Long> {

}
