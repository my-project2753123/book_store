package cuongnuyen.BE_Spring.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cuongnuyen.BE_Spring.domain.User;
import cuongnuyen.BE_Spring.domain.request.ReqLoginDTO;
import cuongnuyen.BE_Spring.domain.response.ResFetchUser;
import cuongnuyen.BE_Spring.domain.response.ResLoginDTO;
import cuongnuyen.BE_Spring.domain.response.ResRegisterDTO;
import cuongnuyen.BE_Spring.services.UserService;
import cuongnuyen.BE_Spring.utils.SecurityUntil;
import cuongnuyen.BE_Spring.utils.error.IdInvalidException;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1")
public class AuthController {
    private final Logger log = LoggerFactory.getLogger(AuthController.class);
    private final UserService userService;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final SecurityUntil securityUntil;

    @Value("${config-spring-security.jwt.refresh-token-validity-in-seconds}")
    private long refreshTokenExpiration;

    public AuthController(UserService userService,
            AuthenticationManagerBuilder authenticationManagerBuilder, SecurityUntil securityUntil) {
        this.userService = userService;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.securityUntil = securityUntil;
    }

    @PostMapping("/auth/login")
    public ResponseEntity<ResLoginDTO> handleLogin(@RequestBody @Valid ReqLoginDTO reqLoginDTO) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                reqLoginDTO.getEmail(), reqLoginDTO.getPassword());
        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        ResLoginDTO res = new ResLoginDTO();
        User user = this.userService.fetchUserByEmail(reqLoginDTO.getEmail());
        if (user != null) {
            ResLoginDTO.UserLogin login = new ResLoginDTO.UserLogin(
                    user.getId(), user.getEmail(), user.getName(), user.getRole());
            res.setUser(login);
        }

        String accessToken = this.securityUntil.createAccessToken(authentication.getName(), res);
        res.setAccessToken(accessToken);
        String refreshToken = this.securityUntil.createRefreshToken(reqLoginDTO.getEmail(), res);
        this.userService.handleUpdateUserToken(refreshToken, reqLoginDTO.getEmail());

        ResponseCookie responseCookie = ResponseCookie
                .from("refresh_token", refreshToken)
                .httpOnly(true)
                .secure(true)
                .path("/")
                .maxAge(refreshTokenExpiration)
                .build();

        return ResponseEntity.status(HttpStatus.OK)
                .header(HttpHeaders.SET_COOKIE, responseCookie.toString())
                .body(res);
    }

    @GetMapping("/auth/account")
    public ResponseEntity<ResLoginDTO.UserGetAccount> handleGetAccount() {
        String email = SecurityUntil.getCurrentUserLogin().isPresent()
                ? SecurityUntil.getCurrentUserLogin().get()
                : "";
        User currentUserDb = this.userService.fetchUserByEmail(email);
        ResLoginDTO.UserLogin userLogin = new ResLoginDTO.UserLogin();
        ResLoginDTO.UserGetAccount getAccount = new ResLoginDTO.UserGetAccount();

        if (currentUserDb != null) {
            userLogin.setId(currentUserDb.getId());
            userLogin.setEmail(currentUserDb.getEmail());
            userLogin.setName(currentUserDb.getName());
            userLogin.setRole(currentUserDb.getRole());
            getAccount.setUser(userLogin);
        }
        return ResponseEntity.ok().body(getAccount);
    }

    @GetMapping("/auth/refresh-token")
    public ResponseEntity<ResLoginDTO> handleRefreshToken(
            @CookieValue(name = "refresh_token", defaultValue = "abc") String refresh_token) throws IdInvalidException {
        if (refresh_token.equals("abc")) {
            throw new IdInvalidException("Bạn không có refresh token ở cookie");
        }

        Jwt decodedToken = this.securityUntil.checkValidRefreshToken(refresh_token);
        String email = decodedToken.getSubject();

        User currentUser = this.userService.handleFetchUserByRefreshAndEmail(refresh_token, email);

        if (currentUser == null) {
            throw new IdInvalidException("Refresh Token không hợp lệ");
        }

        ResLoginDTO res = new ResLoginDTO();
        User currentDB = this.userService.fetchUserByEmail(email);
        if (currentDB != null) {
            ResLoginDTO.UserLogin userLogin = new ResLoginDTO.UserLogin(
                    currentDB.getId(),
                    currentDB.getEmail(),
                    currentDB.getName(), currentDB.getRole());
            res.setUser(userLogin);
        }
        String accessToken = this.securityUntil.createAccessToken(email, res);
        res.setAccessToken(accessToken);
        String new_refresh_token = this.securityUntil.createRefreshToken(email, res);
        this.userService.handleUpdateUserToken(new_refresh_token, email);
        ResponseCookie responseCookie = ResponseCookie
                .from("refresh_token", new_refresh_token)
                .httpOnly(true)
                .secure(true)
                .path("/")
                .maxAge(refreshTokenExpiration)
                .build();

        return ResponseEntity.status(HttpStatus.OK)
                .header(HttpHeaders.SET_COOKIE, responseCookie.toString())
                .body(res);

    }

    @GetMapping("/auth/logout")
    public ResponseEntity<Void> handleLogout() throws IdInvalidException {
        String email = SecurityUntil.getCurrentUserLogin().isPresent()
                ? SecurityUntil.getCurrentUserLogin().get()
                : "";
        if (email == "") {
            throw new IdInvalidException("Token không hợp lệ");
        } else {
            this.userService.handleUpdateUserToken(null, email);

            ResponseCookie responseCookie = ResponseCookie
                    .from("refresh_token", null)
                    .httpOnly(true)
                    .secure(true)
                    .path("/")
                    .maxAge(0)
                    .build();

            return ResponseEntity.status(HttpStatus.OK)
                    .header(HttpHeaders.SET_COOKIE, responseCookie.toString())
                    .body(null);

        }
    }

    @PostMapping("/auth/register")
    public ResponseEntity<ResRegisterDTO> handleRegister(@RequestBody @Valid User user) throws IdInvalidException {
        User userDB = this.userService.fetchUserByEmail(user.getEmail());
        if (userDB != null) {
            throw new IdInvalidException("user đã tồn tại");
        }
        User userCreate = this.userService.handleCreateUser(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(this.userService.convertUserRegister(userCreate));
    }

}
