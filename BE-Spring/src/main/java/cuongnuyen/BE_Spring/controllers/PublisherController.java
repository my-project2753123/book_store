package cuongnuyen.BE_Spring.controllers;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.turkraft.springfilter.boot.Filter;

import cuongnuyen.BE_Spring.domain.Publisher;
import cuongnuyen.BE_Spring.domain.response.ResResultPagination;
import cuongnuyen.BE_Spring.services.PublisherService;
import cuongnuyen.BE_Spring.utils.error.IdInvalidException;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class PublisherController {
    private final PublisherService publisherService;

    @PostMapping("/publisher")
    public ResponseEntity<Publisher> handleCreatePublisher(@RequestBody Publisher publisher) throws IdInvalidException {
        Publisher currentPublisher = this.publisherService.handleFetchPublisherById(publisher.getId());
        if (currentPublisher != null) {
            throw new IdInvalidException("Publisher already");
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(this.publisherService.handleCreatePublisher(publisher));
    }

    @PutMapping("/publisher")
    public ResponseEntity<Publisher> handleUpdatePublisher(@RequestBody Publisher publisher) throws IdInvalidException {
        Publisher currentPublisher = this.publisherService.handleFetchPublisherById(publisher.getId());
        if (currentPublisher == null) {
            throw new IdInvalidException("Publisher not found");
        }
        return ResponseEntity.ok().body(this.publisherService.handleUpdatePublisher(currentPublisher, publisher));
    }

    @GetMapping("/publisher")
    public ResponseEntity<ResResultPagination> handleFetchAllPublisher(
            @Filter Specification<Publisher> spec, Pageable pageable) {
        return ResponseEntity.ok().body(this.publisherService.handleFetchAllPublisher(spec, pageable));
    }

    @GetMapping("/publisher/{id}")
    public ResponseEntity<Publisher> handleFetchPublisherById(@PathVariable("id") long id) throws IdInvalidException {
        Publisher publisher = this.publisherService.handleFetchPublisherById(id);
        if (publisher == null) {
            throw new IdInvalidException("Publisher not found");
        }
        return ResponseEntity.ok().body(publisher);

    }

}
