package cuongnuyen.BE_Spring.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.turkraft.springfilter.boot.Filter;

import cuongnuyen.BE_Spring.domain.Role;
import cuongnuyen.BE_Spring.domain.response.ResResultPagination;
import cuongnuyen.BE_Spring.domain.response.ResRoleDTO;
import cuongnuyen.BE_Spring.services.RoleService;
import cuongnuyen.BE_Spring.utils.error.IdInvalidException;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class RoleController {
    private final RoleService roleService;

    @PostMapping("/roles")
    public ResponseEntity<ResRoleDTO> handleCreateRole(@RequestBody Role role) throws IdInvalidException {
        Role roleDB = this.roleService.handleFindByName(role.getName());
        if (roleDB != null) {
            if (roleDB.isActive() == true)
                throw new IdInvalidException("role is already");
            else {
                roleDB.setActive(false);
                Role cRole = this.roleService.handleCreateRole(roleDB);
                return ResponseEntity.ok().body(this.roleService.handleConvertRole(cRole));
            }

        }
        Role cRole = this.roleService.handleCreateRole(role);
        return ResponseEntity.ok().body(this.roleService.handleConvertRole(cRole));
    }

    @GetMapping("/roles")
    public ResponseEntity<ResResultPagination> handleFetchAllRole(
            @Filter Specification<Role> spec, Pageable pageable) {
        return ResponseEntity.ok().body(this.roleService.handleFetchAll(spec, pageable));
    }

    @DeleteMapping("/roles/{id}")
    public ResponseEntity<Void> handleDetleteRole(@PathVariable("id") long id) throws IdInvalidException {
        Role role = this.roleService.handleFetchRoleById(id);
        if (role == null) {
            throw new IdInvalidException("role not found!");
        }
        this.roleService.handleDeleteRole(id);
        return ResponseEntity.ok().body(null);
    }

    @PutMapping("/roles")
    public ResponseEntity<ResRoleDTO> handleUpdateRole(@RequestBody Role role) throws IdInvalidException {
        Role currentRole = this.roleService.handleFetchRoleById(role.getId());
        if (currentRole == null) {
            throw new IdInvalidException("role not found!");
        } else {
            if (role.getName() != null) {
                Role roleDB = this.roleService.handleFindByName(role.getName());
                if (roleDB != null) {
                    if (!role.getName().equals(currentRole.getName())) {
                        throw new IdInvalidException("role is already");
                    }
                } else {
                    Role cRole = this.roleService.handleUpdateRole(currentRole, role);
                    return ResponseEntity.ok().body(this.roleService.handleConvertRole(cRole));
                }
            }
            Role cRole = this.roleService.handleUpdateRole(currentRole, role);
            return ResponseEntity.ok().body(this.roleService.handleConvertRole(cRole));
        }
    }

    @GetMapping("/roles/{id}")
    public ResponseEntity<ResRoleDTO> handleFetchRoleById(@PathVariable("id") long id) throws IdInvalidException {
        Role role = this.roleService.handleFetchRoleById(id);
        if (role == null) {
            throw new IdInvalidException("role not found");
        }
        return ResponseEntity.ok().body(this.roleService.handleConvertRole(role));
    }
}
