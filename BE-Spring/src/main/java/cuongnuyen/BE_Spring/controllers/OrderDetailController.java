package cuongnuyen.BE_Spring.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cuongnuyen.BE_Spring.domain.Order;
import cuongnuyen.BE_Spring.domain.OrderDetail;
import cuongnuyen.BE_Spring.domain.response.ResOrderDetailDTO;
import cuongnuyen.BE_Spring.repository.OrderDetailRepository;
import cuongnuyen.BE_Spring.services.OrderDetailService;
import cuongnuyen.BE_Spring.services.OrderService;
import cuongnuyen.BE_Spring.utils.error.IdInvalidException;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class OrderDetailController {
    private final OrderDetailService orderDetailService;
    private final OrderService orderService;

    @GetMapping("/order-detail/order-id/{id}")
    public ResponseEntity<List<ResOrderDetailDTO>> handleFetchOrderDetailByOrder(@PathVariable("id") long id)
            throws IdInvalidException {
        Order order = this.orderService.handleFetchOrderById(id);
        if (order == null) {
            throw new IdInvalidException("không tìm thấy order...");
        }
        List<OrderDetail> details = this.orderDetailService.handleFetchOrderDetailByOrder(order);
        List<ResOrderDetailDTO> detailDTOs = details.stream()
                .map(item -> this.orderDetailService.handleConvertOrderDetail(item)).collect(Collectors.toList());
        return ResponseEntity.ok().body(detailDTOs);
    }
}
