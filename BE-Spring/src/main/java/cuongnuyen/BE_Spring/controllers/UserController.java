package cuongnuyen.BE_Spring.controllers;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.turkraft.springfilter.boot.Filter;

import cuongnuyen.BE_Spring.domain.User;
import cuongnuyen.BE_Spring.domain.response.ResFetchUser;
import cuongnuyen.BE_Spring.domain.response.ResResultPagination;
import cuongnuyen.BE_Spring.services.UserService;
import cuongnuyen.BE_Spring.utils.error.IdInvalidException;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/users")
    public ResponseEntity<ResFetchUser> handleCreateUser(@RequestBody @Valid User user) throws IdInvalidException {
        User user2 = this.userService.fetchUserByEmail(user.getEmail());
        if (user2 != null) {
            throw new IdInvalidException("user already exist");
        }
        User userSave = this.userService.handleCreateUser(user);
        ResFetchUser userConvert = this.userService.convertUser(userSave);
        return ResponseEntity.status(HttpStatus.CREATED).body(userConvert);
    }

    @PutMapping("/users")
    public ResponseEntity<ResFetchUser> handleUpdateUser(@RequestBody @Valid User user) throws IdInvalidException {
        User userCurrent = this.userService.fetchUserById(user.getId());
        if (userCurrent == null) {
            throw new IdInvalidException("user is not exist");
        }
        User userUpdate = this.userService.handleUpdateUser(userCurrent, user);
        ResFetchUser userConvert = this.userService.convertUser(userUpdate);
        return ResponseEntity.ok().body(userConvert);
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Void> handleDeleteUser(@PathVariable("id") long id) throws IdInvalidException {
        User userCurrent = this.userService.fetchUserById(id);
        if (userCurrent == null) {
            throw new IdInvalidException("user is not exist");
        }
        this.userService.handleDeleteUser(userCurrent);
        return ResponseEntity.ok().body(null);
    }

    @GetMapping("/users")
    public ResponseEntity<ResResultPagination> handleFetchAllUser(
            @Filter Specification<User> spec, Pageable pageable) {
        return ResponseEntity.ok().body(this.userService.handleFetchAllUser(spec, pageable));
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<ResFetchUser> handleFetchUserById(@PathVariable("id") long id) throws IdInvalidException {
        User user = this.userService.fetchUserById(id);
        if (user == null) {
            throw new IdInvalidException("not found user");
        }
        return ResponseEntity.ok().body(this.userService.handleFetchUserByIdFormat(user));
    }

}
