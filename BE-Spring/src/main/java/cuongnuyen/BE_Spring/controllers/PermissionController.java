package cuongnuyen.BE_Spring.controllers;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.turkraft.springfilter.boot.Filter;

import cuongnuyen.BE_Spring.domain.Permission;
import cuongnuyen.BE_Spring.domain.response.ResResultPagination;
import cuongnuyen.BE_Spring.services.PermissionService;
import cuongnuyen.BE_Spring.utils.error.IdInvalidException;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class PermissionController {
    private final PermissionService permissionService;

    @PostMapping("/permissions")
    public ResponseEntity<Permission> handleCreatePermission(@RequestBody Permission permission)
            throws IdInvalidException {
        Permission currentPermission = this.permissionService.handleFindPermissionExist(permission);
        if (currentPermission != null) {
            throw new IdInvalidException("permission allready!");
        }
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(this.permissionService.handleCreatePermission(permission));
    }

    @GetMapping("/permissions")
    public ResponseEntity<ResResultPagination> handleFetchAllPermission(
            @Filter Specification<Permission> spec, Pageable pageable) {
        return ResponseEntity.ok().body(this.permissionService.handleFetchAllPermission(spec, pageable));
    }

    @DeleteMapping("/permissions/{id}")
    public ResponseEntity<Void> handleDeletePermission(@PathVariable("id") long id) throws IdInvalidException {
        Permission permission = this.permissionService.handleFindById(id);
        if (permission == null) {
            throw new IdInvalidException("permission not found");
        }
        this.permissionService.handleDeletePermission(id);
        return ResponseEntity.ok().body(null);
    }

    @PutMapping("/permissions")
    public ResponseEntity<Permission> handleUpdatePermission(@RequestBody Permission permission)
            throws IdInvalidException {
        Permission permissionDB = this.permissionService.handleFindById(permission.getId());
        if (permissionDB == null) {
            throw new IdInvalidException("permission not found");
        } else {
            Permission currentPermission = this.permissionService.handleFindPermissionExist(permission);
            if (currentPermission != null) {
                if (permission.getName().equals(currentPermission.getName())) {
                    throw new IdInvalidException("permission is already!");
                }
            }
            return ResponseEntity.ok().body(this.permissionService.handleUpdatePermission(permission, permissionDB));
        }

    }
}
