package cuongnuyen.BE_Spring.controllers;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.turkraft.springfilter.boot.Filter;

import cuongnuyen.BE_Spring.domain.Order;
import cuongnuyen.BE_Spring.domain.response.ResStatisticalOrderDTO;
import cuongnuyen.BE_Spring.services.StatisticalService;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class StatisticalController {
    private final StatisticalService statisticalService;

    @GetMapping("/statistical-order")
    public ResponseEntity<ResStatisticalOrderDTO> handleFetchOrderStatistical(
            @Filter Specification<Order> spec) {
        List<Order> orders = this.statisticalService.handleStatisticalOrder(spec);
        return ResponseEntity.ok().body(this.statisticalService.handleConvertStatisticalOrder(orders));
    }
}
