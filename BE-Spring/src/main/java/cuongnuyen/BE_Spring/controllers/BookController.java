package cuongnuyen.BE_Spring.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.turkraft.springfilter.boot.Filter;

import cuongnuyen.BE_Spring.domain.Author;
import cuongnuyen.BE_Spring.domain.Book;
import cuongnuyen.BE_Spring.domain.Publisher;
import cuongnuyen.BE_Spring.domain.response.ResBookDTO;
import cuongnuyen.BE_Spring.domain.response.ResCategoryDTO;
import cuongnuyen.BE_Spring.domain.response.ResResultPagination;
import cuongnuyen.BE_Spring.services.AuthorService;
import cuongnuyen.BE_Spring.services.BookService;
import cuongnuyen.BE_Spring.services.PublisherService;
import cuongnuyen.BE_Spring.utils.error.IdInvalidException;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class BookController {
    private final BookService bookService;
    private final AuthorService authorService;
    private final PublisherService publisherService;

    @PostMapping("/books")
    public ResponseEntity<ResBookDTO> handleCreateBook(@RequestBody Book book) throws IdInvalidException {
        Book bookDB = this.bookService.handleFetchBookByName(book.getName());
        Author author = this.authorService.handleFetchAuthorById(book.getAuthor().getId());
        Publisher publisher = this.publisherService.handleFetchPublisherById(book.getPublisher().getId());
        if (bookDB != null) {
            throw new IdInvalidException("book is already");
        }
        if (author == null) {
            throw new IdInvalidException("author not found");
        }
        if (publisher == null) {
            throw new IdInvalidException("publisher not found");
        }
        book.setAuthor(author);
        book.setPublisher(publisher);
        Book createBook = this.bookService.handleCreateBook(book);
        return ResponseEntity.status(HttpStatus.CREATED).body(this.bookService.convertBookDTO(createBook));
    }

    @GetMapping("/books")
    public ResponseEntity<ResResultPagination> handleFetchAllBook(
            @Filter Specification<Book> spec, Pageable pageable) {
        return ResponseEntity.ok().body(this.bookService.handleFetchAllBook(spec, pageable));
    }

    @PutMapping("/books")
    public ResponseEntity<ResBookDTO> handleUpdateBook(@RequestBody Book book) throws IdInvalidException {
        Book bookById = this.bookService.handleFetchBookById(book.getId());
        Author author = this.authorService.handleFetchAuthorById(book.getAuthor().getId());
        Publisher publisher = this.publisherService.handleFetchPublisherById(book.getPublisher().getId());
        if (bookById == null) {
            throw new IdInvalidException("book is not found");
        }
        if (book.getName() != null) {
            Book bookDB = this.bookService.handleFetchBookByName(book.getName());
            if (bookDB != null) {
                if (!bookDB.getName().equals(bookById.getName())) {
                    throw new IdInvalidException("book is already");
                }
            }
        }
        if (author == null) {
            throw new IdInvalidException("author not found");
        }
        if (publisher == null) {
            throw new IdInvalidException("publisher not found");
        }
        book.setAuthor(author);
        book.setPublisher(publisher);
        Book updateBook = this.bookService.handleUpdateBook(bookById, book);
        return ResponseEntity.ok().body(this.bookService.convertBookDTO(updateBook));
    }

    @GetMapping("/books/{id}")
    public ResponseEntity<ResBookDTO> handleFetchBookById(@PathVariable("id") long id) throws IdInvalidException {
        Book book = this.bookService.handleFetchBookById(id);
        if (book == null) {
            throw new IdInvalidException("Book is not found");
        }
        return ResponseEntity.ok().body(this.bookService.convertBookDTO(book));
    }

    @GetMapping("/books/categories")
    public ResponseEntity<List<ResCategoryDTO>> handleFetchAllCategories() throws IdInvalidException {
        return ResponseEntity.ok().body(this.bookService.handleFetchAllCategory());
    }

}
