package cuongnuyen.BE_Spring.controllers;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.turkraft.springfilter.boot.Filter;

import cuongnuyen.BE_Spring.domain.Author;
import cuongnuyen.BE_Spring.domain.response.ResResultPagination;
import cuongnuyen.BE_Spring.services.AuthorService;
import cuongnuyen.BE_Spring.utils.error.IdInvalidException;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class AuthorController {
    private final AuthorService authorService;

    @PostMapping("/author")
    public ResponseEntity<Author> handleCreateAuthor(@RequestBody Author author) throws IdInvalidException {
        Author currentAuthor = this.authorService.handleFetchAuthorById(author.getId());
        if (currentAuthor != null) {
            throw new IdInvalidException("author already");
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(this.authorService.handleCreateAuthor(author));
    }

    @PutMapping("/author")
    public ResponseEntity<Author> handleUpdateAuthor(@RequestBody Author author) throws IdInvalidException {
        Author currentAuthor = this.authorService.handleFetchAuthorById(author.getId());
        if (currentAuthor == null) {
            throw new IdInvalidException("author not found");
        }
        return ResponseEntity.ok().body(this.authorService.handleUpdateAuthor(currentAuthor, author));
    }

    @GetMapping("/author")
    public ResponseEntity<ResResultPagination> handleFetchAllAuthor(
            @Filter Specification<Author> spec, Pageable pageable) {
        return ResponseEntity.ok().body(this.authorService.handleFetchAllAuthor(spec, pageable));
    }

    @GetMapping("/author/{id}")
    public ResponseEntity<Author> handleFetchAuthorById(@PathVariable("id") long id) throws IdInvalidException {
        Author author = this.authorService.handleFetchAuthorById(id);
        if (author == null) {
            throw new IdInvalidException("author not found");
        }
        return ResponseEntity.ok().body(author);

    }
}
