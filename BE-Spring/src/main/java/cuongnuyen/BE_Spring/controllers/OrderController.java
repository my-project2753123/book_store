package cuongnuyen.BE_Spring.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.turkraft.springfilter.boot.Filter;

import cuongnuyen.BE_Spring.domain.Book;
import cuongnuyen.BE_Spring.domain.Order;
import cuongnuyen.BE_Spring.domain.OrderDetail;
import cuongnuyen.BE_Spring.domain.User;
import cuongnuyen.BE_Spring.domain.response.ResOrderDTO;
import cuongnuyen.BE_Spring.domain.response.ResResultPagination;
import cuongnuyen.BE_Spring.services.BookService;
import cuongnuyen.BE_Spring.services.OrderService;
import cuongnuyen.BE_Spring.services.UserService;
import cuongnuyen.BE_Spring.utils.enums.OrderEnum;
import cuongnuyen.BE_Spring.utils.error.IdInvalidException;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class OrderController {
    private final OrderService orderService;
    private final UserService userService;
    private final BookService bookService;

    @PostMapping("/orders")
    public ResponseEntity<ResOrderDTO> handleCreateOrder(@RequestBody Order order) throws IdInvalidException {
        if (order.getDetails().size() <= 0) {
            throw new IdInvalidException("vui lòng thêm sản phẩm");
        } else {
            List<OrderDetail> details = order.getDetails();
            if (details.size() > 0) {
                for (OrderDetail od : details) {
                    Book book = this.bookService.handleFetchBookById(od.getBook().getId());
                    if (book.getQuantity() < od.getQuantity()) {
                        throw new IdInvalidException("số lượng không đủ");
                    }
                }
            }
            Order orderCurrent = this.orderService.handleCreateOrder(order);
            return ResponseEntity.status(HttpStatus.CREATED).body(this.orderService.handleConvertOrder(orderCurrent));
        }
    }

    @GetMapping("/orders")
    public ResponseEntity<ResResultPagination> handleFetchAllOrder(
            @Filter Specification<Order> spec, Pageable pageable) {
        return ResponseEntity.ok().body(this.orderService.handleFetchAllOrder(spec, pageable));
    }

    @PutMapping("/orders")
    public ResponseEntity<ResOrderDTO> handleUpdateOrder(@RequestBody Order order) throws IdInvalidException {
        if (order.getDetails().size() <= 0) {
            throw new IdInvalidException("Vui lòng thêm sản phẩm");
        } else {
            Order orderCurrent = this.orderService.handleFetchOrderById(order.getId());
            if (orderCurrent == null) {
                throw new IdInvalidException("Không tìm thấy order");
            } else {
                if (orderCurrent.getStatus().equals(OrderEnum.PAID)) {
                    throw new IdInvalidException("Hóa đơn đã được thanh toán, không thể sửa");
                }
                if (order.getStatus().equals(OrderEnum.PAID)) {
                    List<OrderDetail> activeDetails = order.getDetails().stream()
                            .filter(OrderDetail::isActive)
                            .collect(Collectors.toList());

                    if (activeDetails.size() > 0) {
                        for (OrderDetail od : activeDetails) {
                            Book book = this.bookService.handleFetchBookById(od.getBook().getId());
                            if (book.getQuantity() < od.getQuantity()) {
                                throw new IdInvalidException("Số lượng không đủ");
                            }
                        }
                    } else {
                        throw new IdInvalidException("Số lượng không đủ");
                    }
                }

                Order orderUpdate = this.orderService.handleUpdateOrder(order, orderCurrent);

                return ResponseEntity.ok().body(this.orderService.handleConvertOrder(orderUpdate));
            }
        }
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Void> handleDeleteOrder(@PathVariable("id") long id) throws IdInvalidException {
        Order order = this.orderService.handleFetchOrderById(id);
        if (order == null) {
            throw new IdInvalidException("not found order");
        }
        this.orderService.handleDeleteOrder(order);
        return ResponseEntity.ok().body(null);
    }

    @GetMapping("/orders/paid/{id}")
    public ResponseEntity<ResOrderDTO> handlePaidOrder(@PathVariable("id") long id) throws IdInvalidException {
        Order order = this.orderService.handleFetchOrderById(id);
        if (order == null) {
            throw new IdInvalidException("not found order");
        }
        if (order.getStatus().equals(OrderEnum.PAID)) {
            throw new IdInvalidException("hóa đơn đã được thanh toán");
        }
        for (OrderDetail od : order.getDetails().stream()
                .filter(item -> item.isActive())
                .collect(Collectors.toList())) {
            Book book = this.bookService.handleFetchBookById(od.getBook().getId());
            if (book.getQuantity() < od.getQuantity()) {
                throw new IdInvalidException("số lượng không đủ");
            }
        }
        Order orderChange = this.orderService.handlePaid(order);
        return ResponseEntity.ok().body(this.orderService.handleConvertOrder(orderChange));
    }

    @GetMapping("/orders/paid-transfer/{id}")
    public ResponseEntity<ResOrderDTO> handleTransFerPaidOrder(@PathVariable("id") long id) throws IdInvalidException {
        Order order = this.orderService.handleFetchOrderById(id);
        if (order == null) {
            throw new IdInvalidException("not found order");
        }
        if (order.getStatus().equals(OrderEnum.PAID)) {
            throw new IdInvalidException("hóa đơn đã được thanh toán");
        }
        for (OrderDetail od : order.getDetails().stream()
                .filter(item -> item.isActive())
                .collect(Collectors.toList())) {
            Book book = this.bookService.handleFetchBookById(od.getBook().getId());
            if (book.getQuantity() < od.getQuantity()) {
                throw new IdInvalidException("số lượng không đủ");
            }
        }
        Order orderChange = this.orderService.handleTransFerPaid(order);
        return ResponseEntity.ok().body(this.orderService.handleConvertOrder(orderChange));
    }

    @GetMapping("/orders/unpaid/{id}")
    public ResponseEntity<ResOrderDTO> handleUnpaidOrder(@PathVariable("id") long id) throws IdInvalidException {
        Order order = this.orderService.handleFetchOrderById(id);
        if (order == null) {
            throw new IdInvalidException("not found order");
        }
        Order orderChange = this.orderService.handleUnpaid(order);
        return ResponseEntity.ok().body(this.orderService.handleConvertOrder(orderChange));
    }

    @PostMapping("/orders/admin")
    public ResponseEntity<ResOrderDTO> handleCreateOrderAD(@RequestBody Order order) throws IdInvalidException {
        if (order.getDetails().size() <= 0) {
            throw new IdInvalidException("vui lòng thêm sản phẩm");
        } else {
            if (this.userService.fetchUserById(order.getUser().getId()) == null) {
                throw new IdInvalidException("User không tồn tại");
            }
            for (OrderDetail od : order.getDetails().stream()
                    .filter(item -> item.isActive())
                    .collect(Collectors.toList())) {
                Book book = this.bookService.handleFetchBookById(od.getBook().getId());
                if (book.getQuantity() < od.getQuantity()) {
                    throw new IdInvalidException("số lượng không đủ");
                }
            }
            Order orderCurrent = this.orderService.handleCreateOrderAD(order);
            return ResponseEntity.status(HttpStatus.CREATED).body(this.orderService.handleConvertOrder(orderCurrent));
        }
    }

    @GetMapping("/orders/user-active/{id}")
    public ResponseEntity<List<ResOrderDTO>> handleUserOrderActive(@PathVariable("id") long id)
            throws IdInvalidException {
        User user = this.userService.handleFetchUserById(id);
        if (user == null) {
            throw new IdInvalidException("Không tìm thấy user");
        }
        List<ResOrderDTO> orders = this.orderService.handleOrderByUserActive(user);
        return ResponseEntity.ok().body(orders);
    }

    @GetMapping("/orders/user-inactive/{id}")
    public ResponseEntity<List<ResOrderDTO>> handleUserOrderInactive(@PathVariable("id") long id)
            throws IdInvalidException {
        User user = this.userService.handleFetchUserById(id);
        if (user == null) {
            throw new IdInvalidException("Không tìm thấy user");
        }
        List<ResOrderDTO> orders = this.orderService.handleOrderByUserInactive(user);
        return ResponseEntity.ok().body(orders);
    }

}
