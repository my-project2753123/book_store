package cuongnuyen.BE_Spring.controllers.paypal;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cuongnuyen.BE_Spring.domain.CompletedOrder;
import cuongnuyen.BE_Spring.domain.PayPalCheckOut;
import cuongnuyen.BE_Spring.domain.PaymentOrder;
import cuongnuyen.BE_Spring.services.PaypalService;

@RestController
@RequestMapping("/api/v1")
public class PayPalController {

    @Autowired
    private PaypalService service;

    @PostMapping(value = "/init")
    public PaymentOrder createPayment(
            @RequestBody PayPalCheckOut checkOut) {
        return service.createPayment(checkOut.getSum());
    }

    @PostMapping(value = "/capture")
    public CompletedOrder completePayment(@RequestParam("token") String token) {
        return service.completePayment(token);
    }
}
