import { defineConfig, loadEnv } from 'vite'
import react from '@vitejs/plugin-react-swc'
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
  const env = loadEnv(mode, process.cwd(), '');

  return {
    plugins: [react()],
    server: {
      port: parseInt(env.PORT)
    },
    resolve: {
      alias: {
        "@": path.resolve(__dirname, "./src"),
        components: `${path.resolve(__dirname, "./src/components")}`,
        config: path.resolve(__dirname, "./src/config"),
        pages: path.resolve(__dirname, "./src/pages"),
        // redux: path.resolve(__dirname, "./src/redux"),
        styles: path.resolve(__dirname, "./src/styles"),
        types: path.resolve(__dirname, "./src/types"),
        assets: path.resolve(__dirname, "./src/assets"),
      }
    }

  }
})
