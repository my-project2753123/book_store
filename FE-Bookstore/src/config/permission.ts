export const ALL_PERMISSIONS = {
    PERMISSIONS: {
        GET_PAGINATE: { method: "GET", apiPath: '/api/v1/permissions', module: "PERMISSIONS" },
        CREATE: { method: "POST", apiPath: '/api/v1/permissions', module: "PERMISSIONS" },
        UPDATE: { method: "PUT", apiPath: '/api/v1/permissions', module: "PERMISSIONS" },
        DELETE: { method: "DELETE", apiPath: '/api/v1/permissions/{id}', module: "PERMISSIONS" },
    },
    ROLES: {
        GET_PAGINATE: { method: "GET", apiPath: '/api/v1/roles', module: "ROLES" },
        CREATE: { method: "POST", apiPath: '/api/v1/roles', module: "ROLES" },
        UPDATE: { method: "PUT", apiPath: '/api/v1/roles', module: "ROLES" },
        DELETE: { method: "DELETE", apiPath: '/api/v1/roles/{id}', module: "ROLES" },
    },
    USERS: {
        GET_PAGINATE: { method: "GET", apiPath: '/api/v1/users', module: "USERS" },
        CREATE: { method: "POST", apiPath: '/api/v1/users', module: "USERS" },
        UPDATE: { method: "PUT", apiPath: '/api/v1/users', module: "USERS" },
        DELETE: { method: "DELETE", apiPath: '/api/v1/users/{id}', module: "USERS" },
    },
    BOOKS: {
        GET_PAGINATE: { method: "GET", apiPath: '/api/v1/books', module: "BOOKS" },
        CREATE: { method: "POST", apiPath: '/api/v1/books', module: "BOOKS" },
        UPDATE: { method: "PUT", apiPath: '/api/v1/books', module: "BOOKS" },
        DELETE: { method: "DELETE", apiPath: '/api/v1/books/{id}', module: "BOOKS" },
    },
    AUTHOR: {
        GET_PAGINATE: { method: "GET", apiPath: '/api/v1/author', module: "AUTHOR" },
        CREATE: { method: "POST", apiPath: '/api/v1/author', module: "AUTHOR" },
        UPDATE: { method: "PUT", apiPath: '/api/v1/author', module: "AUTHOR" },
        DELETE: { method: "DELETE", apiPath: '/api/v1/author/{id}', module: "AUTHOR" },
    },
    PUBLISHER: {
        GET_PAGINATE: { method: "GET", apiPath: '/api/v1/publisher', module: "PUBLISHER" },
        CREATE: { method: "POST", apiPath: '/api/v1/publisher', module: "PUBLISHER" },
        UPDATE: { method: "PUT", apiPath: '/api/v1/publisher', module: "PUBLISHER" },
        DELETE: { method: "DELETE", apiPath: '/api/v1/publisher/{id}', module: "PUBLISHER" },
    },
    ORDER: {
        GET_PAGINATE: { method: "GET", apiPath: '/api/v1/orders', module: "ORDERS" },
        CREATE: { method: "POST", apiPath: '/api/v1/orders/admin', module: "ORDERS" },
        UPDATE: { method: "PUT", apiPath: '/api/v1/orders', module: "ORDERS" },
        DELETE: { method: "DELETE", apiPath: '/api/v1/orders/{id}', module: "ORDERS" },
    },
}