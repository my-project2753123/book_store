import book1980 from 'assets/image/1980books.png.webp';
import bookstore from 'assets/image/bookstore.jpg.webp';
import fahasa from 'assets/image/fahasa.jpg.webp';
import finfin from 'assets/image/finfin.jpg.webp';
import giver from 'assets/image/giver.jpg.webp';
import hocmai from 'assets/image/hocmai.png.webp';
import megabook from 'assets/image/megabook.png.webp';
import saigonbooks from 'assets/image/saigonbooks.jpg.webp';
import sbooks from 'assets/image/sbooks.jpg.webp';
import sthm from 'assets/image/sthm.jpg.webp';

export const BANNER = [
    {
        text1: "Bộ Sưu Tập Sách Mới Giảm Đến 30%",
        text2: "Tài trợ bởi 1980 Books Tại TiKi Trading",
        color: "rgb(145 179 152)",
        img: `${book1980}`
    },
    {
        text1: "Top 10 Sách Kinh Doanh: Dễ Đọc, Dễ Hiểu, Dễ Áp Dụng!",
        text2: "Tài trợ bởi GIVER Tại TiKi Trading",
        color: "rgb(139 159 201)",
        img: `${giver}`
    },
    {
        text1: "Giảm đến 50%",
        text2: "Tài trợ bởi nhà sách văn lang",
        color: "rgb(252 251 252)",
        img: `${bookstore}`
    },
    {
        text1: "HOCMAI",
        text2: "Tài trợ bởi HOCMAI",
        color: "rgb(204 224 239)",
        img: `${hocmai}`
    },
    {
        text1: "Đầu tư đúng cách với FinFin",
        text2: "Tài trợ bởi FinFin",
        color: "rgb(217 214 190)",
        img: `${finfin}`
    },
    {
        text1: "Sbooks",
        text2: "Tài trợ bởi Sbooks",
        color: "rgb(212 220 130)",
        img: `${sbooks}`
    },
    {
        text1: "TIKI TRADING SÀI GÒN BOOKS",
        text2: "Tài trợ bởi SÀI GÒN BOOKS",
        color: "rgb(249 251 252)",
        img: `${saigonbooks}`
    },
    {
        text1: "STHM stationery",
        text2: "Tài trợ bởi SOTAYHANDMADEVN",
        color: "rgb(246 243 233)",
        img: `${sthm}`
    },
    {
        text1: "Nhà sách FAHASA",
        text2: "Tài trợ bởi Nhà sách FAHASA",
        color: "rgb(224 171 185)",
        img: `${fahasa}`
    },
    {
        text1: "Nhà sách MegaBook",
        text2: "Tài trợ bởi Nhà sách MegaBook",
        color: "rgb(224 171 185)",
        img: `${megabook}`
    }
]