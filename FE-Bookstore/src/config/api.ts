import { IAccount, IAuthor, IBackendRes, ICategory, IGetAccount, IOrder, IOrderAD, IOrderDetailAD, IPagination, IPermission, IProduct, IPublisher, IRole, IStatisticalOrder, IUser } from '@/types/backend';
import axios from './axios-customize';

//api auth

export const callRegister = (name: string, email: string, password: string, age: number, gender: string, address: string) => {
    return axios.post<IBackendRes<IUser>>('/api/v1/auth/register', { name, email, password, age, gender, address })
}

export const callLogin = (email: string, password: string) => {
    return axios.post<IBackendRes<IAccount>>("/api/v1/auth/login", { email, password });
}

export const callLogout = () => {
    return axios.get<IBackendRes<void>>("/api/v1/auth/logout");
}

//api user

export const callFetchAccount = () => {
    return axios.get<IBackendRes<IGetAccount>>("/api/v1/auth/account");
}

export const callFetchAllUser = (query: string) => {
    return axios.get<IBackendRes<IPagination<IUser>>>(`/api/v1/users?${query}`);
}

export const callCreateUser = (user: IUser) => {
    return axios.post<IBackendRes<IUser>>("/api/v1/users", { ...user });
}

export const callDeleteUser = (id: number) => {
    return axios.delete<IBackendRes<void>>(`/api/v1/users/${id}`);
}

export const callUpdateUser = (user: IUser) => {
    return axios.put<IBackendRes<IUser>>("/api/v1/users", { ...user });
}

export const callUserById = (id: number) => {
    return axios.get<IBackendRes<IUser>>(`/api/v1/users/${id}`);
}


//api permissions

export const callFetchAllPermission = (query: string) => {
    return axios.get<IBackendRes<IPagination<IPermission>>>(`/api/v1/permissions?${query}`);
}

export const callCreatePermission = (name: string, apiPath: string, module: string, method: string) => {
    return axios.post<IBackendRes<IPermission>>("api/v1/permissions", { name, apiPath, module, method })
}

export const callUpdatePermission = (id: number, name: string, apiPath: string, module: string, method: string) => {
    return axios.put<IBackendRes<IPermission>>("api/v1/permissions", { id, name, apiPath, module, method })
}

export const callDeletePermission = (id: number) => {
    return axios.delete<IBackendRes<void>>(`api/v1/permissions/${id}`)
}

//api role

export const callFetchAllRole = (query: string) => {
    return axios.get<IBackendRes<IPagination<IRole>>>(`/api/v1/roles?${query}`);
}

export const callCreateRole = (role: IRole) => {
    return axios.post<IBackendRes<IRole>>("api/v1/roles", { ...role })
}

export const callUpdateRole = (role: IRole, id: string) => {
    return axios.put<IBackendRes<IPermission>>("api/v1/roles", { id, ...role })
}

export const callDeleteRole = (id: number) => {
    return axios.delete<IBackendRes<void>>(`api/v1/roles/${id}`)
}

export const callFetchRoleById = (id: number) => {
    return axios.get<IBackendRes<void>>(`api/v1/roles/${id}`)
}

//api author

export const callCreateAuthor = (author: IAuthor) => {
    return axios.post<IBackendRes<IAuthor>>("api/v1/author", { ...author })
}

export const callUpdateAuthor = (author: IAuthor) => {
    return axios.put<IBackendRes<IAuthor>>("api/v1/author", { ...author })
}

export const callFetchAllAuthor = (query: string) => {
    return axios.get<IBackendRes<IPagination<IAuthor>>>(`api/v1/author?${query}`)
}

export const callFetchAuthorById = (id: number) => {
    return axios.get<IBackendRes<IAuthor>>(`api/v1/author/${id}`)
}

//api publisher

export const callCreatePublisher = (publisher: IPublisher) => {
    return axios.post<IBackendRes<IPublisher>>("api/v1/publisher", { ...publisher })
}

export const callUpdatePublisher = (publisher: IPublisher) => {
    return axios.put<IBackendRes<IPublisher>>("api/v1/publisher", { ...publisher })
}

export const callFetchAllPublisher = (query: string) => {
    return axios.get<IBackendRes<IPagination<IPublisher>>>(`api/v1/publisher?${query}`)
}

export const callFetchPublisherById = (id: number) => {
    return axios.get<IBackendRes<IPublisher>>(`api/v1/publisher/${id}`)
}

//api product

export const callCreateProduct = (product: IProduct) => {
    return axios.post<IBackendRes<IProduct>>("api/v1/books", { ...product })
}

export const callUpdateProduct = (product: IProduct) => {
    return axios.put<IBackendRes<IProduct>>("api/v1/books", { ...product })
}

export const callFetchAllProduct = (query: string) => {
    return axios.get<IBackendRes<IPagination<IProduct>>>(`api/v1/books?${query}`)
}

export const callDeleteProduct = (id: string) => {
    return axios.delete<IBackendRes<void>>(`api/v1/books/${id}`)
}

export const callFetchProductById = (id: string) => {
    return axios.get<IBackendRes<IProduct>>(`api/v1/books/${id}`)
}

export const callFetchAllCategories = () => {
    return axios.get<IBackendRes<ICategory[]>>(`api/v1/books/categories`)

}

//order

export const callCreateOrder = (data: IOrder) => {
    return axios.post<IBackendRes<IOrder>>("api/v1/orders", { ...data })
}

export const callFetchAllOrder = (query: string) => {
    return axios.get<IBackendRes<IPagination<IOrderAD>>>(`api/v1/orders?${query}`)
}

export const callPaiDOrder = (id: string) => {
    return axios.get<IBackendRes<IOrderAD>>(`api/v1/orders/paid/${id}`)
}

export const callTransferPaiDOrder = (id: string) => {
    return axios.get<IBackendRes<IOrderAD>>(`api/v1/orders/paid-transfer/${id}`)
}


export const callDeleteOrder = (id: string) => {
    return axios.delete<IBackendRes<void>>(`api/v1/orders/${id}`)
}

export const callCreateOrderAD = (data: IOrder) => {
    return axios.post<IBackendRes<IOrderAD>>("api/v1/orders/admin", { ...data })
}

export const callUpdateOrderAD = (data: IOrder) => {
    return axios.put<IBackendRes<IOrderAD>>("api/v1/orders", { ...data })
}

export const callUserOrderActive = (id: string) => {
    return axios.get<IBackendRes<IOrderAD[]>>(`api/v1/orders/user-active/${id}`)
}

export const callUserOrderInactive = (id: string) => {
    return axios.get<IBackendRes<IOrderAD[]>>(`api/v1/orders/user-inactive/${id}`)
}

//order detail

export const callFetchOrderDetailByOrder = (id: string) => {
    return axios.get<IBackendRes<IOrderDetailAD[]>>(`api/v1/order-detail/order-id/${id}`)
}

//order statistical

export const callFetchStatisticalOrder = (query: string) => {
    return axios.get<IBackendRes<IStatisticalOrder>>(`api/v1/statistical-order?${query}`)
}

// upload file
export const callUploadSingleFile = (file: any, folderType: string) => {
    const bodyFormData = new FormData();
    bodyFormData.append('file', file);
    bodyFormData.append('folder', folderType);

    return axios<IBackendRes<{ fileName: string }>>({
        method: 'post',
        url: '/api/v1/files',
        data: bodyFormData,
        headers: {
            "Content-Type": "multipart/form-data",
        },
    });
}
