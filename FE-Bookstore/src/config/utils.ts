import { grey, green, blue, red, orange } from '@ant-design/colors';

export function colorMethod(method: "POST" | "PUT" | "GET" | "DELETE" | string) {
    switch (method) {
        case "POST":
            return green[6]
        case "PUT":
            return orange[6]
        case "GET":
            return blue[6]
        case "DELETE":
            return red[6]
        case "PAID":
            return green[6]
        case "UNPAID":
            return red[6]
        case "DIRECT":
            return orange[6]
        case "TRANSFER":
            return blue[6]
        default:
            return grey[10];
    }
}

export const responsive = {
    superLargeDesktop: {
        breakpoint: { max: 4000, min: 3000 },
        items: 2
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 2
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 1
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1
    }
};

export const CarouselCategory = {
    superLargeDesktop: {
        breakpoint: { max: 4000, min: 3000 },
        items: 4
    },
    desktop: {
        breakpoint: { max: 3000, min: 1024 },
        items: 4
    },
    tablet: {
        breakpoint: { max: 1024, min: 820 },
        items: 3
    },
    mobile: {
        breakpoint: { max: 820, min: 0 },
        items: 2
    }
};


