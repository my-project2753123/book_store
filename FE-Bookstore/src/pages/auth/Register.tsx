import { useState } from 'react';
import { Button, Card, Col, Divider, Form, Input, Select, message, notification } from 'antd';
import 'styles/auth.scss'
import NProgress from "nprogress";
import { useNavigate } from 'react-router-dom';
import { IUser } from '@/types/backend';
import { callRegister } from '@/config/api';
const { Option } = Select;

const Register = () => {

    const navigate = useNavigate();
    const [isSubmit, setIsSubmit] = useState<boolean>(false);

    const handleLoadStart = () => {
        NProgress.start();

        setTimeout(() => {
            NProgress.done();
            navigate("/login");
        }, 1000);
    }

    const onFinish = async (value: IUser) => {
        const { name, email, password, gender, age, address } = value;
        setIsSubmit(true)
        const res = await callRegister(name, email, password as string, +age, gender, address);
        setIsSubmit(false)
        if (res?.data?.id) {
            message.success("Đăng ký tài khoản thành công");
            navigate("/login")
        } else {
            notification.error({
                message: "có lỗi xảy ra",
                description:
                    res?.message && Array.isArray(res?.message) ? res?.message[0] : res?.message, duration: 5
            })
        }
    }


    return (
        <div className='auth-container'>
            <div className='main'>
                <div className='auth-content'>
                    <Col span={24} md={24} xs={20} >
                        <Card>
                            <div>
                                <h2 className='text-title'>Đăng Ký</h2>
                                <Divider />
                            </div>
                            <Form<IUser>
                                name="basic"
                                onFinish={onFinish}
                                autoComplete="off"
                            >
                                <Form.Item
                                    labelCol={{ span: 24 }} //whole column
                                    label="Họ tên"
                                    name="name"
                                    rules={[{ required: true, message: 'Họ và tên không được để trống!' }]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    labelCol={{ span: 24 }} //whole column
                                    label="Email"
                                    name="email"
                                    rules={[{ required: true, message: 'Email không được để trống!' }]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    labelCol={{ span: 24 }} //whole column
                                    label="Mật khẩu"
                                    name="password"
                                    rules={[{ required: true, message: 'Mật khẩu không được để trống!' }]}
                                >
                                    <Input.Password />
                                </Form.Item>


                                <Form.Item
                                    labelCol={{ span: 24 }} //whole column
                                    label="Tuổi"
                                    name="age"
                                    rules={[{ required: true, message: 'Tuổi không để trống!' }]}
                                >
                                    <Input type='number' />
                                </Form.Item>


                                <Form.Item
                                    labelCol={{ span: 24 }} //whole column
                                    label="Giới tính"
                                    name="gender"
                                    rules={[{ required: true, message: 'Giới tính không để trống!' }]}
                                >
                                    <Select
                                        // placeholder="Select a option and change input text above"
                                        // onChange={onGenderChange}
                                        allowClear
                                    >
                                        <Option value="MALE">Nam</Option>
                                        <Option value="FEMALE">Nữ</Option>
                                        <Option value="OTHER">Khác</Option>
                                    </Select>

                                </Form.Item>

                                <Form.Item
                                    labelCol={{ span: 24 }} //whole column
                                    label="Địa chỉ"
                                    name="address"
                                    rules={[{ required: true, message: 'Địa chỉ không để trống!' }]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                // wrapperCol={{ offset: 6, span: 16 }}
                                >
                                    <Button type="primary" htmlType="submit"
                                        loading={isSubmit}
                                    >
                                        Đăng ký
                                    </Button>
                                </Form.Item>
                                <Divider>Or</Divider>
                                <p className="text text-normal">Đã có tài khoản ?
                                    <span className='link' onClick={() => handleLoadStart()}>
                                        &nbsp;Đăng nhập
                                    </span>
                                </p>
                            </Form>
                        </Card>
                    </Col>

                </div>
            </div>
        </div>
    );
};

export default Register;