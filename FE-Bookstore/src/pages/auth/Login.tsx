import { Button, Card, Col, Divider, Form, Input, message, notification } from 'antd';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import 'styles/auth.scss'
import NProgress from "nprogress";
import { callLogin } from '@/config/api';
import { useDispatch } from 'react-redux';
import { setUserLoginInfo } from '@/redux/slide/authSlide';

const Login = () => {

    const navigate = useNavigate();
    const [isSubmit, setIsSubmit] = useState<boolean>(false);
    const dispatch = useDispatch();

    const handleLoadStart = () => {
        NProgress.start();

        setTimeout(() => {
            NProgress.done();
            navigate("/register");
        }, 1000);
    }

    const onFinish = async (value: any) => {
        const { email, password } = value;
        setIsSubmit(true)
        const res = await callLogin(email, password);
        if (res?.data?.user?.id) {
            setIsSubmit(false)
            localStorage.setItem('access_token', res?.data?.access_token as string)
            dispatch(setUserLoginInfo(res?.data?.user))
            message.success('Đăng nhập tài khoản thành công!');
            navigate("/")
        }
        else {
            setIsSubmit(false)
            notification.error({
                message: "Có lỗi xảy ra",
                description:
                    res?.message && Array.isArray(res?.message) ? res?.message[0] : res?.message,
                duration: 5
            })
        }

    }

    return (
        <div className='auth-container'>
            <div className='main-login'>

                <div className='auth-content'>
                    <Col span={24} md={24} xs={20} >
                        <Card>
                            <div>
                                <h2 className='text-title'>Đăng Nhập</h2>
                                <Divider />
                            </div>
                            <Form
                                name="basic"
                                onFinish={onFinish}
                                autoComplete="off"
                            >
                                <Form.Item
                                    labelCol={{ span: 24 }}
                                    label="Email"
                                    name="email"
                                    rules={[{ required: true, message: 'Email không được để trống!' }]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    labelCol={{ span: 24 }}
                                    label="Mật khẩu"
                                    name="password"
                                    rules={[{ required: true, message: 'Mật khẩu không được để trống!' }]}
                                >
                                    <Input.Password />
                                </Form.Item>

                                <Form.Item
                                // wrapperCol={{ offset: 6, span: 16 }}
                                >
                                    <Button type="primary" htmlType="submit"
                                        loading={isSubmit}
                                    >
                                        Đăng nhập
                                    </Button>
                                </Form.Item>
                                <Divider>Or</Divider>
                                <p className="text text-normal">Chưa có tài khoản ?
                                    <span className='link' onClick={() => handleLoadStart()}>
                                        &nbsp;Đăng ký
                                    </span>
                                </p>
                            </Form>
                        </Card>
                    </Col>
                </div>
            </div>

        </div>
    );
};

export default Login;