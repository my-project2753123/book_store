import React, { useEffect, useState } from 'react';
import { Card, Col, Divider, Row } from 'antd';
import { Column, Line, Pie } from '@ant-design/charts';
import { ProForm, ProFormDateYearRangePicker } from '@ant-design/pro-components';
import { useAppDispatch } from '@/redux/hook';
import { fetchRole } from '@/redux/slide/roleSlide';
import { IStatisticalOrder } from '@/types/backend';
import { callFetchStatisticalOrder } from '@/config/api';
import { sfGt, sfLt } from "spring-filter-query-builder";
import queryString from 'query-string';


interface IProps {
    handleFetchData: (v: string) => Promise<void>;
}

const Header = (props: IProps) => {

    const dispatch = useAppDispatch();
    const { handleFetchData } = props;

    const handleOnFinish = (value: any) => {
        const { dateYear } = value;
        const q: any = {
            filter: ""
        }
        if (dateYear?.length > 0) {
            q.filter = `createdAt>=${dateYear[0]} and createdAt<=${dateYear[1]}`;
            handleFetchData(queryString.stringify(q));
        }
    }

    return (
        <div style={
            {
                display: 'flex',
                justifyContent: "space-between",
                alignItems: "center"
            }
        }>
            Thống kê
            <ProForm
                style={{
                    padding: "15px 0"
                }}
                initialValues={{
                    dateYear: Date.now(),

                }}

                onFinish={async (values) => {
                    // dispatch(fetchRole({ values }))
                    handleOnFinish(values)
                }}
            >
                <ProFormDateYearRangePicker
                    fieldProps={{
                        format: 'YYYY',
                    }}
                    name={"dateYear"} />
            </ProForm>
        </div>
    )
}

const Revenue = () => {

    const [data, setdata] = useState<IStatisticalOrder>();
    const [dataStatis, setdataStatis] = useState<any[]>([]);

    const handleFetchData = async (query: string) => {
        const res = await callFetchStatisticalOrder(query);
        if (res?.data) {
            setdataStatis(res?.data?.months);
            setdata(res?.data);
        }
    }

    useEffect(() => {
        handleFetchData("")
    }, [])

    const config = {
        data: dataStatis ?? [],
        xField: 'month',
        yField: 'total',
        point: {
            shapeField: 'square',
            sizeField: 4,
        },
        interaction: {
            tooltip: {
                marker: false,
            },
        },
        style: {
            lineWidth: 2,
        },
    };
    return (
        <Row gutter={[16, 16]}>
            <Col span={24}>
                <Card title={<Header
                    handleFetchData={handleFetchData}
                />}>
                    <Line {...config} />
                    <Divider />
                    <Row gutter={10}>
                        <Col span={3}>
                        </Col>
                        <Col span={3}>
                            <span
                                style={{
                                    fontSize: 16,
                                    fontWeight: 600
                                }}
                            >
                                Hóa đơn
                            </span>
                        </Col>
                        <Col span={3}>
                            <span
                                style={{
                                    fontSize: 16,
                                    fontWeight: 600
                                }}
                            >
                                Tổng tiền
                            </span>
                        </Col>
                        <Col span={3}>
                            <span
                                style={{
                                    fontSize: 16,
                                    fontWeight: 600
                                }}
                            >
                                Thành tiền
                            </span>
                        </Col>
                    </Row>
                    <Row gutter={10}>
                        <Col span={3}>
                            Tổng
                        </Col>
                        <Col span={3}>
                            <span
                                style={{
                                    fontSize: 16,
                                    fontWeight: 600
                                }}
                            >
                                {data?.quantity ?? 0}
                            </span>
                        </Col>
                        <Col span={3}>
                            <span
                                style={{
                                    fontSize: 16,
                                    fontWeight: 600
                                }}
                            >
                                {data?.total ?? 0}
                            </span>
                        </Col>
                        <Col span={3}>
                            <span
                                style={{
                                    fontSize: 16,
                                    fontWeight: 600
                                }}
                            >
                                {data?.totalPayable ?? 0}
                            </span>
                        </Col>
                    </Row>

                </Card>
            </Col>
        </Row>
    );
};

export default Revenue;
