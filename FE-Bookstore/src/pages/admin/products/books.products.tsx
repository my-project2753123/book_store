import DataTable from "@/components/admin/share/data-table";
import { IProduct, IPublisher } from "@/types/backend";
import { EditOutlined, PlusOutlined, EyeOutlined } from "@ant-design/icons";
import { ActionType, ProColumns } from '@ant-design/pro-components';
import { Button, Space, Tag } from "antd";
import { useState, useRef, useCallback } from 'react';
import dayjs from 'dayjs';
import queryString from 'query-string';
import { sfLike } from "spring-filter-query-builder";
import { useAppDispatch, useAppSelector } from "@/redux/hook";
import Access from "@/components/share/protect.route/permission.route";
import { ALL_PERMISSIONS } from "@/config/permission";
import ModalProduct from "@/components/admin/product/modal.product";
import { fetchProduct } from "@/redux/slide/productSlide";
import DrawProduct from "@/components/admin/product/draw.product";
export interface ISelect {
    label: string;
    value: string;
}

const booksProducts = () => {
    const [openModal, setOpenModal] = useState<boolean>(false);
    const [dataInit, setDataInit] = useState<IProduct | null>(null);
    const [openViewDetail, setOpenViewDetail] = useState<boolean>(false);

    const tableRef = useRef<ActionType>();

    const isFetching = useAppSelector(state => state.product.isFetching);
    const meta = useAppSelector(state => state.product.meta);
    const author = useAppSelector(state => state.product.result);
    const dispatch = useAppDispatch();

    const reloadTable = () => {
        tableRef?.current?.reload();
    }

    const handleEdit = useCallback((entity: any) => {
        setDataInit(entity);
        setOpenModal(true);
    }, [dataInit])

    const handleShowUser = (entity: any) => {
        setDataInit(entity)
        setOpenViewDetail(true);
    }

    const columns: ProColumns<IProduct>[] = [
        {
            title: 'STT',
            key: 'index',
            width: 50,
            align: "center",
            render: (_value, entity, _index, _action) => {
                return (
                    <>
                        {(_index + 1) + (meta.page - 1) * (meta.pageSize)}
                    </>
                )
            },
            hideInSearch: true,
        },
        {
            title: 'Name',
            dataIndex: 'name',
            sorter: true,
        },
        {
            title: 'Category',
            dataIndex: 'category',
            sorter: true,
        },
        {
            title: 'Genre',
            dataIndex: 'genre',
            sorter: true,
        },
        {
            title: 'Author',
            dataIndex: 'author',
            sorter: true,
            render(dom, entity, index, action, schema) {
                return <p key={`key-author-${index}`}>
                    {
                        entity?.author?.name

                    }
                </p>
            },
        },
        {
            title: 'Publisher',
            dataIndex: 'publisher',
            sorter: true,
            render(dom, entity, index, action, schema) {
                return <p key={`key-publisher-${index}`}>
                    {
                        entity?.publisher?.name
                    }
                </p>
            },
        },
        {
            title: 'Quantity',
            dataIndex: 'quantity',
            sorter: true,
            hideInSearch: true,
        },
        {
            title: 'Price',
            dataIndex: 'price',
            sorter: true,
            render(dom, entity, index, action, schema) {
                let price = entity?.prices.filter(item => item.active === true)
                    .map(item => item.price).toString();
                const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }
                const formated = new Intl.NumberFormat('vi-VN', config).format(+price);
                return <p key={`key-price-${index}`}>
                    {
                        formated
                    }
                </p>
            },
            valueType: "digit"
        },
        {
            title: 'Trạng thái',
            dataIndex: 'active',
            render(dom, entity, index, action, schema) {
                return <>
                    <Tag key={`key-active-${index}`} color={entity.active ? "lime" : "red"} >
                        {entity.active ? "ACTIVE" : "INACTIVE"}
                    </Tag>
                </>
            },
            hideInSearch: true,
        },
        {
            title: 'CreatedAt',
            dataIndex: 'createdAt',
            width: 200,
            sorter: true,
            render: (text, record, index, action) => {
                return (
                    <>{record.createdAt ? dayjs(record.createdAt).format('DD-MM-YYYY HH:mm:ss') : ""}</>
                )
            },
            hideInSearch: true,
        },
        {
            title: 'UpdatedAt',
            dataIndex: 'updatedAt',
            width: 200,
            sorter: true,
            render: (text, record, index, action) => {
                return (
                    <>{record.updatedAt ? dayjs(record.updatedAt).format('DD-MM-YYYY HH:mm:ss') : ""}</>
                )
            },
            hideInSearch: true,
        },
        {

            title: 'Actions',
            hideInSearch: true,
            width: 50,
            render: (_value, entity, _index, _action) => (
                <Space>
                    < Access
                        permission={ALL_PERMISSIONS.USERS.UPDATE}
                        hideChildren
                    >
                        <EditOutlined
                            style={{
                                fontSize: 20,
                                color: '#ffa500',
                            }}
                            type=""
                            onClick={() => handleEdit(entity)}
                        />
                    </Access >

                    <Access
                        permission={ALL_PERMISSIONS.USERS.DELETE}
                        hideChildren
                    >
                        <EyeOutlined
                            style={{
                                fontSize: 20,
                                color: '#ff4d4f',
                                margin: '0 10px'
                            }}
                            type=""
                            onClick={() => handleShowUser(entity)}
                        />
                    </Access>
                </Space >
            ),

        },
    ];

    const buildQuery = (params: any, sort: any, filter: any) => {
        const q: any = {
            page: params.current,
            size: params.pageSize,
            filter: ""
        }

        const clone = { ...params };
        if (clone.name) q.filter = `${sfLike("name", clone.name)}`;
        if (clone.email) {
            q.filter = clone.name ?
                q.filter + " and " + `${sfLike("email", clone.email)}`
                : `${sfLike("email", clone.email)}`;
        }

        if (!q.filter) delete q.filter;
        let temp = queryString.stringify(q);

        let sortBy = "";
        if (sort && sort.name) {
            sortBy = sort.name === 'ascend' ? "sort=name,asc" : "sort=name,desc";
        }
        if (sort && sort.email) {
            sortBy = sort.email === 'ascend' ? "sort=email,asc" : "sort=email,desc";
        }
        if (sort && sort.createdAt) {
            sortBy = sort.createdAt === 'ascend' ? "sort=createdAt,asc" : "sort=createdAt,desc";
        }
        if (sort && sort.updatedAt) {
            sortBy = sort.updatedAt === 'ascend' ? "sort=updatedAt,asc" : "sort=updatedAt,desc";
        }

        //mặc định sort theo updatedAt
        if (Object.keys(sortBy).length === 0) {
            temp = `${temp}&sort=updatedAt,desc`;
        } else {
            temp = `${temp}&${sortBy}`;
        }

        return temp;
    }

    return (
        <div>
            <Access
                permission={ALL_PERMISSIONS.USERS.GET_PAGINATE}
            >
                <DataTable<IProduct>
                    actionRef={tableRef}
                    headerTitle="Danh sách Publisher"
                    rowKey="id"
                    loading={isFetching}
                    columns={columns}
                    dataSource={author}
                    manualRequest={true}
                    request={async (params, sort, filter): Promise<any> => {
                        const query = buildQuery(params, sort, filter);
                        dispatch(fetchProduct({ query }))
                    }}
                    scroll={{ x: true }}
                    pagination={
                        {
                            current: meta.page,
                            pageSize: meta.pageSize,
                            showSizeChanger: true,
                            total: meta.total,
                            showTotal: (total, range) => { return (<div> {range[0]}-{range[1]} trên {total} rows</div>) }
                        }
                    }
                    rowSelection={false}
                    toolBarRender={(_action, _rows): any => {
                        return (
                            <Access
                                permission={ALL_PERMISSIONS.USERS.CREATE}
                                hideChildren
                            >
                                <Button
                                    icon={<PlusOutlined />}
                                    type="primary"
                                    onClick={() => setOpenModal(true)}
                                >
                                    Thêm mới
                                </Button>
                            </Access>
                        );
                    }}
                />

            </Access>
            <ModalProduct
                dataInit={dataInit}
                setDataInit={setDataInit}
                openModal={openModal}
                setOpenModal={setOpenModal}
                reloadTable={reloadTable}
            />
            <DrawProduct
                onClose={setOpenViewDetail}
                open={openViewDetail}
                dataInit={dataInit}
                setDataInit={setDataInit}
            />
        </div >
    )
};

export default booksProducts;