import { callDeletePermission, callFetchAllPermission } from "@/config/api";
import { IPermission } from "@/types/backend";
import { ActionType, ProColumns } from "@ant-design/pro-components";
import queryString from "query-string";
import { useRef, useState } from "react";
import { sfLike } from "spring-filter-query-builder";
import dayjs from 'dayjs';
import { Button, Popconfirm, Space, message, notification } from "antd";
import { DeleteOutlined, EditOutlined, PlusOutlined } from "@ant-design/icons";
import DataTable from "@/components/admin/share/data-table";
import { colorMethod } from "@/config/utils";
import ModalPermission from "@/components/admin/permissions/modal.permission";
import { useAppDispatch, useAppSelector } from "@/redux/hook";
import { fetchPermission } from "@/redux/slide/permissionSlide";
import Access from "@/components/share/protect.route/permission.route";
import { ALL_PERMISSIONS } from "@/config/permission";

const permissionAdmin = () => {

    const [openModal, setOpenModal] = useState<boolean>(false);
    const [dataInit, setDataInit] = useState<IPermission | null>(null);
    const [openViewDetail, setOpenViewDetail] = useState<boolean>(false);

    const tableRef = useRef<ActionType>();

    const isFetching = useAppSelector(state => state.permission.isFetching);
    const meta = useAppSelector(state => state.permission.meta);
    const permissions = useAppSelector(state => state.permission.result);
    const dispatch = useAppDispatch();

    const columns: ProColumns<IPermission>[] = [
        {
            title: 'STT',
            key: 'index',
            width: 50,
            align: "center",
            render: (_value, entity, _index, _action) => {
                return (
                    <>
                        {(_index + 1) + (meta.page - 1) * (meta.pageSize)}
                    </>
                )
            },
            hideInSearch: true,
        },
        {
            title: 'Name',
            dataIndex: 'name',
            sorter: true,
        },
        {
            title: 'Path',
            dataIndex: 'apiPath',
            sorter: true,
        },

        {
            title: 'Method',
            dataIndex: 'method',
            sorter: true,
            valueType: 'select',
            valueEnum: {
                GET: {
                    text: 'GET',
                },
                POST: {
                    text: 'POST',
                },
                PUT: {
                    text: 'PUT',
                },
                DELETE: {
                    text: 'DELETE',
                },
            },
            render(dom, entity, index, action, schema) {
                return (
                    <p style={{ paddingLeft: 10, fontWeight: 'bold', marginBottom: 0, color: colorMethod(entity?.method as string) }}>{entity?.method || ''}</p>
                )
            },
        },
        {
            title: 'Module',
            dataIndex: 'module',
            sorter: true,
            hideInSearch: true
        },
        {
            title: 'CreatedAt',
            dataIndex: 'createdAt',
            width: 200,
            sorter: true,
            render: (text, record, index, action) => {
                return (
                    <>{record.createdAt ? dayjs(record.createdAt).format('DD-MM-YYYY HH:mm:ss') : ""}</>
                )
            },
            hideInSearch: true,
        },
        {
            title: 'UpdatedAt',
            dataIndex: 'updatedAt',
            width: 200,
            sorter: true,
            render: (text, record, index, action) => {
                return (
                    <>{record.updatedAt ? dayjs(record.updatedAt).format('DD-MM-YYYY HH:mm:ss') : ""}</>
                )
            },
            hideInSearch: true,
        },
        {

            title: 'Actions',
            hideInSearch: true,
            width: 50,
            render: (_value, entity, _index, _action) => (
                <Space>
                    < Access
                        permission={ALL_PERMISSIONS.PERMISSIONS.UPDATE}
                        hideChildren
                    >
                        <EditOutlined
                            style={{
                                fontSize: 20,
                                color: '#ffa500',
                            }}
                            type=""
                            onClick={() => handleEdit(entity)}
                        />
                    </Access >

                    <Access
                        permission={ALL_PERMISSIONS.PERMISSIONS.DELETE}
                        hideChildren
                    >
                        <Popconfirm
                            placement="leftTop"
                            title={"Xác nhận xóa permission"}
                            description={"Bạn có chắc chắn muốn xóa permission này ?"}
                            onConfirm={() => handleDeletePermission(entity.id)}
                            okText="Xác nhận"
                            cancelText="Hủy"
                        >
                            <span style={{ cursor: "pointer", margin: "0 10px" }}>
                                <DeleteOutlined
                                    style={{
                                        fontSize: 20,
                                        color: '#ff4d4f',
                                    }}
                                />
                            </span>
                        </Popconfirm>
                    </Access>
                </Space >
            ),

        },
    ];

    const handleDeletePermission = async (id: any) => {
        const res = await callDeletePermission(parseInt(id));
        if (+res.statusCode === 200) {
            message.success('Xóa Permission thành công');
            reloadTable();
        } else {
            notification.error({
                message: 'Có lỗi xảy ra',
                description: res.message
            });
        }
    }

    const handleEdit = (entity: IPermission) => {
        setDataInit(entity)
        setOpenModal(true)
    }

    const reloadTable = () => {
        tableRef?.current?.reload();
    }

    const buildQuery = (params: any, sort: any, filter: any) => {
        const q: any = {
            page: params.current,
            size: params.pageSize,
            filter: ""
        }

        const clone = { ...params };
        if (clone.name) q.filter = `${sfLike("name", clone.name)}`;
        if (clone.apiPath) {
            q.filter = clone.name ?
                q.filter + " and " + `${sfLike("apiPath", clone.apiPath)}`
                : `${sfLike("apiPath", clone.apiPath)}`;
        }
        if (clone.method) {
            q.filter = clone.name || clone.apiPath ?
                q.filter + " and " + `${sfLike("method", clone.method)}`
                : `${sfLike("method", clone.method)}`;
        }

        if (!q.filter) delete q.filter;
        let temp = queryString.stringify(q);

        let sortBy = "";
        if (sort && sort.name) {
            sortBy = sort.name === 'ascend' ? "sort=name,asc" : "sort=name,desc";
        }
        if (sort && sort.apiPath) {
            sortBy = sort.apiPath === 'ascend' ? "sort=apiPath,asc" : "sort=apiPath,desc";
        }
        if (sort && sort.method) {
            sortBy = sort.method === 'ascend' ? "sort=method,asc" : "sort=method,desc";
        }
        if (sort && sort.createdAt) {
            sortBy = sort.createdAt === 'ascend' ? "sort=createdAt,asc" : "sort=createdAt,desc";
        }
        if (sort && sort.updatedAt) {
            sortBy = sort.updatedAt === 'ascend' ? "sort=updatedAt,asc" : "sort=updatedAt,desc";
        }

        //mặc định sort theo updatedAt
        if (Object.keys(sortBy).length === 0) {
            temp = `${temp}&sort=updatedAt,desc`;
        } else {
            temp = `${temp}&${sortBy}`;
        }

        return temp;
    }

    return (
        <div>
            <Access
                permission={ALL_PERMISSIONS.PERMISSIONS.GET_PAGINATE}
                hideChildren
            >
                <DataTable<IPermission>
                    actionRef={tableRef}
                    headerTitle="Danh sách Users"
                    rowKey="id"
                    loading={isFetching}
                    columns={columns}
                    dataSource={permissions}
                    manualRequest={true}
                    request={async (params, sort, filter): Promise<any> => {
                        const query = buildQuery(params, sort, filter);
                        dispatch(fetchPermission({ query }));
                    }}
                    scroll={{ x: true }}
                    pagination={
                        {
                            current: meta?.page,
                            pageSize: meta?.pageSize,
                            showSizeChanger: true,
                            total: meta?.total,
                            showTotal: (total, range) => { return (<div> {range[0]}-{range[1]} trên {total} rows</div>) }
                        }
                    }
                    rowSelection={false}
                    toolBarRender={(_action, _rows): any => {
                        return (
                            <Access
                                permission={ALL_PERMISSIONS.PERMISSIONS.GET_PAGINATE}
                                hideChildren
                            >
                                <Button
                                    icon={<PlusOutlined />}
                                    type="primary"
                                    onClick={() => setOpenModal(true)}
                                >
                                    Thêm mới
                                </Button>
                            </Access>
                        );
                    }}
                />
            </Access>
            <ModalPermission
                openModal={openModal}
                setOpenModal={setOpenModal}
                reloadTable={reloadTable}
                dataInit={dataInit}
                setDataInit={setDataInit}
            />


        </div>
    );
};

export default permissionAdmin;