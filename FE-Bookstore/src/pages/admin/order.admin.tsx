import { callDeleteOrder, callDeleteRole, callPaiDOrder } from "@/config/api";
import { IOrderAD, IRole } from "@/types/backend";
import { ActionType, ProColumns } from "@ant-design/pro-components";
import queryString from "query-string";
import { useRef, useState } from "react";
import { sfLike } from "spring-filter-query-builder";
import dayjs from 'dayjs';
import { Button, Popconfirm, Space, Tag, message, notification } from "antd";
import { DeleteOutlined, EditOutlined, EyeOutlined, PlusOutlined, TransactionOutlined } from "@ant-design/icons";
import DataTable from "@/components/admin/share/data-table";
import { useAppDispatch, useAppSelector } from "@/redux/hook";
import Access from "@/components/share/protect.route/permission.route";
import { ALL_PERMISSIONS } from "@/config/permission";
import { fetchOrder } from "@/redux/slide/orderSilde";
import { colorMethod } from "@/config/utils";
import ModalOrder from "@/components/admin/order/modal.order";
import DrawOrder from "@/components/admin/order/draw.order";

const orderAdmin = () => {
    const [openModal, setOpenModal] = useState<boolean>(false);

    const tableRef = useRef<ActionType>();

    const isFetching = useAppSelector(state => state.order.isFetching);
    const meta = useAppSelector(state => state.order.meta);
    const orders = useAppSelector(state => state.order.result);
    const dispatch = useAppDispatch();
    const [dataInit, setDataInit] = useState<IOrderAD | null>(null);

    const [openViewDetail, setOpenViewDetail] = useState<boolean>(false);


    const columns: ProColumns<IOrderAD>[] = [
        {
            title: 'Id',
            dataIndex: 'id',
            render: (text, record, index, action) => {
                return (
                    <span>
                        {record.id}
                    </span>
                )
            },
            hideInSearch: true,
        },
        {
            title: 'User',
            dataIndex: 'user',
            sorter: true,
            render(dom, entity, index, action, schema) {
                return <p key={`key-user-${index}`}>
                    {
                        entity?.user?.email
                    }
                </p>
            },
        },
        {
            title: 'Payment',
            dataIndex: 'payment',
            sorter: true,
            valueType: 'select',
            valueEnum: {
                DIRECT: {
                    text: 'DIRECT',
                },
                TRANSFER: {
                    text: 'TRANSFER',
                },
            },
            render(dom, entity, index, action, schema) {
                return (
                    <p style={{ paddingLeft: 10, fontWeight: 'bold', marginBottom: 0, color: colorMethod(entity?.payment as string) }}>{entity?.payment || ''}</p>
                )
            },
        },
        {
            title: 'Status',
            dataIndex: 'status',
            sorter: true,
            valueType: 'select',
            valueEnum: {
                PAID: {
                    text: 'PAID',
                },
                UNPAID: {
                    text: 'UNPAID',
                },
            },
            render(dom, entity, index, action, schema) {
                return (
                    <p style={{ paddingLeft: 10, fontWeight: 'bold', marginBottom: 0, color: colorMethod(entity?.status as string) }}>{entity?.status || ''}</p>
                )
            },
        },
        {
            title: 'Total Amount',
            dataIndex: 'totalAmount',
            sorter: true,
            render(dom, entity, index, action, schema) {
                const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }
                const formated = new Intl.NumberFormat('vi-VN', config).format(+entity?.totalAmount);
                return <p key={`key-total-amount-${index}`} style={{ paddingLeft: 10, fontWeight: 'bold', marginBottom: 0 }}>
                    {
                        formated
                    }
                </p>
            },
            hideInSearch: true,
        },
        {
            title: 'Discount',
            dataIndex: 'discount',
            sorter: true,
            hideInSearch: true,
        },
        {
            title: 'Total Amount Payable',
            dataIndex: 'totalAmountPlayable',
            sorter: true,
            render(dom, entity, index, action, schema) {
                const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }
                const formated = new Intl.NumberFormat('vi-VN', config).format(+entity?.totalAmountPlayable);
                return <p key={`key-total-amount-payable-${index}`}
                    style={{ paddingLeft: 10, fontWeight: 'bold', marginBottom: 0, color: "#ff4d4f" }}>
                    {
                        formated
                    }
                </p>
            },
            hideInSearch: true,
        },
        {
            title: 'Trạng thái',
            dataIndex: 'active',
            render(dom, entity, index, action, schema) {
                return <>
                    <Tag color={entity.active ? "lime" : "red"} >
                        {entity.active ? "ACTIVE" : "INACTIVE"}
                    </Tag>
                </>
            },
            hideInSearch: true,
        },
        {
            title: 'CreatedAt',
            dataIndex: 'createdAt',
            width: 200,
            sorter: true,
            render: (text, record, index, action) => {
                return (
                    <>{record.createdAt ? dayjs(record.createdAt).format('DD-MM-YYYY HH:mm:ss') : ""}</>
                )
            },
            hideInSearch: true,
        },
        {
            title: 'UpdatedAt',
            dataIndex: 'updatedAt',
            width: 200,
            sorter: true,
            render: (text, record, index, action) => {
                return (
                    <>{record.updatedAt ? dayjs(record.updatedAt).format('DD-MM-YYYY HH:mm:ss') : ""}</>
                )
            },
            hideInSearch: true,
        },
        {

            title: 'Actions',
            hideInSearch: true,
            width: 50,
            render: (_value, entity, _index, _action) => (
                <Space>
                    <Access
                        permission={ALL_PERMISSIONS.ORDER.UPDATE}
                        hideChildren
                    >
                        <EditOutlined
                            style={{
                                fontSize: 20,
                                color: '#ffa500',
                            }}
                            type=""
                            onClick={() => {
                                handleUpdateOrder(entity)
                            }}
                        />
                    </Access>
                    <Access
                        permission={ALL_PERMISSIONS.ORDER.DELETE}
                        hideChildren
                    >
                        <Popconfirm
                            placement="leftTop"
                            title={"Xác nhận xóa role"}
                            description={"Bạn có chắc chắn muốn xóa role này ?"}
                            onConfirm={() => handleDeleteOrder(entity.id)}
                            okText="Xác nhận"
                            cancelText="Hủy"
                        >
                            <span style={{ cursor: "pointer" }}>
                                <DeleteOutlined
                                    style={{
                                        fontSize: 20,
                                        color: '#ff4d4f',
                                    }}
                                />
                            </span>
                        </Popconfirm>
                    </Access>
                    <EyeOutlined
                        style={{
                            fontSize: 20,
                            color: '#91caff',
                        }}
                        type=""
                        onClick={() => {
                            setDataInit(entity)
                            setOpenViewDetail(true);
                        }}
                    />
                    <TransactionOutlined
                        style={{
                            fontSize: 20,
                            color: '#389e0d',
                        }}
                        type=""
                        onClick={() => {
                            handleUpdatePaidOrder(entity?.id);
                        }}
                    />
                </Space>
            ),

        },
    ];

    const handleUpdateOrder = (entity: IOrderAD) => {
        setDataInit(entity);
        setOpenModal(true);
    }

    const handleDeleteOrder = async (id: string) => {
        const res = await callDeleteOrder(id);
        if (+res.statusCode === 200) {
            message.success('Xóa order thành công');
            reloadTable();
        } else {
            notification.error({
                message: 'Có lỗi xảy ra',
                description: res.message
            });
        }
    }

    const handleUpdatePaidOrder = async (id: string) => {
        const res = await callPaiDOrder(id);
        if (+res.statusCode === 200) {
            message.success("cập nhật trạng thái thành công");
            reloadTable();
        } else {
            notification.error({
                message: 'Có lỗi xảy ra',
                description: res.message
            });
        }
    }

    const reloadTable = () => {
        tableRef?.current?.reload();
    }

    const buildQuery = (params: any, sort: any, filter: any) => {
        const q: any = {
            page: params.current,
            size: params.pageSize,
            filter: ""
        }

        const clone = { ...params };
        if (clone.user) q.filter = `${sfLike("user.email", clone.user)}`;
        if (clone.payment) {
            q.filter = clone.name ?
                q.filter + " and " + `${sfLike("payment", clone.payment)}`
                : `${sfLike("payment", clone.payment)}`;
        }
        if (clone.status) {
            q.filter = clone.name || clone.payment ?
                q.filter + " and " + `${sfLike("status", clone.status)}`
                : `${sfLike("status", clone.status)}`;
        }
        if (!q.filter) delete q.filter;
        let temp = queryString.stringify(q);

        let sortBy = "";
        if (sort && sort.user) {
            sortBy = sort.user === 'ascend' ? "sort=user,asc" : "sort=user,desc";
        }
        if (sort && sort.payment) {
            sortBy = sort.payment === 'ascend' ? "sort=payment,asc" : "sort=payment,desc";
        }
        if (sort && sort.status) {
            sortBy = sort.status === 'ascend' ? "sort=status,asc" : "sort=status,desc";
        }
        if (sort && sort.createdAt) {
            sortBy = sort.createdAt === 'ascend' ? "sort=createdAt,asc" : "sort=createdAt,desc";
        }
        if (sort && sort.updatedAt) {
            sortBy = sort.updatedAt === 'ascend' ? "sort=updatedAt,asc" : "sort=updatedAt,desc";
        }

        //mặc định sort theo updatedAt
        if (Object.keys(sortBy).length === 0) {
            temp = `${temp}&sort=updatedAt,desc`;
        } else {
            temp = `${temp}&${sortBy}`;
        }

        return temp;
    }

    return (
        <div>
            <Access
                permission={ALL_PERMISSIONS.ORDER.GET_PAGINATE}
                hideChildren
            >
                <DataTable<IOrderAD>
                    actionRef={tableRef}
                    headerTitle="Danh sách Order"
                    rowKey="id"
                    loading={isFetching}
                    columns={columns}
                    dataSource={orders}
                    request={async (params, sort, filter): Promise<any> => {
                        const query = buildQuery(params, sort, filter);
                        dispatch(fetchOrder({ query }))
                    }}
                    scroll={{ x: true }}
                    pagination={
                        {
                            current: meta?.page,
                            pageSize: meta?.pageSize,
                            showSizeChanger: true,
                            total: meta?.total,
                            showTotal: (total, range) => { return (<div> {range[0]}-{range[1]} trên {total} rows</div>) }
                        }
                    }
                    rowSelection={false}
                    toolBarRender={(_action, _rows): any => {
                        return (
                            <Button
                                icon={<PlusOutlined />}
                                type="primary"
                                onClick={() => setOpenModal(true)}
                            >
                                Thêm mới
                            </Button>
                        );
                    }}
                />
            </Access>
            <ModalOrder
                openModal={openModal}
                setOpenModal={setOpenModal}
                reloadTable={reloadTable}
                dataInit={dataInit}
                setDataInit={setDataInit}
            />
            <DrawOrder
                onClose={setOpenViewDetail}
                open={openViewDetail}
                dataInit={dataInit}
                setDataInit={setDataInit}
            />
        </div>
    );
};

export default orderAdmin;