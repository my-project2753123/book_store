import { useRef, useState } from "react";
import { IOrderAD } from "@/types/backend";
import { ActionType, ProColumns } from "@ant-design/pro-components";
import DataTable from "@/components/admin/share/data-table";
import { Space } from "antd";
import 'styles/input.number.less'
import { EyeOutlined } from "@ant-design/icons";
import { useAppSelector } from '@/redux/hook';
import { colorMethod } from '@/config/utils';
import { callUserOrderInactive } from '@/config/api';
import DrawOrderClient from '@/components/client/order/draw.index';

const Tabunorder = () => {

    const tableRef = useRef<ActionType>();
    const [order, setorder] = useState<IOrderAD[]>([])
    const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }
    const userRedux = useAppSelector((state: any) => state.account.user)

    const [dataInit, setDataInit] = useState<IOrderAD | null>(null);
    const [openViewDetail, setOpenViewDetail] = useState<boolean>(false);

    const columns: ProColumns<IOrderAD>[] = [
        {
            title: 'Id',
            dataIndex: 'id',
            render: (text, record, index, action) => {
                return (
                    <span>
                        {record.id}
                    </span>
                )
            },
            hideInSearch: true,
        },
        {
            title: 'User',
            dataIndex: 'user',
            sorter: true,
            render(dom, entity, index, action, schema) {
                return <p key={`key-user-${index}`}>
                    {
                        entity?.user?.email
                    }
                </p>
            },
        },
        {
            title: 'Total Amount',
            dataIndex: 'totalAmount',
            sorter: true,
            render(dom, entity, index, action, schema) {
                return (
                    <>
                        {new Intl.NumberFormat('vi-VN', config).format(+entity?.totalAmount)}
                    </>
                )
            },
            valueType: "digit",
            hideInSearch: true,
        },
        {
            title: 'Discount',
            dataIndex: 'discount',
            sorter: true,
            hideInSearch: true,
        },
        {
            title: 'Total Amount Payable',
            dataIndex: 'totalAmountPlayable',
            sorter: true,
            render(dom, entity, index, action, schema) {
                return (
                    <>
                        {new Intl.NumberFormat('vi-VN', config).format(+entity?.totalAmountPlayable)}
                    </>
                )
            },
            valueType: "digit",
            hideInSearch: true,
        },
        {
            title: 'Payment',
            dataIndex: 'payment',
            sorter: true,
            valueType: 'select',
            valueEnum: {
                DIRECT: {
                    text: 'DIRECT',
                },
                TRANSFER: {
                    text: 'TRANSFER',
                },
            },
            render(dom, entity, index, action, schema) {
                return (
                    <p style={{ paddingLeft: 10, fontWeight: 'bold', marginBottom: 0, color: colorMethod(entity?.payment as string) }}>{entity?.payment || ''}</p>
                )
            },
        },
        {

            title: 'Actions',
            hideInSearch: true,
            width: 50,
            render: (_value, entity, _index, _action) => (
                <Space>
                    <EyeOutlined
                        style={{
                            fontSize: 20,
                            color: '#91caff',
                        }}
                        type=""
                        onClick={() => {
                            setDataInit(entity)
                            setOpenViewDetail(true);
                        }}
                    />
                </Space>
            ),

        },
    ];

    const handleOrderActive = async () => {
        const res = await callUserOrderInactive(userRedux?.id);
        if (res?.data) {
            setorder(res.data)
        }
    }

    return (
        <div>
            <DataTable<IOrderAD>
                actionRef={tableRef}
                headerTitle="Danh sách đơn hàng hủy"
                rowKey="id"
                columns={columns}
                dataSource={order}
                search={false}
                scroll={{ x: true }}
                request={async (params, sort, filter): Promise<any> => {
                    handleOrderActive()
                }}
                pagination={
                    {
                        defaultPageSize: 10, showSizeChanger: true
                    }
                }
            />
            <DrawOrderClient
                onClose={setOpenViewDetail}
                open={openViewDetail}
                dataInit={dataInit}
                setDataInit={setDataInit}
            />
        </div>
    );
};

export default Tabunorder;