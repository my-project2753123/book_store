import { callCreateOrder, callUserById } from "@/config/api";
import { useAppSelector } from "@/redux/hook";
import { IOrder, IOrderDetail, IUser } from "@/types/backend";
import { Button, Card, Col, Divider, Form, Input, Result, Row, message, notification } from "antd";
import { useEffect, useState } from "react";
import { isMobile } from "react-device-detect";
import { useLocation, useNavigate } from "react-router-dom";

const orderIndex = () => {

    const { state } = useLocation();
    const navigate = useNavigate();
    const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }
    const userRedux = useAppSelector((state: any) => state.account.user)
    const { sum, arr } = state || {};

    const [form] = Form.useForm();
    const [isSubmit, setIsSubmit] = useState<boolean>(false);

    const handleFetchUser = async () => {
        const res = await callUserById(userRedux?.id);
        if (res?.data) {
            form.setFieldsValue(res.data)
        }
    }

    useEffect(() => {
        handleFetchUser()
    }, [])

    const handleOrder = async () => {
        const cparr = arr?.length > 0 ? [...arr] : null;
        let arrdt = cparr?.map(item => {
            return {
                quantity: +item?.quantity,
                book: {
                    id: item.id
                }
            } as IOrderDetail
        })
        const obj: IOrder = {
            payment: "DIRECT",
            status: "UNPAID",
            discount: 0,
            active: true,
            address: form.getFieldValue("address"),
            details: arrdt as IOrderDetail[],
        }
        console.log(obj)

        setIsSubmit(true);
        const res = await callCreateOrder(obj);
        if (res?.data) {
            setIsSubmit(false);
            message.success("Đặt hàng thành công");
        }
        else {
            notification.error({
                message: 'Có lỗi xảy ra',
                description: res.message
            });
        }
    }


    if (!sum || arr?.length < 0) {
        return (
            <Result
                title="Bạn chưa chọn sản phẩm"
                extra={
                    <Button type="primary" key="console"
                        onClick={() => navigate("/")}
                    >
                        Trang chủ
                    </Button>
                }
            />
        )
    } else {
        return (
            <div style={{
                marginTop: "10px"
            }}>
                <Row gutter={[16, 16]} >
                    <Col span={isMobile ? 24 : 10} style={{ marginTop: "15px" }}>
                        <Row>
                            <Card>
                                <Form<IUser>
                                    name="basic"
                                    autoComplete="off"
                                    form={form}
                                >
                                    <Row>
                                        <Col span={24}>
                                            <Form.Item
                                                labelCol={{ span: 24 }} //whole column
                                                label="Họ tên"
                                                name="name"
                                                rules={[{ required: true, message: 'Họ và tên không được để trống!' }]}
                                            >
                                                <Input />
                                            </Form.Item>
                                        </Col>
                                        <Col span={24}>
                                            <Form.Item
                                                labelCol={{ span: 24 }} //whole column
                                                label="Email"
                                                name="email"
                                                rules={[{ required: true, message: 'Email không được để trống!' }]}
                                            >
                                                <Input disabled />
                                            </Form.Item>
                                        </Col>

                                        <Col span={24}>
                                            <Form.Item
                                                labelCol={{ span: 24 }} //whole column
                                                label="Địa chỉ"
                                                name="address"
                                                rules={[{ required: true, message: 'Địa chỉ không để trống!' }]}
                                            >
                                                <Input />
                                            </Form.Item>
                                        </Col>
                                    </Row>

                                </Form>

                            </Card>
                        </Row>
                    </Col>
                    <Col span={isMobile ? 24 : 14} style={{ marginTop: "15px" }}>
                        <Row gutter={[10, 10]}>
                            <Col span={24}>
                                <Card>
                                    <Row gutter={10}>
                                        <Col span={15}>
                                            <span
                                                style={{
                                                    fontSize: 16,
                                                    fontWeight: 600
                                                }}
                                            >
                                                Sản phẩm
                                            </span>
                                        </Col>
                                        <Col span={3} style={{ display: isMobile ? "none" : "block" }}>
                                            <span
                                                style={{
                                                    fontSize: 16,
                                                    fontWeight: 600
                                                }}
                                            >
                                                Đơn giá
                                            </span>
                                        </Col>
                                        <Col span={isMobile ? 4 : 3}>
                                            <span
                                                style={{
                                                    fontSize: 16,
                                                    fontWeight: 600
                                                }}
                                            >
                                                Số lượng
                                            </span>
                                        </Col>
                                        <Col span={isMobile ? 5 : 3}>
                                            <span
                                                style={{
                                                    fontSize: 16,
                                                    fontWeight: 600
                                                }}
                                            >
                                                Thành tiền
                                            </span>
                                        </Col>

                                    </Row>
                                    {
                                        arr?.length > 0
                                        && arr?.map((item: any, index: any) => {
                                            return (
                                                <Card key={`cart-list-card-index-${index}`} style={{ marginTop: "5px" }}>
                                                    <Row gutter={10}>
                                                        <Col span={15}>
                                                            <div style={{
                                                                display: 'flex', alignItems: 'center'
                                                            }}>
                                                                <div style={{
                                                                    width: "100px",
                                                                    height: "90px",
                                                                    backgroundImage: `url(${import.meta.env.VITE_BACKEND_URL_UPLOAD}/storage/product/${item?.img})`,
                                                                    backgroundSize: 'cover',
                                                                    backgroundRepeat: 'no-repeat',
                                                                    backgroundPosition: 'center'
                                                                }}>

                                                                </div>
                                                                <div style={{
                                                                    display: "flex", flexDirection: "column",
                                                                    textOverflow: "ellipsis",
                                                                    whiteSpace: "nowrap",
                                                                    overflow: "hidden",
                                                                    width: "100%"
                                                                }}>
                                                                    {item?.name}
                                                                </div>
                                                            </div>
                                                        </Col>
                                                        <Col span={3} style={{ display: isMobile ? "none" : "block" }}>
                                                            <div style={{ height: "90px", display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                                                <span
                                                                    style={{
                                                                        fontSize: 17,
                                                                        fontWeight: 600
                                                                    }}
                                                                >
                                                                    {
                                                                        new Intl.NumberFormat('vi-VN', config).format(+(item?.price as string))
                                                                    }
                                                                </span>
                                                            </div>
                                                        </Col>
                                                        <Col span={isMobile ? 4 : 3}>
                                                            <div style={{ height: "90px", display: 'flex', alignItems: 'center', justifyContent: "center" }}>
                                                                {item?.quantity}
                                                            </div>
                                                        </Col>
                                                        <Col span={isMobile ? 5 : 3}>
                                                            <div style={{ height: "90px", display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                                                <span
                                                                    style={{
                                                                        fontSize: 17,
                                                                        fontWeight: 600,
                                                                        color: "#ff7043"
                                                                    }}
                                                                >
                                                                    {
                                                                        new Intl.NumberFormat('vi-VN', config).format(+(item?.total as string))
                                                                    }
                                                                </span>
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                </Card>
                                            )
                                        })
                                    }
                                </Card>
                            </Col>
                            <Col span={24}>
                                <Card>
                                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                        <span>Tạm tính</span>
                                        <span>
                                            {
                                                new Intl.NumberFormat('vi-VN', config).format(
                                                    +arr?.reduce((sum: any, item: any) => +sum + (+item.total as number), 0)
                                                )
                                            }
                                        </span>
                                    </div>
                                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                        <span>Giảm giá</span>
                                        <span>
                                            {
                                                new Intl.NumberFormat('vi-VN', config).format(
                                                    0
                                                )
                                            }
                                        </span>
                                    </div>
                                    <Divider />
                                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                        <span>Tổng tiền</span>
                                        <span>
                                            <span
                                                style={{
                                                    fontSize: 17,
                                                    fontWeight: 600,
                                                    color: "#ff7043"
                                                }}
                                            >
                                                {
                                                    new Intl.NumberFormat('vi-VN', config).format(
                                                        +arr?.reduce((sum: any, item: any) => +sum + (+item.total as number), 0)
                                                    )
                                                }
                                            </span>
                                        </span>
                                    </div>
                                    <Button style={{ width: "100%", marginTop: "15px", marginBottom: "15px" }} danger
                                        // disabled={checkedList?.length > 0 ? false : true}
                                        onClick={() => handleOrder()}
                                        loading={isSubmit}
                                    >
                                        Xác nhận đặt hàng
                                    </Button>
                                </Card>
                            </Col>
                        </Row>
                    </Col>

                </Row>
            </div >
        );
    }

};

export default orderIndex;