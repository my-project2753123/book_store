import { ProCard } from '@ant-design/pro-components';
import { Tabs } from 'antd';
import { SizeType } from 'antd/es/config-provider/SizeContext';
import React, { useState } from 'react';
import Taborder from './Taborder';
import Tabunorder from './Tabunorder';

const orderClient = () => {

    const [size, setSize] = useState<SizeType>('small');

    return (
        <div style={{ marginTop: "15px" }}>
            <Tabs
                defaultActiveKey="1"
                type="card"
                size={size}
                destroyInactiveTabPane={true}
                items={new Array(2).fill(null).map((_, i) => {
                    const id = String(i + 1);
                    return {
                        label: +id === 1 ? "Đơn đặt hàng" : "Đơn đã hủy",
                        key: id,
                        children: +id === 1
                            ?
                            <Taborder
                            />
                            :
                            <Tabunorder
                            />
                    };
                })}
            />
        </div>
    );
};

export default orderClient;