import { callFetchAllProduct, callFetchProductById } from "@/config/api";
import { IProduct } from "@/types/backend";
import { Box, Button, Card, CardContent, Container, Grid, IconButton, InputBase, Typography, makeStyles, useMediaQuery, useTheme } from "@mui/material";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import RemoveIcon from '@mui/icons-material/Remove';
import AddIcon from '@mui/icons-material/Add';
import BookDetailInfo from "@/components/client/home/books/detail/book.detail.info";
import { sfEqual } from "spring-filter-query-builder";
import nProgress from "nprogress";
import { isMobile, isTablet } from "react-device-detect";
import { useAppDispatch } from "@/redux/hook";
import { setItemInCart } from "@/redux/slide/cartSlide";

const detail = () => {

    const params = useParams();
    const { id } = params;
    const [book, setbook] = useState<IProduct | null>(null);
    const [quantity, setquantity] = useState<number>(1);
    const [bookByCategory, setbookByCategory] = useState<IProduct[]>([]);
    const location = useLocation();
    const dispatch = useAppDispatch();
    const theme = useTheme();
    const isMd = useMediaQuery(theme.breakpoints.up('md'));
    const navigate = useNavigate();

    const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }
    const price = book && book.prices ? book?.prices.find(items => items.active === true)?.price : 0

    const handleFetchDetailBook = useCallback(async () => {
        let res = await callFetchProductById(id ? id as string : "");
        if (res?.data) {
            nProgress.done()
            setbook(res?.data)
        }
    }, [book])

    const handleFetchBookForCategory = useCallback(async () => {
        if (book) {
            let query = `page=${1}&size=${20}`;
            query = `&${book?.category}`;
            let q = sfEqual("category", book?.category as string).toString();

            query = `&filter=${encodeURIComponent(q)}`;

            const res = await callFetchAllProduct(query);
            if (res?.data?.result) {
                setbookByCategory(res.data.result);
            }
        }

    }, [bookByCategory, book])

    useEffect(() => {
        nProgress.start();
        handleFetchDetailBook()
        setquantity(1)
    }, [])

    useEffect(() => {
        handleFetchBookForCategory()
    }, [book])

    useEffect(() => {
        handleFetchDetailBook()
        setquantity(1)
    }, [location])

    useEffect(() => {
        window.scrollTo(0, 0)
    }, [location])

    const handleAddCart = () => {
        const arrLocal = localStorage?.getItem("arrProduct")
        if (arrLocal) {
            const arrCart = [...JSON.parse(arrLocal)];
            const objIndex = arrCart.findIndex(item => item.id === book?.id);
            if (objIndex !== -1) {
                arrCart[objIndex].quantity += quantity;
                arrCart[objIndex].price += price ? (price as number) * quantity : 0;
                localStorage.setItem("arrProduct", JSON.stringify(arrCart));
                dispatch(setItemInCart(arrCart))
            } else {
                const product = {
                    id: book?.id,
                    img: book?.image,
                    name: book?.name,
                    total: price ? (price as number) * quantity : 0,
                    quantity: quantity,
                    price: price
                }
                arrCart.push(product);
                localStorage.setItem("arrProduct", JSON.stringify(arrCart));
                dispatch(setItemInCart(arrCart))
            }
        }
        else {
            const arrCart = [];
            const product = {
                id: book?.id,
                img: book?.image,
                name: book?.name,
                total: price ? (price as number) * quantity : 0,
                quantity: quantity,
                price: price
            }

            arrCart.push(product)

            localStorage.setItem("arrProduct", JSON.stringify(arrCart));
            dispatch(setItemInCart(arrCart))
        }

    }

    const handleBuy = () => {
        const arrLocal = localStorage?.getItem("arrProduct")
        if (arrLocal) {
            const arrCart = [...JSON.parse(arrLocal)];
            const objIndex = arrCart.findIndex(item => item.id === book?.id);
            if (objIndex !== -1) {
                arrCart[objIndex].quantity += quantity;
                arrCart[objIndex].price += price ? (price as number) * quantity : 0;
                localStorage.setItem("arrProduct", JSON.stringify(arrCart));
                dispatch(setItemInCart(arrCart))
                navigate("/cart")

            } else {
                const product = {
                    id: book?.id,
                    img: book?.image,
                    name: book?.name,
                    total: price ? (price as number) * quantity : 0,
                    quantity: quantity,
                    price: price
                }
                arrCart.push(product);
                localStorage.setItem("arrProduct", JSON.stringify(arrCart));
                dispatch(setItemInCart(arrCart))
                navigate("/cart")

            }
        }
        else {
            const arrCart = [];
            const product = {
                id: book?.id,
                img: book?.image,
                name: book?.name,
                total: price ? (price as number) * quantity : 0,
                quantity: quantity,
                price: price
            }

            arrCart.push(product)

            localStorage.setItem("arrProduct", JSON.stringify(arrCart));
            dispatch(setItemInCart(arrCart))
            navigate("/cart")

        }
    }

    return (
        <Grid container spacing={isMobile || isTablet ? 0 : 4}>
            <Grid item xs={isMobile || isTablet ? 12 : 3} sm={4.5} md={3}>
                <Card sx={{ position: isMd ? "sticky" : "inherit", top: 0 }}>
                    {
                        useMemo(() => (
                            <CardContent
                                sx={{ display: 'flex', justifyContent: 'center', mt: isMobile || isTablet ? 1 : 0 }}
                            >
                                <Box
                                    sx={{
                                        width: isMobile || isTablet ? "70%" : "100%",
                                        height: "400px",
                                        backgroundImage: `url(${import.meta.env.VITE_BACKEND_URL_UPLOAD}/storage/product/${book?.image})`,
                                        backgroundSize: 'cover',
                                        backgroundRepeat: 'no-repeat',
                                        backgroundPosition: 'center',
                                    }}
                                >

                                </Box>
                            </CardContent>
                        ), [book])
                    }
                </Card>
                {
                    !isMobile
                        ?
                        <Card sx={{ position: "inherit", top: 0, p: 2, mt: 2, display: isMd ? "none" : "block" }}>
                            {
                                useMemo(() => (
                                    <Box>
                                        <Typography variant="caption" fontSize={17} fontWeight={600}>
                                            Số lượng
                                        </Typography>
                                        <Box sx={{ display: 'flex', alignItems: 'center' }}>
                                            <IconButton
                                                disabled={quantity <= 1 ? true : false}
                                                onClick={() => setquantity((pre) => pre - 1)}
                                            >
                                                <RemoveIcon />
                                            </IconButton>
                                            <InputBase
                                                type="number"
                                                value={quantity}
                                                onChange={(e) => { +e.target.value < 1 ? 1 : setquantity(+e.target.value) }}
                                                sx={{
                                                    border: "1px solid rgb(166 166 176)", m: 1, borderRadius: 1, textAlign: 'center',
                                                    width: "15%",
                                                    '& input': {
                                                        textAlign: 'center',
                                                    },
                                                }}
                                            />
                                            <IconButton
                                                onClick={() => setquantity((pre) => pre + 1)}
                                            >
                                                <AddIcon />
                                            </IconButton>
                                        </Box>
                                        <Typography variant="caption" fontSize={18} fontWeight={600}>
                                            Tạm tính
                                        </Typography>
                                        <br />
                                        <Typography variant="caption" fontSize={25} fontWeight={600}>
                                            {new Intl.NumberFormat('vi-VN', config).format(
                                                price ? (price as number) * quantity : 0
                                            )}
                                        </Typography>
                                    </Box>
                                ), [book, quantity])
                            }

                            <Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: "space-between", height: 80 }}>
                                <Button variant="contained" color="error" >Mua ngay</Button>
                                <Button variant="outlined"
                                    onClick={() => handleAddCart()}
                                >
                                    Thêm vào giỏ hàng
                                </Button>
                            </Box>
                        </Card>
                        :
                        <></>
                }

            </Grid>
            <Grid item xs={isMobile || isTablet ? 12 : 6} sm={7.5} md={6}>
                <BookDetailInfo
                    book={book}
                    bookByCategory={bookByCategory}
                />
            </Grid>
            {
                !isMobile ?
                    <Grid item xs={3}>
                        <Card sx={{ position: "sticky", top: 0, p: 2, display: isMd ? "block" : "none" }}>
                            {
                                useMemo(() => (
                                    <Box>
                                        <Typography variant="caption" fontSize={17} fontWeight={600}>
                                            Số lượng
                                        </Typography>
                                        <Box sx={{ display: 'flex', alignItems: 'center' }}>
                                            <IconButton
                                                disabled={quantity <= 1 ? true : false}
                                                onClick={() => setquantity((pre) => pre - 1)}
                                            >
                                                <RemoveIcon />
                                            </IconButton>
                                            <InputBase
                                                type="number"
                                                value={quantity}
                                                onChange={(e) => { +e.target.value < 1 ? 1 : setquantity(+e.target.value) }}
                                                sx={{
                                                    border: "1px solid rgb(166 166 176)", m: 1, borderRadius: 1, textAlign: 'center',
                                                    width: "15%",
                                                    '& input': {
                                                        textAlign: 'center',
                                                    },
                                                }}
                                            />
                                            <IconButton
                                                onClick={() => setquantity((pre) => pre + 1)}
                                            >
                                                <AddIcon />
                                            </IconButton>
                                        </Box>
                                        <Typography variant="caption" fontSize={18} fontWeight={600}>
                                            Tạm tính
                                        </Typography>
                                        <br />
                                        <Typography variant="caption" fontSize={25} fontWeight={600}>
                                            {new Intl.NumberFormat('vi-VN', config).format(
                                                price ? (price as number) * quantity : 0
                                            )}
                                        </Typography>
                                    </Box>
                                ), [book, quantity])
                            }

                            <Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: "space-between", height: 80 }}>
                                <Button variant="contained" color="error"
                                    onClick={() => handleBuy()}
                                >
                                    Mua ngay
                                </Button>
                                <Button variant="outlined"
                                    onClick={() => handleAddCart()}
                                >
                                    Thêm vào giỏ hàng
                                </Button>
                            </Box>
                        </Card>
                    </Grid>
                    : <></>
            }

        </Grid >
    );
};

export default detail;