import { IProduct } from '@/types/backend';
import { Box, Paper, Typography, styled } from '@mui/material';
import nProgress from 'nprogress';
import { useNavigate } from 'react-router-dom';

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    height: 300,
    width: 160,
    borderRadius: 3,
    marginBottom: 1,
    '&:hover': {
        boxShadow: '0 0 10px rgba(0, 0, 0, 0.2)',
        cursor: "pointer"
    },
    [theme.breakpoints.between('sm', 'md')]: {
        height: 400,
        width: 240,
    },
}));

interface IProps {
    item: IProduct
}

const infoBookCategory = (props: IProps) => {

    const { item } = props;
    let price = item?.prices.find(items => items.active === true)?.price.toString();
    const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }
    const formated = new Intl.NumberFormat('vi-VN', config).format(+(price as string));
    const navigate = useNavigate();

    return (
        <Item
            onClick={() => {
                navigate(`/book/${item?.id}`)
                nProgress.start();
            }}
        >
            <Box
                sx={{
                    width: "100%",
                    height: "70%",
                    backgroundImage: `url(${import.meta.env.VITE_BACKEND_URL_UPLOAD}/storage/product/${item?.image})`,
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    backgroundPosition: 'center'
                }}>
            </Box>
            <Box
                sx={{
                    width: "100%"
                }}
            >
                <Typography
                    sx={{
                        width: "100%",
                        whiteSpace: "nowrap",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        p: '5 !important'
                    }}
                    color="rgb(39, 39, 42)"
                >
                    {item?.name}
                </Typography>
                <Typography sx={{
                    fontSize: 16,
                    fontWeight: 600
                }} >
                    {formated}
                </Typography>
            </Box>
        </Item>
    );
};

export default infoBookCategory;