import { useAppDispatch, useAppSelector } from '@/redux/hook';
import { setItemInCart } from '@/redux/slide/cartSlide';
import { IInfoCart, IProduct } from '@/types/backend';
import { DeleteOutlined } from '@ant-design/icons';
import { Button, Card, Checkbox, CheckboxProps, Col, Divider, GetProp, Row, Select } from 'antd';
import InputNumber from 'rc-input-number';
import { useMemo, useState } from 'react';
import 'styles/input.number.less'
import ButtonPayPal from '@/components/client/home/paypal/button';
import { PayPalScriptProvider } from '@paypal/react-paypal-js';
import { isMobile, isTablet } from 'react-device-detect';
import { useNavigate } from 'react-router-dom';

const indexCart = () => {

    const dispatch = useAppDispatch();
    const listcart = useAppSelector((state) => state.cart.cart)
    const user = useAppSelector((state) => state.account)
    const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }
    const [checkedList, setCheckedList] = useState<IInfoCart[]>([]);
    const navigate = useNavigate();

    const checkAll = listcart?.length === checkedList.length;
    const indeterminate = checkedList.length > 0 && checkedList.length < listcart?.length;

    const onChange = (val: any, item: any) => {
        if (listcart?.length > 0) {
            const cplistcart = [...listcart];
            let index = cplistcart?.findIndex(items => items.id === item.id)
            if (index !== -1) {
                const updatedItem = { ...cplistcart[index] };
                updatedItem.quantity = val;
                updatedItem.total = (+val * +updatedItem.price).toString();
                cplistcart[index] = updatedItem;

                localStorage.setItem("arrProduct", JSON.stringify(cplistcart));
                dispatch(setItemInCart(cplistcart));

            }
            if (checkedList?.length > 0) {
                const cpchecklist = [...checkedList];
                let index = cpchecklist?.findIndex(items => items?.id === item?.id)
                if (index !== -1) {
                    const updatedChecklistItem = { ...cpchecklist[index] };
                    updatedChecklistItem.quantity = val;
                    updatedChecklistItem.total = (+val * +updatedChecklistItem.price).toString();
                    cpchecklist[index] = updatedChecklistItem;
                    setCheckedList(cpchecklist)
                }
            }
        }
    };

    const onChangeCheck = (checked: boolean, item: IInfoCart) => {
        setCheckedList(prevList => {
            if (checked) {
                return [...prevList, item];
            } else {
                return prevList.filter(cartItem => cartItem.id !== item.id);
            }
        });
    };

    const onCheckAllChange: CheckboxProps['onChange'] = (e) => {
        setCheckedList(e.target.checked ? listcart : []);
    };

    const handleDeleteItem = (item: IInfoCart) => {
        const cplistcart = [...listcart];
        if (cplistcart?.length > 0) {
            let arrupdate = cplistcart?.filter((items) => items?.id !== item?.id);
            dispatch(setItemInCart(arrupdate));
            localStorage.setItem("arrProduct", JSON.stringify(arrupdate));
        }
    }

    const handleDeleteAll = () => {
        dispatch(setItemInCart([]));
        localStorage.setItem("arrProduct", JSON.stringify([]));
    }

    const handleOrder = () => {
        if (user?.isAuthenticated) {
            let sum = checkedList?.reduce((sum, item) => +sum + (+item.total as number), 0).toString()
            let arr = checkedList?.length > 0 ? checkedList : [];
            navigate("/order", { state: { sum, arr } })
        } else {
            navigate("/login")
        }
    }

    return (
        <div>
            <h2
                style={{ margin: "5px 0" }}
            >Giỏ hàng</h2>
            <Row gutter={[16, 16]}>
                <Col span={!isMobile ? 19 : 24}>
                    <Card>
                        <Row gutter={10}>
                            <Col span={!isMobile ? 13 : 22}>
                                <Checkbox indeterminate={indeterminate} onChange={onCheckAllChange} checked={checkAll}>
                                    <span style={{
                                        fontSize: 16,
                                        fontWeight: 600
                                    }}
                                    >
                                        Tất cả({checkedList?.length ?? 0})
                                    </span>
                                </Checkbox>
                            </Col>
                            {
                                !isMobile
                                    ?
                                    <>
                                        <Col span={3}>
                                            <span
                                                style={{
                                                    fontSize: 16,
                                                    fontWeight: 600
                                                }}
                                            >
                                                Đơn giá
                                            </span>
                                        </Col>
                                        <Col span={3}>
                                            <span
                                                style={{
                                                    fontSize: 16,
                                                    fontWeight: 600
                                                }}
                                            >
                                                Số lượng
                                            </span>
                                        </Col>
                                        <Col span={3}>
                                            <span
                                                style={{
                                                    fontSize: 16,
                                                    fontWeight: 600
                                                }}
                                            >
                                                Thành tiền
                                            </span>
                                        </Col>
                                    </>
                                    :
                                    <></>
                            }

                            <Col span={2}>
                                <span
                                    style={{
                                        fontSize: 16,
                                        fontWeight: 600
                                    }}
                                >
                                    <DeleteOutlined style={{
                                        cursor: "pointer",
                                        color: "#ff9800"
                                    }}
                                        onClick={() => handleDeleteAll()}

                                    />
                                </span>
                            </Col>
                        </Row>
                    </Card>
                    {
                        listcart?.length > 0
                        && listcart?.map((item, index) => {
                            return (
                                <Card key={`cart-list-card-index-${index}`} style={{ marginTop: "5px" }}>
                                    <Row gutter={10}>
                                        <Col span={!isMobile ? 13 : 22}>
                                            <Checkbox
                                                checked={checkedList.some(cartItem => cartItem.id === item.id)}
                                                onChange={(e) => onChangeCheck(e.target.checked, item)}
                                                style={{
                                                    textOverflow: "ellipsis",
                                                    whiteSpace: "nowrap",
                                                    overflow: "hidden",
                                                    width: "100%"
                                                }}
                                            >
                                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                                    <div style={{
                                                        width: "100px",
                                                        height: "90px",
                                                        backgroundImage: `url(${import.meta.env.VITE_BACKEND_URL_UPLOAD}/storage/product/${item?.img})`,
                                                        backgroundSize: 'cover',
                                                        backgroundRepeat: 'no-repeat',
                                                        backgroundPosition: 'center'
                                                    }}>

                                                    </div>
                                                    <div style={{
                                                        display: "flex", flexDirection: "column"
                                                    }}>
                                                        {item?.name}
                                                        {
                                                            isMobile
                                                                ?
                                                                <>
                                                                    <span
                                                                        style={{
                                                                            fontSize: 17,
                                                                            fontWeight: 600
                                                                        }}
                                                                    >
                                                                        {
                                                                            new Intl.NumberFormat('vi-VN', config).format(+(item?.price as string))
                                                                        }
                                                                    </span>
                                                                    <InputNumber
                                                                        style={{ width: 100 }}
                                                                        onChange={(val) => onChange(val, item)}
                                                                        min={1}
                                                                        defaultValue={+item?.quantity}
                                                                    />
                                                                </>
                                                                :
                                                                <></>
                                                        }
                                                    </div>
                                                </div>
                                            </Checkbox>
                                        </Col>
                                        {
                                            !isMobile
                                                ?
                                                <>
                                                    <Col span={3}>
                                                        <div style={{ height: "90px", display: 'flex', alignItems: 'center' }}>
                                                            <span
                                                                style={{
                                                                    fontSize: 17,
                                                                    fontWeight: 600
                                                                }}
                                                            >
                                                                {
                                                                    new Intl.NumberFormat('vi-VN', config).format(+(item?.price as string))
                                                                }
                                                            </span>
                                                        </div>
                                                    </Col>
                                                    <Col span={3}>
                                                        <div style={{ height: "90px", display: 'flex', alignItems: 'center' }}>
                                                            <InputNumber
                                                                style={{ width: 100 }}
                                                                onChange={(val) => onChange(val, item)}
                                                                min={1}
                                                                defaultValue={+item?.quantity}
                                                            />
                                                        </div>
                                                    </Col>
                                                    <Col span={3}>
                                                        <div style={{ height: "90px", display: 'flex', alignItems: 'center' }}>
                                                            <span
                                                                style={{
                                                                    fontSize: 17,
                                                                    fontWeight: 600,
                                                                    color: "#ff7043"
                                                                }}
                                                            >
                                                                {
                                                                    new Intl.NumberFormat('vi-VN', config).format(+(item?.total as string))
                                                                }
                                                            </span>
                                                        </div>
                                                    </Col>
                                                </>
                                                :
                                                <></>
                                        }
                                        <Col span={2}>
                                            <div style={{ height: "90px", display: 'flex', alignItems: 'center' }}>
                                                <DeleteOutlined style={{
                                                    cursor: "pointer",
                                                    color: "#ff9800"
                                                }}
                                                    onClick={() => handleDeleteItem(item)}
                                                />
                                            </div>
                                        </Col>
                                    </Row>
                                </Card>
                            )
                        })
                    }
                </Col>
                <Col span={!isMobile ? 5 : 24}>
                    <Card>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                            <span>Tạm tính</span>
                            <span>
                                {
                                    new Intl.NumberFormat('vi-VN', config).format(
                                        +checkedList.reduce((sum, item) => +sum + (+item.total as number), 0)
                                    )
                                }
                            </span>
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                            <span>Giảm giá</span>
                            <span>
                                {
                                    new Intl.NumberFormat('vi-VN', config).format(
                                        0
                                    )
                                }
                            </span>
                        </div>
                        <Divider />
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                            <span>Tổng tiền</span>
                            <span>
                                <span
                                    style={{
                                        fontSize: 17,
                                        fontWeight: 600,
                                        color: "#ff7043"
                                    }}
                                >
                                    {
                                        new Intl.NumberFormat('vi-VN', config).format(
                                            +checkedList.reduce((sum, item) => +sum + (+item.total as number), 0)
                                        )
                                    }
                                </span>
                            </span>
                        </div>
                        <Button style={{ width: "100%", marginTop: "15px", marginBottom: "15px" }} danger
                            disabled={checkedList?.length > 0 ? false : true}
                            onClick={() => handleOrder()}
                        >
                            Mua hàng
                        </Button>
                        {
                            useMemo(() => (
                                checkedList?.length > 0 && user?.isAuthenticated
                                    ?
                                    <>
                                        <PayPalScriptProvider options={{ clientId: `${import.meta.env.VITE_CLIENT_ID}` }}>
                                            <ButtonPayPal
                                                sum={+checkedList?.reduce((sum, item) => +sum + (+item.total as number), 0).toString()}
                                                arr={checkedList}
                                            />
                                        </PayPalScriptProvider>

                                    </>
                                    :
                                    <></>
                            ), [checkedList])
                        }
                    </Card>
                </Col>
            </Row>
        </div >
    );
};

export default indexCart;