import CategoryBooks from "@/components/client/home/category/category.book";
import { Container, Grid } from "@mui/material";
import ContentBook from "@/components/client/home/books/content.book";
import { isMobile, isTablet } from "react-device-detect";
import { useCallback, useEffect, useMemo, useState } from "react";
import { IMeta, IProduct } from "@/types/backend";
import { callFetchAllProduct } from "@/config/api";
import _ from "lodash";
import { sfAnd, sfEqual, sfGe, sfLe, sfLike } from "spring-filter-query-builder";
import { useOutletContext } from "react-router-dom";

interface FormFilter {
    valueFrom?: number,
    valueTo?: number,
    authors?: string[],
    publishers?: string[]
}

const homeClient = () => {

    const [books, setbooks] = useState<IProduct[]>([]);
    const [meta, setmeta] = useState<IMeta>();
    const [current, setCurrent] = useState(1);
    const [pageSize, setPageSize] = useState(20);
    const [filter, setFilter] = useState("");
    const [sortQuery, setSortQuery] = useState("sort=name,asc");
    const [filterAll, setfilterAll] = useState<FormFilter | null>()

    const [search]: [string] = useOutletContext();

    const filterCategory = useCallback((name: string) => {
        setFilter(name);
    }, [filter])

    const closeCategory = useCallback((name: string) => {
        setFilter(name);
        setfilterAll(null)
    }, [filter, filterAll])

    useEffect(() => {
        handleFetchBooks()
    }, [filter, filterAll, search])

    useEffect(() => {
        window.scrollTo(0, 0)
    }, [filter, filterAll, current])


    const handleFetchBooks = useCallback(async () => {
        let query = `page=${current}&size=${pageSize}`;
        if (search) {
            query = `&${search}`;
            let q = sfLike("name", search).toString();

            query = `&filter=${encodeURIComponent(q)}`;
        }

        if (filter) {
            query = `&${filter}`;
            let q = sfEqual("genre", filter).toString();

            query = `&filter=${encodeURIComponent(q)}`;
        }
        if (filterAll) {
            let qr: string = "";
            if (filterAll.valueFrom) {
                qr = sfAnd([sfEqual('prices.active', "true"), sfGe("prices.price", filterAll.valueFrom)]).toString()
            }
            if (filterAll.valueTo) {
                qr = qr ? qr + " and " + sfAnd([sfEqual('prices.active', "true"), sfLe("prices.price", filterAll.valueTo)]).toString()
                    : sfAnd([sfEqual('prices.active', "true"), sfLe("prices.price", filterAll.valueTo)]).toString()
            }
            if (filterAll.authors) {
                filterAll.authors.forEach((item, index) => {
                    if (index > 0) {
                        qr += " or " + `${sfLike("author.name", item)}`
                    } else {
                        qr = qr ? qr + " and " + `${sfLike("author.name", item)}` : `${sfLike("author.name", item)}`
                    }
                })
            }
            if (filterAll.publishers) {
                filterAll.publishers.forEach((item, index) => {
                    if (index > 0) {
                        qr += " or " + `${sfLike("publisher.name", item)}`
                    } else {
                        qr = qr ? qr + " and " + `${sfLike("publisher.name", item)}` : `${sfLike("publisher.name", item)}`
                    }
                })
            }
            query = `&filter=${encodeURIComponent(qr)}`;
        }
        if (sortQuery) {
            query += `&${sortQuery}`;
        }

        const res = await callFetchAllProduct(query)
        if (res?.data?.result) {
            setbooks(res?.data?.result)
            setmeta(res?.data?.meta)
        }
    }, [books, current, filter, filterAll, search])

    return (
        <div>
            <Grid container spacing={!isMobile || isTablet ? 5 : 0}>
                {
                    !isMobile || isTablet
                        ?
                        <>
                            <Grid item xs={3} sm={3.5} md={2.5}>
                                {
                                    useMemo(() => (
                                        <CategoryBooks
                                            filterCategory={filterCategory}
                                            closeCategory={closeCategory}
                                        />
                                    ), [filter])
                                }
                            </Grid>
                            <Grid item xs={9} sm={8.5} md={9.5}>
                                {
                                    useMemo(() => (
                                        <ContentBook
                                            books={books}
                                            meta={meta}
                                            setCurrent={setCurrent}
                                            handleFetchBooks={handleFetchBooks}
                                            current={current}
                                            pageSize={pageSize}
                                            setfilterAll={setfilterAll}
                                            closeCategory={closeCategory}
                                        />
                                    ), [handleFetchBooks])
                                }

                            </Grid>
                        </>
                        :
                        <>
                            <Grid item xs={12}>
                                {
                                    useMemo(() => (
                                        <ContentBook
                                            books={books}
                                            meta={meta}
                                            setCurrent={setCurrent}
                                            handleFetchBooks={handleFetchBooks}
                                            current={current}
                                            pageSize={pageSize}
                                            setfilterAll={setfilterAll}
                                            closeCategory={closeCategory}
                                        />
                                    ), [handleFetchBooks])
                                }
                            </Grid>
                        </>
                }
            </Grid>
        </div>
    );
};

export default homeClient;