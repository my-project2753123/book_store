import { Button, Card, Col, Divider, Form, Input, Row, Select, message, notification } from 'antd';
import 'styles/auth.scss'
import { IUser } from '@/types/backend';
import { useAppSelector } from '@/redux/hook';
import { callUpdateUser, callUserById } from '@/config/api';
import { useEffect, useRef, useState } from 'react';
const { Option } = Select;

const formAccount = () => {

    const userRedux = useAppSelector((state: any) => state.account.user)
    const [form] = Form.useForm();
    const [isSubmit, setIsSubmit] = useState<boolean>(false);

    const handleFetchUser = async () => {
        const res = await callUserById(userRedux?.id);
        if (res?.data) {
            form.setFieldsValue(res.data)
        }
    }

    useEffect(() => {
        handleFetchUser()
    }, [])

    const onFinish = async (value: IUser) => {
        const { name, email, password, gender, age, address } = value;
        setIsSubmit(true)
        const user: IUser = {
            id: userRedux?.id,
            name: name,
            email: email,
            password: password,
            age: age,
            gender: gender,
            address: address,
        }
        const res = await callUpdateUser(user);
        if (res?.data?.id) {
            message.success("Cập nhật user thành công!");
        } else {
            notification.error({
                message: "có lỗi xảy ra",
                description:
                    res?.message && Array.isArray(res?.message) ? res?.message[0] : res?.message, duration: 5
            })
        }
    }

    return (
        <div>
            <Row gutter={[16, 16]}>
                <Card style={{ marginTop: "15px" }}>
                    <Form<IUser>
                        name="basic"
                        onFinish={onFinish}
                        autoComplete="off"
                        form={form}
                    >
                        <Row gutter={[16, 5]}>
                            <Col span={12}>
                                <Form.Item
                                    labelCol={{ span: 24 }} //whole column
                                    label="Họ tên"
                                    name="name"
                                    rules={[{ required: true, message: 'Họ và tên không được để trống!' }]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item
                                    labelCol={{ span: 24 }} //whole column
                                    label="Email"
                                    name="email"
                                    rules={[{ required: true, message: 'Email không được để trống!' }]}
                                >
                                    <Input disabled />
                                </Form.Item>
                            </Col>

                            <Col span={12}>
                                <Form.Item
                                    labelCol={{ span: 24 }} //whole column
                                    label="Mật khẩu"
                                    name="password"
                                    rules={[{ required: true, message: 'Mật khẩu không được để trống!' }]}
                                >
                                    <Input.Password />
                                </Form.Item>
                            </Col>

                            <Col span={12}>
                                <Form.Item
                                    labelCol={{ span: 24 }} //whole column
                                    label="Tuổi"
                                    name="age"
                                    rules={[{ required: true, message: 'Tuổi không để trống!' }]}
                                >
                                    <Input type='number' />
                                </Form.Item>
                            </Col>

                            <Col span={12}>
                                <Form.Item
                                    labelCol={{ span: 24 }} //whole column
                                    label="Giới tính"
                                    name="gender"
                                    rules={[{ required: true, message: 'Giới tính không để trống!' }]}
                                >
                                    <Select
                                        // placeholder="Select a option and change input text above"
                                        // onChange={onGenderChange}
                                        allowClear
                                    >
                                        <Option value="MALE">Nam</Option>
                                        <Option value="FEMALE">Nữ</Option>
                                        <Option value="OTHER">Khác</Option>
                                    </Select>

                                </Form.Item>
                            </Col>

                            <Col span={12}>
                                <Form.Item
                                    labelCol={{ span: 24 }} //whole column
                                    label="Địa chỉ"
                                    name="address"
                                    rules={[{ required: true, message: 'Địa chỉ không để trống!' }]}
                                >
                                    <Input />
                                </Form.Item>
                            </Col>

                            <Col span={12}>
                                <Form.Item
                                >
                                    <Button type="primary" htmlType="submit"
                                        loading={isSubmit}
                                    >
                                        Cập nhật
                                    </Button>
                                </Form.Item>
                            </Col>



                        </Row>

                    </Form>

                </Card>
            </Row>
        </div>
    );
};

export default formAccount;