import 'styles/auth.scss'
import OrderClient from '../order';
import FormAccount from './index.form';

const accountClient = () => {
    return (
        <div>
            <FormAccount />
            <OrderClient />
        </div>
    );
};

export default accountClient;