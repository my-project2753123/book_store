import { Payment, Status } from "./enum.config";

export interface IBackendRes<T> {
    error?: string | string[];
    message: string;
    statusCode: number | string;
    data?: T;
}

export interface IUser {
    id?: string;
    name: string;
    email: string;
    password?: string;
    age: number;
    gender: string;
    address: string;
    createdAt?: string;
    createdBy?: string;
    updatedAt?: string;
    updatedBy?: string;
    active?: boolean;
    role?: {
        id: string,
        name: string
    }
}

export interface IAccount {
    access_token?: string;
    user: {
        id: string;
        email: string;
        name: string;
        role: {
            id?: string;
            name?: string;
            permissions?: {
                id: string;
                name: string;
                apiPath: string;
                method: string;
                module: string;
            }[]
        }
    }
}

export interface IGetAccount extends Omit<IAccount, "access_token"> { }

export interface ITable<T> {
    dataSource?: T[],
    columns?: ProColumns<T>[]
}

export interface IPagination<T> {
    meta: {
        page: number;
        pageSize: number;
        pages: number;
        total: number;
    },
    result: T[]
}

export interface IPermission {
    id?: string;
    name?: string;
    apiPath?: string;
    method?: string;
    module?: string;
    createdAt?: string;
    createdBy?: string;
    updatedAt?: string;
    updatedBy?: string;
}

export interface IRole {
    id?: string;
    name: string;
    description: string;
    active: boolean;
    permissions: IPermission[] | string[]
    createdAt?: string;
    createdBy?: string;
    updatedAt?: string;
    updatedBy?: string;
}

export interface IAuthor {
    id?: string;
    name: string;
    description: string;
    logo: string;
    createdAt?: string;
    createdBy?: string;
    updatedAt?: string;
    updatedBy?: string;
}

export interface IPublisher extends IAuthor { }

export interface IProduct {
    id?: string;
    name: string;
    description: string;
    prices: {
        id?: string,
        price: string | number,
        active?: boolean
    }[],
    image: string,
    quantity: string,
    author: {
        id: string,
        name: string
    };
    publisher?: {
        id: string,
        name: string
    };
    category: string;
    genre: string;
    quantityPurchased?: number;
    active?: boolean;
    createdAt?: string;
    createdBy?: string;
    updatedAt?: string;
    updatedBy?: string;
}

export interface IMeta {
    page: number;
    pageSize: number;
    pages: number;
    total: number;
}

export interface ICategory {
    category: string;
    genres: string[];
}

export interface IInfoCart {
    id?: string;
    img?: string;
    name?: string;
    price: string;
    total: string;
    quantity: string;
}

export interface IPayPal {
    sum: number,
    orderId?: string,
    status?: string
}

export interface IOrder {
    id?: string,
    payment: string,
    status: string,
    discount: number,
    active?: boolean,
    address?: string,
    details: IOrderDetail[]
    user?: {
        id: string
    }
}

export interface IOrderDetail {
    quantity: number,
    book: {
        id: string
    }
}

export interface IOrderAD {
    id: string,
    payment: string,
    status: string,
    active: boolean,
    totalAmount: number,
    totalAmountPlayable: number,
    discount: number,
    details: IOrderDetailAD[],
    user: {
        id: string,
        name?: string,
        email?: string,
        address?: string
    },
    createdAt?: string;
    createdBy?: string;
    updatedAt?: string;
    updatedBy?: string;
}

export interface IOrderDetailAD {
    id: string,
    quantity: number,
    price: number,
    discount: number,
    amount: number,
    active?: boolean,
    createdAt?: string;
    createdBy?: string;
    updatedAt?: string;
    updatedBy?: string;
    book: {
        id: string,
        image?: string,
        name?: string
    }
}

export interface IStatisticalOrder {
    total: string,
    totalPayable: string,
    quantity: string,
    months: {
        month: string,
        total: string
    }[]
}


