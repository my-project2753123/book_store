import { IInfoCart } from "@/types/backend";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

interface IState {
    isFetching: boolean;
    cart: IInfoCart[]
}

const initialState: IState = {
    isFetching: true,
    cart: []

};

export const fetchCartInRedux = createAsyncThunk(
    'cart/fetchCart',
    async () => {
        const arrCart = localStorage.getItem("arrProduct");
        if (arrCart) {
            return JSON.parse(arrCart);
        }
        return [];
    }
)

export const cartSlide = createSlice({
    name: 'cart',
    initialState,
    // The `reducers` field lets us define reducers and generate associated actions
    reducers: {
        // Use the PayloadAction type to declare the contents of `action.payload`
        setItemInCart: (state, action) => {
            state.cart = [...action.payload];
        },


    },
    extraReducers: (builder) => {
        // Add reducers for additional action types here, and handle loading state as needed
        builder.addCase(fetchCartInRedux.pending, (state, action) => {
            state.isFetching = false;
            // Add user to the state array
            // state.courseOrder = action.payload;
        })

        builder.addCase(fetchCartInRedux.rejected, (state, action) => {
            state.isFetching = false;
            // Add user to the state array
            // state.courseOrder = action.payload;
        })

        builder.addCase(fetchCartInRedux.fulfilled, (state, action) => {
            state.isFetching = false;
            if (action.payload) {
                state.cart = action.payload
            }
            // Add user to the state array

            // state.courseOrder = action.payload;
        })
    },

});

export const {
    setItemInCart,
} = cartSlide.actions;

export default cartSlide.reducer;