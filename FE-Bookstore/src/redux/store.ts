import { Action, ThunkAction, configureStore } from '@reduxjs/toolkit'
import accountReducer from './slide/authSlide'
import userReducer from './slide/userSlide';
import permissionReducer from './slide/permissionSlide'
import roleReducer from './slide/roleSlide'
import authorReducer from './slide/authorSilde';
import publisherReducer from './slide/publisherSlide';
import productReducer from './slide/productSlide';
import cartReducer from './slide/cartSlide';
import orderReducer from './slide/orderSilde';

export const store = configureStore({
    reducer: {
        account: accountReducer,
        user: userReducer,
        permission: permissionReducer,
        role: roleReducer,
        author: authorReducer,
        publisher: publisherReducer,
        product: productReducer,
        cart: cartReducer,
        order: orderReducer
    },
})

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
>;