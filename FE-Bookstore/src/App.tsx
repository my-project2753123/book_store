import AppRoutes from 'components/share/app.route';
import styles from 'styles/app.module.scss';

const App = () => {
  return (
    <div className={styles.container}>
      <AppRoutes />
    </div>
  )
}

export default App
