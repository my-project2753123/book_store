import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import { BrowserRouter } from 'react-router-dom'
import 'nprogress/nprogress.css'
import 'react-multi-carousel/lib/styles.css';
import { Provider } from 'react-redux'
import { store } from '@/redux/store'
import { ConfigProvider } from 'antd'
import vi_VN from 'antd/locale/vi_VN';


ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <ConfigProvider locale={vi_VN}>
          <App />
        </ConfigProvider>
      </Provider>
    </BrowserRouter>
  </React.StrictMode>,
)
