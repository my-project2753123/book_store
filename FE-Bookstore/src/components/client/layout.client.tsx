import { Box, Container, Grid, Typography } from "@mui/material";
import NavBarClient from "./share/navbar.client";
import 'styles/app.client.scss'
import { Outlet } from "react-router-dom";
import { useEffect, useRef, useState } from "react";
import { useAppDispatch } from "@/redux/hook";
import { fetchAccount } from "@/redux/slide/authSlide";
import { fetchCartInRedux } from "@/redux/slide/cartSlide";
import { styled } from '@mui/material/styles';

const LayoutClient = () => {

    const dispatch = useAppDispatch();
    const [search, setsearch] = useState<string>("")

    useEffect(() => {
        if (window.location.pathname === '/login'
            || window.location.pathname === '/register'
        )
            return;
        dispatch(fetchAccount())
        dispatch(fetchCartInRedux())
    }, [])

    const CustomContainer = styled(Container)(({ theme }) => ({
        [theme.breakpoints.down('md')]: {
            padding: '0 !important',
        },
    }));

    return (
        <Box sx={{ flexGrow: 1 }}>
            <Grid container spacing={5}>
                <Grid item xs={12}>
                    <NavBarClient
                        setsearch={setsearch}
                    />
                </Grid>
                <Grid item xs={12}>
                    <Box className="fullBackground" >
                        <CustomContainer maxWidth={false}>
                            <Outlet context={[search]} />
                        </CustomContainer>
                        <Box sx={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            p: 3,
                            position: "inherit",
                            bottom: 0,
                        }}>
                            <Typography variant="h6">
                                MUI ©{new Date().getFullYear()} Created by Cuong Nguyen
                            </Typography>
                        </Box>

                    </Box>

                </Grid>
            </Grid>
        </Box>

    );
};

export default LayoutClient;