
import { callCreateOrder, callTransferPaiDOrder } from '@/config/api';
import { IInfoCart, IOrder, IOrderAD, IOrderDetail, IPayPal } from '@/types/backend';
import { PayPalButtons } from '@paypal/react-paypal-js';
import { message, notification } from 'antd';
import { useCallback } from 'react';

interface IProps extends IPayPal {
    dataInit: IOrderAD
}

const buttonPayPalInAccount = (props: IProps) => {

    const { sum, dataInit } = props;

    const createOrder = useCallback(async () => {
        return fetch(`${import.meta.env.VITE_BACKEND_URL_UPLOAD}/api/v1/init`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                sum: (sum / 30000).toFixed(2)
            }),
        })
            .then((response) => response.json())
            .then((order) => {
                return order?.data?.payId;
            });
    }, [sum])

    const onApprove = (data: any) => {
        return fetch(`${import.meta.env.VITE_BACKEND_URL_UPLOAD}/api/v1/capture?token=${data?.orderID}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                orderID: data.orderID
            })
        })
            .then((response) => response.json())
            .then(async (orderData) => {
                const name = orderData.payId;

                if (dataInit?.id) {
                    const res = await callTransferPaiDOrder(dataInit?.id);
                    if (res?.data) {
                        message.success("Thanh toán thành công");
                    }
                    else {
                        notification.error({
                            message: 'Có lỗi xảy ra',
                            description: res.message
                        });
                    }
                }
            });
    }


    return (
        <>
            <PayPalButtons
                createOrder={createOrder}
                forceReRender={[sum]}
                onApprove={onApprove}
            />
        </>
    );
}

export default buttonPayPalInAccount;