
import { callCreateOrder } from '@/config/api';
import { IInfoCart, IOrder, IOrderDetail, IPayPal } from '@/types/backend';
import { PayPalButtons } from '@paypal/react-paypal-js';
import { message, notification } from 'antd';
import { useCallback } from 'react';

interface IProps extends IPayPal {
    arr: IInfoCart[]
}

const buttonPayPal = (props: IProps) => {

    const { sum, arr } = props;

    const createOrder = useCallback(async () => {
        return fetch(`${import.meta.env.VITE_BACKEND_URL_UPLOAD}/api/v1/init`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                sum: (sum / 30000).toFixed(2)
            }),
        })
            .then((response) => response.json())
            .then((order) => {
                return order?.data?.payId;
            });
    }, [sum])

    const onApprove = (data: any) => {
        return fetch(`${import.meta.env.VITE_BACKEND_URL_UPLOAD}/api/v1/capture?token=${data?.orderID}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                orderID: data.orderID
            })
        })
            .then((response) => response.json())
            .then(async (orderData) => {
                const name = orderData.payId;
                const cparr = arr?.length > 0 ? [...arr] : null;
                let arrdt = cparr?.map(item => {
                    return {
                        quantity: +item?.quantity,
                        book: {
                            id: item.id
                        }
                    } as IOrderDetail
                })
                const obj: IOrder = {
                    payment: "TRANSFER",
                    status: "PAID",
                    discount: 0,
                    active: true,
                    details: arrdt as IOrderDetail[],
                }
                if (orderData?.data?.status) {
                    const res = await callCreateOrder(obj);
                    if (res?.data) {
                        message.success("Thanh toán thành công");
                    }
                    else {
                        notification.error({
                            message: 'Có lỗi xảy ra',
                            description: res.message
                        });
                    }
                }
            });
    }


    return (
        <>
            <PayPalButtons
                createOrder={createOrder}
                forceReRender={[sum]}
                onApprove={onApprove}
            />
        </>
    );
}

export default buttonPayPal;