import { ModalForm, ProFormDigit } from "@ant-design/pro-components";
import { Checkbox, Col, Divider, Form, GetProp, Row } from "antd";
import { isMobile, isTablet } from "react-device-detect";
import 'react-quill/dist/quill.snow.css';
import 'styles/reset.scss'
import { useCallback, useEffect, useState } from "react";
import { callFetchAllAuthor, callFetchAllPublisher } from "@/config/api";

interface IProps {
    openModal: boolean
    setOpenModal: (v: boolean) => void;
    setfilterAll: (v: any) => void
}

export interface IItem {
    label: string;
    value: string;
}

const modalClientBook = (props: IProps) => {
    const { openModal, setOpenModal, setfilterAll } = props;

    const [form] = Form.useForm();
    const [itemAuthor, setitemAuthor] = useState<IItem[]>([])
    const [itemPublisher, setitemPublisher] = useState<IItem[]>([])

    const onChangeAuthor: GetProp<typeof Checkbox.Group, 'onChange'> = (checkedValues) => {
        form.setFieldsValue({ authors: checkedValues });
    };

    const onChangePublisher: GetProp<typeof Checkbox.Group, 'onChange'> = (checkedValues) => {
        form.setFieldsValue({ publishers: checkedValues });
    };

    const handleFetchAuthor = useCallback(async () => {
        const res = await callFetchAllAuthor("page=1&size=100");
        if (res?.data?.result) {
            let arr = res?.data?.result
                .map((item, index) => {
                    return { label: item.name, value: item.name }
                })
            setitemAuthor(arr);
        }
    }, [itemAuthor])

    const handleFetchPublisher = useCallback(async () => {
        const res = await callFetchAllPublisher("page=1&size=100");
        if (res?.data?.result) {
            let arr = res?.data?.result
                .map((item, index) => {
                    return { label: item.name, value: item.name }
                })
            setitemPublisher(arr);
        }
    }, [itemPublisher])

    const onFinishs = async (values: any) => {
        setfilterAll(values)
        handleReset()
    }

    const handleReset = () => {
        form.resetFields();
        setOpenModal(false)
    }

    useEffect(() => {
        handleFetchAuthor();
        handleFetchPublisher();
    }, [])

    return (
        <>
            <ModalForm
                title={<h3>Tất cả bộ lọc</h3>}
                open={openModal}
                modalProps={{
                    onCancel: () => { handleReset() },
                    afterClose: () => handleReset(),
                    destroyOnClose: true,
                    width: isMobile || isTablet ? "100%" : 900,
                    footer: null,
                    keyboard: false,
                    maskClosable: false,
                }}
                scrollToFirstError={true}
                preserve={false}
                form={form}
                onFinish={onFinishs}
            >
                <Row gutter={16}>
                    <Col span={24}>
                        <h3>Giá</h3>
                    </Col>
                    <Col span={12}>
                        <ProFormDigit
                            label="từ"
                            name="valueFrom"
                            placeholder="Nhập giá từ"
                        />
                    </Col>
                    <Col span={12}>
                        <ProFormDigit
                            label="đến"
                            name="valueTo"
                            placeholder="Nhập giá đến"
                        />
                    </Col>
                    <Divider />
                    <Col span={24}>
                        <h3>Tác giả</h3>
                    </Col>
                    <Col span={24}>
                        <Form.Item name="authors">
                            <Checkbox.Group
                                onChange={onChangeAuthor}
                            >
                                <Row>
                                    {
                                        itemAuthor?.length > 0 &&
                                        itemAuthor?.map((item, index) => {
                                            return (
                                                <Col key={`itemauthor-index-${index}`} span={8}>
                                                    <Checkbox value={`${item.value}`}>{item.label}</Checkbox>
                                                </Col>
                                            )
                                        })
                                    }

                                </Row>
                            </Checkbox.Group>
                        </Form.Item>
                    </Col>
                    <Divider />
                    <Col span={24}>
                        <h3>Nhà xuất bản</h3>
                    </Col>
                    <Col span={24}>
                        <Form.Item name="publishers">
                            <Checkbox.Group
                                onChange={onChangePublisher}
                            >
                                <Row>
                                    {
                                        itemPublisher?.length > 0 &&
                                        itemPublisher?.map((item, index) => {
                                            return (
                                                <Col key={`itempublisher-index-${index}`} span={8}>
                                                    <Checkbox value={`${item.value}`}>{item.label}</Checkbox>
                                                </Col>
                                            )
                                        })
                                    }

                                </Row>
                            </Checkbox.Group>
                        </Form.Item>
                    </Col>
                </Row >
            </ModalForm >
        </>
    );
};

export default modalClientBook;