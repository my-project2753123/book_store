import { IProduct } from "@/types/backend";
import { Box, Typography } from "@mui/material";

interface IProps {
    item: IProduct
}

const infoBooks = (props: IProps) => {

    const { item } = props;
    let price = item?.prices.find(items => items.active === true)?.price.toString();
    const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }
    const formated = new Intl.NumberFormat('vi-VN', config).format(+(price as string));
    return (
        <Box
            sx={{
                width: '100%',
                height: "100%",
                display: 'flex',
                flexDirection: "column",
                alignItems: "center"
            }}
        >
            <Box
                sx={{
                    width: { xs: "100%", md: "67%", sm: "80%" },
                    height: "70%",
                    backgroundImage: `url(${import.meta.env.VITE_BACKEND_URL_UPLOAD}/storage/product/${item?.image})`,
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                    backgroundPosition: 'center'
                }}
            >
            </Box>
            <Box
                sx={{
                    width: "100%"
                }}
            >
                <Typography sx={{
                    fontSize: 20,
                    fontWeight: 600
                }} color="rgb(255, 66, 78)">
                    {formated}
                </Typography>
                <Typography
                    sx={{
                        fontSize: 18,
                        fontWeight: 550
                    }}
                    color="rgb(128, 128, 137)">
                    {item?.author?.name}
                </Typography>
                <Typography
                    sx={{
                        width: "100%",
                        whiteSpace: "nowrap",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                        p: '5 !important'
                    }}
                    color="rgb(39, 39, 42)"
                >
                    {item?.name}
                </Typography>
            </Box >
        </Box >
    );
};

export default infoBooks;