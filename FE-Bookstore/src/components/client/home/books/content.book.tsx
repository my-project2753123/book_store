import { Box, Button, Card, CardContent, Chip, Divider, Grid, IconButton, Pagination, Paper, Stack, Typography, styled } from "@mui/material";
import Carousel from "react-multi-carousel";
import BannerBook from "./baner.book";
import arrowLeft from 'assets/image/left.png'
import { useCallback, useEffect, useState } from "react";
import { IAuthor, IMeta, IProduct, IPublisher } from "@/types/backend";
import { callFetchAllAuthor, callFetchAllProduct, callFetchAllPublisher } from "@/config/api";
import InfoBooks from "./info.book";
import { BANNER } from "@/config/banner";
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import { isMobile, isTablet } from "react-device-detect";
import { responsive } from 'config/utils';
import DrawerClientFilter from "./drawer.client.filter";
import ModalClientBook from "./modal.client.book";
import ReplayIcon from '@mui/icons-material/Replay';
import { useNavigate } from "react-router-dom";

interface CustomArrowProps {
    onClick?: () => void;
}

const CustomRightArrow: React.FC<CustomArrowProps> = ({ onClick }) => {
    return (
        <button
            onClick={onClick}
            style={{
                position: 'absolute',
                right: 0,
                top: '50%',
                transform: 'translateY(-50%)',
                zIndex: 1,
                backgroundColor: 'transparent',
                border: 'none',
                cursor: 'pointer',
                outline: 'none'
            }}
        >
            <img src={`${arrowLeft}`} width={32} height={56} />
        </button>
    );
};


const CustomLeftArrow: React.FC<CustomArrowProps> = ({ onClick }) => {
    return (
        <button
            onClick={onClick}
            style={{
                position: 'absolute',
                left: 0,
                top: '50%',
                transform: 'translateY(-50%)',
                zIndex: 1,
                backgroundColor: 'transparent',
                border: 'none',
                cursor: 'pointer',
                outline: 'none'
            }}
        >
            <img style={{
                transform: "scaleX(-1)"
            }} src={`${arrowLeft}`} width={32} height={56} />
        </button>
    );
};

const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
    height: 350,
    '&:hover': {
        boxShadow: '0 0 10px rgba(0, 0, 0, 0.2)',
        cursor: "pointer"
    }
}));

interface IProps {
    books: IProduct[],
    meta?: IMeta,
    setCurrent: (v: number) => void,
    handleFetchBooks: () => void,
    current: number
    pageSize: number
    setfilterAll: (v: any) => void
    closeCategory: (v: any) => void
}

const contentBook = (props: IProps) => {

    const { books, meta, setCurrent, handleFetchBooks, current, pageSize, setfilterAll, closeCategory } = props;
    const [author, setauthor] = useState<IAuthor[]>([]);
    const [publisher, setpublisher] = useState<IPublisher[]>([]);
    const [open, setopen] = useState<boolean>(false);
    const [openModal, setOpenModal] = useState<boolean>(false);
    const navigate = useNavigate();

    const handleFetchAuthor = useCallback(async () => {
        const res = await callFetchAllAuthor("page=1&size=100");
        if (res?.data?.result) {
            setauthor(res?.data?.result);
        }
    }, [author])

    const handleFetchPublisher = useCallback(async () => {
        const res = await callFetchAllPublisher("page=1&size=100");
        if (res?.data?.result) {
            setpublisher(res?.data?.result);
        }
    }, [publisher])

    useEffect(() => {
        handleFetchAuthor();
        handleFetchPublisher()
    }, [])

    useEffect(() => {
        handleFetchBooks();
    }, [current, pageSize])

    const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setCurrent(value);
    };

    return (
        <div>
            <Card
                sx={{
                    mt: isMobile || isTablet ? 5 : 0,
                    mb: 2,
                    display: 'flex',
                    alignItems: "center",
                    justifyContent: "center"
                }}
            >
                <CardContent>
                    <Typography variant="inherit" fontSize={35} fontWeight={600}>
                        Nhà sách TiKi
                    </Typography>
                </CardContent>
            </Card>
            <Carousel
                responsive={responsive}
                customLeftArrow={<CustomLeftArrow />}
                customRightArrow={<CustomRightArrow />}
                infinite
                keyBoardControl
                autoPlay={true}
                shouldResetAutoplay={true}
            >
                {
                    BANNER.map((item, index) => {
                        return (
                            <Box key={`key-banner-${index}`}>
                                <BannerBook
                                    text1={item?.text1}
                                    text2={item?.text2}
                                    color={item?.color}
                                    img={item?.img}
                                />
                            </Box>
                        )
                    })
                }
            </Carousel>
            <Card
                sx={{
                    mt: 2,
                }}
            >
                <CardContent>
                    <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                        <Typography variant="inherit" fontSize={20} fontWeight={600}>
                            Tất cả sản phẩm
                        </Typography>
                        {
                            isMobile || isTablet ?
                                <IconButton onClick={() => closeCategory("")}>
                                    <ReplayIcon />
                                </IconButton>
                                :
                                <></>
                        }

                    </Box>

                    <Box
                        sx={{
                            mt: 1,
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            wordBreak: "break-all",
                            pb: 2,
                            overflowX: "auto"
                        }}
                    >
                        <Box>
                            <Typography variant="caption" fontSize={14} color="rgb(128, 128, 137)">
                                Tác giả
                            </Typography>
                            <Box sx={{ mt: 1 }}>
                                <Stack direction="row" useFlexGap flexWrap="wrap" spacing={1} >
                                    {
                                        author?.length > 0 &&
                                        author?.slice(0, 3).map((item, index) => {
                                            return (
                                                <Box
                                                    key={`author-chip-index-${index}`}
                                                    sx={{
                                                        p: 1,
                                                        borderRadius: 5,
                                                        backgroundColor: "#e0e0e0",
                                                        '&:hover': {
                                                            cursor: "pointer",
                                                            backgroundColor: "#4fc3f7",
                                                            color: "white"
                                                        }
                                                    }}
                                                    onClick={() => setfilterAll({ authors: [item.name] })}
                                                >
                                                    {item?.name}
                                                </Box>
                                            )
                                        })
                                    }

                                </Stack>
                            </Box>
                        </Box>
                        <Divider sx={{ height: 40, m: 0.5 }} orientation="vertical" />
                        <Box>
                            <Typography variant="caption" fontSize={14} color="rgb(128, 128, 137)">
                                Nhà cung cấp
                            </Typography>
                            <Box sx={{ mt: 1 }}>
                                <Stack direction="row" useFlexGap flexWrap="wrap" spacing={1}>
                                    {
                                        publisher?.length > 0 &&
                                        publisher?.slice(0, 3).map((item, index) => {
                                            return (
                                                <Box
                                                    key={`publisher-chip-index-${index}`}
                                                    sx={{
                                                        p: 1,
                                                        borderRadius: 5,
                                                        backgroundColor: "#e0e0e0",
                                                        '&:hover': {
                                                            cursor: "pointer",
                                                            backgroundColor: "#4fc3f7",
                                                            color: "white"
                                                        }
                                                    }}
                                                    onClick={() => setfilterAll({ publishers: [item.name] })}
                                                >
                                                    {item?.name}
                                                </Box>
                                            )
                                        })
                                    }
                                </Stack>
                            </Box>
                        </Box>
                        <Divider sx={{ height: 28, m: 0.5 }} orientation="vertical" />
                        <Box>
                            <IconButton
                                onClick={() => isMobile || isTablet ? setopen(true) : setOpenModal(true)}
                            >
                                <FilterAltIcon sx={{ color: "rgb(128, 128, 137)" }} />
                            </IconButton>
                        </Box>
                    </Box>
                </CardContent>
            </Card>
            <Box sx={{ mt: 1 }}>
                <Grid
                    container
                    spacing={{ xs: 1, md: 2 }} columns={{ xs: 4, sm: 12, md: 12, lg: 12 }}
                >
                    {
                        books?.length > 0
                        && books?.map((item, index) => {
                            return (
                                <Grid item xs={2} sm={4} md={4} lg={3} key={`key-books-${index}`}>
                                    <Item
                                        onClick={() => navigate(`/book/${item?.id}`)}
                                    >
                                        <InfoBooks
                                            item={item}
                                        />
                                    </Item>
                                </Grid>
                            )
                        })
                    }
                </Grid>
            </Box>
            <Box sx={{
                mt: 2, mb: 2, ml: "auto", width: "100%",
                display: 'flex', justifyContent: "end"
            }}>
                <Stack spacing={2}>
                    <Pagination count={meta?.pages} variant="outlined" shape="rounded"
                        onChange={handleChange}
                    />
                </Stack>
            </Box>
            <DrawerClientFilter
                open={open}
                setopen={setopen}
                setfilterAll={setfilterAll}
            />
            <ModalClientBook
                openModal={openModal}
                setOpenModal={setOpenModal}
                setfilterAll={setfilterAll}
            />
        </div>
    );
};

export default contentBook;