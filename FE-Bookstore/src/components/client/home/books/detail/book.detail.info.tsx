import { IProduct } from '@/types/backend';
import { Box, Button, Card, Grid, IconButton, Typography } from '@mui/material';
import React, { useEffect, useMemo, useState } from 'react';
import parse from 'html-react-parser';
import Carousel from 'react-multi-carousel';
import { CarouselCategory, responsive } from '@/config/utils';
import InfoBookCategory from '@/pages/client/book/info.book.category';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import { isMobile, isTablet } from 'react-device-detect';
import DrawProductQuantity from '@/components/admin/product/mobile/draw.product.quantity';

interface IProps {
    book: IProduct | null,
    bookByCategory: IProduct[]
}

interface CustomArrowProps {
    onClick?: () => void;
}

const CustomRightArrow: React.FC<CustomArrowProps> = ({ onClick }) => {
    return (
        <IconButton
            onClick={onClick}
            style={{
                position: 'absolute',
                right: 0,
                top: '50%',
                transform: 'translateY(-50%)',
                zIndex: 1,
                backgroundColor: 'rgb(255, 255, 255)',
                border: 'none',
                cursor: 'pointer',
                outline: 'none',
                boxShadow: "rgba(0, 0, 0, 0.2) 0px 2px 8px"
            }}
            color='info'
        >
            <ChevronRightIcon />
        </IconButton>
    );
};


const CustomLeftArrow: React.FC<CustomArrowProps> = ({ onClick }) => {
    return (
        <IconButton
            onClick={onClick}
            style={{
                position: 'absolute',
                left: 0,
                top: '50%',
                transform: 'translateY(-50%)',
                zIndex: 1,
                backgroundColor: 'rgb(255, 255, 255)',
                border: 'none',
                cursor: 'pointer',
                outline: 'none',
                boxShadow: "rgba(0, 0, 0, 0.2) 0px 2px 8px"
            }}
            color='info'
        >
            <ChevronLeftIcon />
        </IconButton>
    );
};

const bookDetailInfo = (props: IProps) => {

    const { book, bookByCategory } = props;

    const [show, setshow] = useState<boolean>(false);
    const [open, setopen] = useState<boolean>(false);

    useEffect(() => {
        setshow(false);
    }, [])

    useEffect(() => {
        setshow(false);
    }, [book])

    const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }

    return (
        <Box sx={{ overflowY: "auto" }}>
            <Card sx={{ mb: isMobile || isTablet ? 1 : 2, p: 2 }}>
                <Box>
                    <Typography variant="body1" sx={{ display: 'flex', alignItems: "center" }}>
                        Tác giả:&nbsp;
                        <span style={{ color: "rgb(13, 92, 182)", fontSize: "15px" }}>{book?.author?.name}</span>
                    </Typography>
                    <Typography variant="h6" fontWeight={600}>
                        {book?.name}
                    </Typography>
                    <Typography variant="body1" color="#ff424e" fontSize="24px" fontWeight={600}>
                        {new Intl.NumberFormat('vi-VN', config).format(
                            +(book?.prices.find(items => items.active === true)?.price.toString() as string)
                        )}
                    </Typography>
                </Box>
            </Card>

            {
                isMobile || isTablet ?
                    <Card sx={{ position: "sticky", top: 0, p: 2, mb: 1 }}>
                        <Box sx={{ display: 'flex', flexDirection: 'column', justifyContent: "space-between", height: 80 }}>
                            <Button variant="contained" color="error" >Mua ngay</Button>
                            <Button variant="outlined"
                                onClick={() => setopen(true)}
                            >
                                Thêm vào giỏ hàng
                            </Button>
                        </Box>
                    </Card>
                    :
                    <></>
            }

            <Card sx={{ mb: 2, p: 2 }}>
                <Typography variant="caption" fontSize={18} fontWeight={600} mb={1}>
                    Sản phẩm tương tự
                </Typography>
                {
                    useMemo(() => (
                        <Carousel
                            responsive={CarouselCategory}
                            customLeftArrow={<CustomLeftArrow />}
                            customRightArrow={<CustomRightArrow />}
                            infinite
                            keyBoardControl
                        >

                            {
                                bookByCategory?.length > 0
                                && bookByCategory?.map((item, index) => {
                                    return (
                                        <InfoBookCategory
                                            key={`info-book-category-index-${index}`}
                                            item={item}
                                        />

                                    )
                                })
                            }

                        </Carousel>
                    ), [bookByCategory])
                }
            </Card>
            <Card sx={{ mb: 2, p: 2 }}>
                {
                    useMemo(() => (
                        <Box sx={{ display: "flex", flexDirection: "column" }}>
                            <Typography color="rgb(39, 39, 42)" fontWeight={600} fontSize={17} mb={1}>
                                Mô tả sản phẩm
                            </Typography>
                            <Box sx={{ height: !show ? 200 : "auto", overflow: 'hidden' }}>
                                <span
                                    style={{
                                        fontSize: 16,
                                        lineHeight: 1
                                    }}
                                >
                                    {parse(book ? book.description : "")}
                                </span>
                            </Box>
                            <Box sx={{ width: "100%" }}>
                                <Typography textAlign="center" p={1} color="rgb(24, 158, 255)"
                                    sx={{
                                        '&:hover': {
                                            cursor: "pointer"
                                        }
                                    }}
                                    onClick={() => setshow(!show)}
                                >
                                    {!show ? <>Xem thêm</> : <>Thu gọn</>}
                                </Typography>
                            </Box>
                        </Box>
                    ), [book, show])
                }
            </Card>
            <DrawProductQuantity
                open={open}
                setopen={setopen}
                book={book}
            />
        </Box>
    );
};

export default bookDetailInfo;