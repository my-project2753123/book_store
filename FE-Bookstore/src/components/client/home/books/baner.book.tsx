import { Box, Typography } from '@mui/material';

interface IProps {
    text1: string,
    text2: string,
    color: string,
    img: string
}

const bannerBook = (props: IProps) => {

    const { text1, text2, color, img } = props;

    return (
        <Box
            sx={{
                height: {
                    xs: 150, md: 230, sm: 230
                },
                width: "100%",
                display: "flex",
            }}
        >
            <Box
                sx={{
                    width: "40%",
                    height: "100%",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    borderRadius: 2,
                    backgroundColor: color
                }}
            >
                <Box
                    sx={{
                        width: "60%",
                        height: "60%",
                        backgroundImage: `url(${img})`,
                        backgroundSize: 'cover',
                        backgroundRepeat: 'no-repeat',
                        backgroundPosition: 'center',
                        borderRadius: 2,
                        border: "1px solid white"
                    }}
                >
                </Box>
            </Box>
            <Box
                sx={{
                    width: "60%",
                    height: "100%",
                    backgroundColor: "rgb(255 255 255)",
                    borderRadius: 2,
                    display: "flex",
                    justifyContent: "center",
                    flexDirection: "column",
                    pl: 2
                }}
            >
                <Typography
                    sx={{
                        fontSize: 20,
                        fontWeight: 600
                    }
                    }
                    color="rgb(56, 56, 61)">
                    {text1}
                </Typography>
                <Typography
                    sx={{
                        fontSize: 16,
                        fontWeight: 500
                    }
                    }
                    color="rgb(128, 128, 137)"
                >
                    {text2}
                </Typography>
            </Box>
        </Box>
    );
};

export default bannerBook;