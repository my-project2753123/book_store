import { styled } from '@mui/material/styles';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion, { AccordionProps } from '@mui/material/Accordion';
import MuiAccordionSummary, {
    AccordionSummaryProps,
} from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import { useCallback, useEffect, useState } from 'react';
import { Box } from '@mui/material';
import { ICategory } from '@/types/backend';
import { callFetchAllCategories } from '@/config/api';

const Accordion = styled((props: AccordionProps) => (
    <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: 6,
    '&:not(:last-child)': {
        borderBottom: 0,
    },
    '&::before': {
        display: 'none',
    },
}));

const AccordionSummary = styled((props: AccordionSummaryProps) => (
    <MuiAccordionSummary
        expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
        {...props}
    />
))(({ theme }) => ({
    backgroundColor:
        theme.palette.mode === 'light'
            ? 'rgba(255, 255, 255, .05)'
            : 'rgba(0, 0, 0, .03)',
    flexDirection: 'row-reverse',
    '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
        transform: 'rotate(90deg)',
    },
    '& .MuiAccordionSummary-content': {
        marginLeft: theme.spacing(1),
    },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
    padding: theme.spacing(2),
    borderTop: '1px solid rgba(0, 0, 0, .125)',
}));

interface IProps {
    filterCategory: (v: string) => void
    closeCategory: (v: any) => void

}

const categoryBooks = (props: IProps) => {

    const { filterCategory, closeCategory } = props;
    const [expanded, setExpanded] = useState<string | false>('panel1');
    const [categories, setcategories] = useState<ICategory[]>([]);


    const handleChange =
        (panel: string) => (event: React.SyntheticEvent, newExpanded: boolean) => {
            setExpanded(newExpanded ? panel : false);
        };

    const handleFetchCategory = useCallback(async () => {
        const res = await callFetchAllCategories();
        if (+res?.statusCode === 200) {
            setcategories(res?.data as ICategory[])
        }
    }, [categories])

    useEffect(() => {
        handleFetchCategory()
    }, [])

    return (
        <div>
            <Accordion>
                <Box
                    sx={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        padding: 2,
                    }}
                >
                    <Typography variant='inherit' fontSize={18} fontWeight={600}
                        sx={{
                            '&:hover': {
                                cursor: "pointer",
                                textDecoration: "underline",
                            }
                        }}
                        onClick={() => { closeCategory("") }}
                    >
                        Khám phá danh mục
                    </Typography>
                </Box>
            </Accordion>
            {
                categories?.length > 0
                && categories?.map((item, index) => {
                    return (
                        <Accordion key={`key-index-accordion-${index}`} expanded={expanded === `panel-${index}`} onChange={handleChange(`panel-${index}`)}>
                            <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                                <Typography variant='inherit' fontSize={17} fontWeight={600}>
                                    {item?.category}
                                </Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                {
                                    item?.genres?.length > 0 &&
                                    item?.genres?.map((item, index) => {
                                        return (
                                            <Typography key={`key-index-genre-cate-${index}`}
                                                variant='inherit'
                                                sx={{
                                                    m: 1,
                                                    '&:hover': {
                                                        cursor: "pointer",
                                                        textDecoration: "underline",
                                                        color: "blue"
                                                    }
                                                }}
                                                onClick={() => filterCategory(item)}
                                            >
                                                {item}
                                            </Typography>
                                        )
                                    })
                                }

                            </AccordionDetails>
                        </Accordion>
                    )
                })
            }
        </div>
    );
};

export default categoryBooks;