import { IInfoCart, IOrderAD, IOrderDetailAD } from "@/types/backend";
import { DrawerForm } from "@ant-design/pro-components";
import { Col, Divider, Form, Image, Row } from "antd";
import { useEffect, useState } from "react";
import { callFetchOrderDetailByOrder } from "@/config/api";
import { PayPalScriptProvider } from "@paypal/react-paypal-js";
import ButtonPayPal from '@/components/client/home/paypal/button.account';

interface IProps {
    onClose: (v: boolean) => void;
    open: boolean;
    dataInit: IOrderAD | null;
    setDataInit: (v: IOrderAD | null) => void;
}

const drawOrderClient = (props: IProps) => {
    const { onClose, open, dataInit, setDataInit } = props;
    const [form] = Form.useForm();
    const [tabRef, settabRef] = useState<IOrderDetailAD[]>([]);
    const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }
    const [arr, setarr] = useState<IInfoCart[]>([]);

    useEffect(() => {
        if (dataInit?.id) {
            fetchOrderDetailByOrder();

            const arrOrder = dataInit?.details.map(item => {
                return {
                    id: item?.id,
                    price: item.price.toString(),
                    total: item.amount.toString(),
                    quantity: item.quantity.toString()
                } as IInfoCart
            })
            setarr(arrOrder)
        }
    }, [dataInit])

    const fetchOrderDetailByOrder = async () => {
        const res = await callFetchOrderDetailByOrder(dataInit?.id as string);
        if (res?.data) {
            settabRef(res?.data)
        }
    }

    const handleOnclose = () => {
        form.resetFields();
        setDataInit(null);
        onClose(false);
    }

    return (
        <DrawerForm
            title="Thông tin order"
            open={open}
            submitter={false}
            drawerProps={{
                destroyOnClose: true,
                onClose: () => handleOnclose(),
                footer: null,
                keyboard: false,
                maskClosable: false,
            }}
            form={form}
            autoFocusFirstInput
            initialValues={dataInit?.id ? dataInit : {}}
            preserve={false}
        >
            <Row gutter={10}>
                <Col span={15}>
                    <span
                        style={{
                            fontSize: 16,
                            fontWeight: 600
                        }}
                    >
                        Thông tin sản phẩm
                    </span>
                </Col>
                <Col span={4}>
                    <span
                        style={{
                            fontSize: 16,
                            fontWeight: 600
                        }}
                    >
                        Số lượng
                    </span>
                </Col>
                <Col span={5}>
                    <span
                        style={{
                            fontSize: 16,
                            fontWeight: 600
                        }}
                    >
                        Thành tiền
                    </span>
                </Col>
            </Row>
            {
                tabRef?.length > 0 &&
                tabRef?.filter(item => item.active === true)?.map((item, index) => {
                    return (
                        <Row gutter={10} key={`index-sp-checked-${index}`}>
                            <Col span={15}>

                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                    <Image
                                        width={70}
                                        src={`${import.meta.env.VITE_BACKEND_URL_UPLOAD}/storage/product/${item?.book?.image}`}
                                    />
                                    <div style={{
                                        display: "flex", flexDirection: "column"
                                    }}>
                                        {item?.book?.name}
                                    </div>
                                </div>
                            </Col>
                            <Col span={4}>
                                <div style={{ height: "90px", display: 'flex', alignItems: 'center' }}>
                                    {+item?.quantity}
                                </div>
                            </Col>
                            <Col span={5}>
                                <div style={{ height: "90px", display: 'flex', alignItems: 'center' }}>
                                    <span
                                        style={{
                                            fontSize: 17,
                                            fontWeight: 600,
                                            color: "#ff7043"
                                        }}
                                    >
                                        {
                                            new Intl.NumberFormat('vi-VN', config).format(+(item?.amount))
                                        }
                                    </span>
                                </div>
                            </Col>
                        </Row>
                    )
                })
            }
            <Row gutter={10}>
                <Col span={24}>
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                        <span>Tạm tính</span>
                        <span>
                            {
                                new Intl.NumberFormat('vi-VN', config).format(
                                    dataInit?.totalAmount ?? 0
                                )
                            }
                        </span>
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                        <span>Giảm giá</span>
                        <span>
                            {
                                new Intl.NumberFormat('vi-VN', config).format(
                                    dataInit?.discount ?? 0
                                )
                            }
                        </span>
                    </div>
                    <Divider />
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                        <span>Tổng tiền</span>
                        <span>
                            <span
                                style={{
                                    fontSize: 17,
                                    fontWeight: 600,
                                    color: "#ff7043"
                                }}
                            >
                                {
                                    new Intl.NumberFormat('vi-VN', config).format(
                                        dataInit?.totalAmountPlayable ?? 0
                                    )
                                }
                            </span>
                        </span>
                    </div>
                </Col>
                <Col span={24}>
                    {
                        dataInit?.id && dataInit?.status === "UNPAID" && dataInit?.active === true
                            ?
                            <>
                                <PayPalScriptProvider options={{ clientId: `${import.meta.env.VITE_CLIENT_ID}` }}>
                                    <ButtonPayPal
                                        sum={dataInit?.totalAmountPlayable ?? 0}
                                        dataInit={dataInit ?? {}}
                                    />
                                </PayPalScriptProvider>

                            </>
                            :
                            <></>
                    }
                </Col>
            </Row>
        </DrawerForm>
    );
};

export default drawOrderClient;