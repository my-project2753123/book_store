import AppBar from '@mui/material/AppBar';
import { Box } from '@mui/material';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { Avatar, Badge, BadgeProps, Button, Divider, InputBase, Paper, styled } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import DrawerClient from './drawer.client';
import React, { useState } from 'react';
import { isMobile, isTablet } from 'react-device-detect';
import logotiki from 'assets/image/logotiki.png';
import { useNavigate } from 'react-router-dom';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import nProgress from 'nprogress';
import { useAppSelector } from '@/redux/hook';
import MenuNavbar from './menu.navbar';


const StyledBadge = styled(Badge)<BadgeProps>(({ theme }) => ({
    '& .MuiBadge-badge': {
        right: -3,
        top: 13,
        border: `2px solid ${theme.palette.background.paper}`,
        padding: '0 4px',
    },
}));

const HEADER_HEIGHT = 20;

interface IProps {
    setsearch: (v: string) => void
}

const navBarClient = (props: IProps) => {

    const { setsearch } = props;

    const [open, setopen] = useState<boolean>(false);
    const navigate = useNavigate();
    const userRedux = useAppSelector((state: any) => state.account)
    const [searchInput, setSearchInput] = useState<string>("");
    const cartRedux = useAppSelector((state: any) => state.cart.cart);

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const openDesktop = Boolean(anchorEl);
    const handleClick = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleChangeSearch = (e: any) => {
        setSearchInput(e.target.value)
    }

    const handleKeyDown = (event: any) => {
        if (event.key === "Enter") {
            console.log(window.location.pathname);

            if (window.location.pathname === "/") {
                event.preventDefault()
                setsearch(searchInput)
            }
            event.preventDefault()
        }
    };

    return (
        <Box sx={{
            flexGrow: 1, marginTop: isMobile || isTablet ? `${HEADER_HEIGHT}px` : '0',
        }}>
            <AppBar position="static" sx={{
                backgroundColor: isMobile || isTablet ? "rgb(27, 168, 255)" : "rgb(255, 255, 255)",
                position: isMobile || isTablet ? "fixed" : "inherit",
                zIndex: 100,
                top: 0,
                boxShadow: "none",
            }}>
                <Toolbar
                    sx={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "space-around"
                    }}
                >
                    {
                        isMobile || isTablet ?
                            <>
                                <IconButton
                                    size="large"
                                    edge="start"
                                    color="inherit"
                                    aria-label="menu"
                                    onClick={() => {
                                        nProgress.start();
                                        navigate(-1)
                                        nProgress.done();
                                    }}
                                >
                                    <ArrowBackIosIcon />
                                </IconButton>
                                <IconButton
                                    size="large"
                                    edge="start"
                                    color="inherit"
                                    aria-label="menu"
                                    sx={{ mr: 1 }}
                                    onClick={() => setopen(true)}
                                >
                                    <MenuIcon />
                                </IconButton>
                            </>

                            :
                            <img src={`${logotiki}`} alt="Paris" width="96" height="40"
                                style={{
                                    cursor: 'pointer',
                                }}
                                onClick={() => {
                                    nProgress.start();
                                    navigate('/')
                                    nProgress.done();
                                }}
                            />
                    }
                    <Paper
                        component="form"
                        sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: isMobile || isTablet ? "100%" : "70%", ml: 1 }}
                    >
                        <SearchIcon />
                        <InputBase
                            sx={{ ml: 1, flex: 1 }}
                            placeholder="Bạn đang tìm kiếm gì?"
                            onChange={(e) => handleChangeSearch(e)}
                            onKeyDown={handleKeyDown}
                        />
                    </Paper>
                    {
                        isMobile || isTablet
                            ?
                            <IconButton
                                size="large"
                                edge="start"
                                color="inherit"
                                aria-label="menu"
                                sx={{ ml: 2 }}

                                onClick={() => navigate("/cart")}
                            >
                                <StyledBadge badgeContent={cartRedux?.length ?? 0} color="error">
                                    <ShoppingCartIcon />
                                </StyledBadge>
                            </IconButton>
                            :
                            <Box
                                sx={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "space-between"
                                }}
                            >
                                {
                                    userRedux?.isAuthenticated
                                        ?
                                        <>
                                            <span
                                                style={{ color: "black", fontSize: "17px", fontWeight: 500 }}
                                            >
                                                {userRedux?.user?.email}
                                            </span>
                                            <IconButton
                                                onClick={handleClick}
                                                size="small"
                                                sx={{ ml: 2 }}
                                                aria-controls={openDesktop ? 'account-menu' : undefined}
                                                aria-haspopup="true"
                                                aria-expanded={openDesktop ? 'true' : undefined}
                                            >
                                                <Avatar sx={{ width: 32, height: 32 }}>{userRedux?.user?.email.slice(0, 2).toUpperCase()}</Avatar>
                                            </IconButton>
                                        </>

                                        :
                                        <Button variant="outlined"
                                            onClick={() => {
                                                nProgress.start();
                                                navigate('/login')
                                                nProgress.done();
                                            }}
                                        >Đăng nhập
                                        </Button>
                                }


                                <Divider sx={{ height: 28, m: 0.5 }} orientation="vertical" />
                                <IconButton
                                    onClick={() => navigate("/cart")}
                                >
                                    <StyledBadge badgeContent={cartRedux?.length ?? 0} color="error">
                                        <ShoppingCartIcon
                                            sx={{
                                                color: "rgb(11, 116, 229)",
                                            }
                                            } />
                                    </StyledBadge>
                                </IconButton>

                            </Box>
                    }
                </Toolbar>
            </AppBar>
            <DrawerClient
                open={open}
                setopen={setopen}
            />
            <MenuNavbar
                anchorEl={anchorEl}
                handleClose={handleClose}
                open={openDesktop}
            />
        </Box>
    );
};

export default navBarClient;