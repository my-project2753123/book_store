import { callLogout } from "@/config/api";
import { useAppDispatch, useAppSelector } from "@/redux/hook";
import { setLogoutAction } from "@/redux/slide/authSlide";
import { Logout, PersonAdd, Settings } from "@mui/icons-material";
import { Avatar, Divider, ListItemIcon, Menu, MenuItem } from "@mui/material";
import { message } from "antd";
import _ from "lodash";
import { useNavigate } from "react-router-dom";

interface IProps {
    handleClose: () => void;
    anchorEl: HTMLElement | null;
    open: boolean;
}

const menuNavbar = (props: IProps) => {

    const { handleClose, anchorEl, open } = props;
    const dispatch = useAppDispatch();
    const user = useAppSelector((state) => state?.account?.user);
    const navigate = useNavigate();

    const handleLogout = async () => {
        const res = await callLogout();
        if (+res?.statusCode === 200) {
            dispatch(setLogoutAction())
            message.success("Đăng xuất thành công!");
            handleClose()
        }
    }

    const handlePageAdmin = () => {
        navigate("/admin")
        handleClose()
    }

    const handlePageAccount = () => {
        navigate("/account")
        handleClose()
    }


    return (
        <Menu
            anchorEl={anchorEl}
            id="account-menu"
            open={open}
            onClose={handleClose}
            onClick={handleClose}
            PaperProps={{
                elevation: 0,
                sx: {
                    overflow: 'visible',
                    filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                    mt: 1.5,
                    '& .MuiAvatar-root': {
                        width: 32,
                        height: 32,
                        ml: -0.5,
                        mr: 1,
                    },
                    '&::before': {
                        content: '""',
                        display: 'block',
                        position: 'absolute',
                        top: 0,
                        right: 14,
                        width: 10,
                        height: 10,
                        bgcolor: 'background.paper',
                        transform: 'translateY(-50%) rotate(45deg)',
                        zIndex: 0,
                    },
                },
            }}
            transformOrigin={{ horizontal: 'right', vertical: 'top' }}
            anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
        >
            <MenuItem onClick={handlePageAccount}>
                Thông tin tài khoản
            </MenuItem>
            <Divider />
            {
                user?.role?.name !== "CUSTOMER"
                    ?
                    <MenuItem onClick={handlePageAdmin}>
                        <ListItemIcon>
                            <Settings fontSize="small" />
                        </ListItemIcon>
                        Trang quản lý
                    </MenuItem>
                    :
                    <></>
            }
            <MenuItem onClick={handleLogout}>
                <ListItemIcon>
                    <Logout fontSize="small" />
                </ListItemIcon>
                Đăng xuất
            </MenuItem>
        </Menu>
    );
};

export default menuNavbar;