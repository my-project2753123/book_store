import { Box, Drawer, IconButton, ListItem, ListItemButton, ListItemIcon, ListItemText, Avatar, Typography, styled } from '@mui/material';
import { Divider, List, message } from 'antd';
import HomeIcon from '@mui/icons-material/Home';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import { Settings } from "@mui/icons-material";
import { useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '@/redux/hook';
import LogoutIcon from '@mui/icons-material/Logout';
import { callLogout } from '@/config/api';
import { setLogoutAction } from '@/redux/slide/authSlide';

const drawerWidth = 240;

interface IProps {
    open: boolean;
    setopen: (v: boolean) => void;
}


const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    backgroundColor: "rgb(27, 168, 255)",
    justifyContent: 'flex-end',
}));


const drawerClient = (props: IProps) => {

    const { open, setopen } = props;
    const navigate = useNavigate();
    const userRedux = useAppSelector((state: any) => state.account)
    const dispatch = useAppDispatch();

    const handleLogin = () => {
        if (userRedux?.isAuthenticated) {
            navigate("/account")
            setopen(false)

        } else {
            navigate("/login")
            setopen(false)
        }

    }

    const handleBackHome = () => {
        navigate("/")
        setopen(false)
    }

    const handleLogout = async () => {
        const res = await callLogout();
        if (+res?.statusCode === 200) {
            dispatch(setLogoutAction())
            message.success("Đăng xuất thành công!");
            setopen(false)
        }
    }

    const shouldHideMenu = (text: string) => {
        return text === 'Đăng xuất';
    };

    return (
        <Box
            component="nav"
            sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
            aria-label="mailbox folders"
        >
            <Drawer
                variant="temporary"
                open={open}
                onClose={() => { setopen(false) }}
                sx={{
                    display: { sm: 'block', md: 'none' },
                    '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                    '& .MuiPaper-root': {
                        height: '100vh',
                    },
                }}

            >
                <DrawerHeader>
                    <Box sx={{ width: "100%", display: "flex", alignItems: "center", justifyContent: "space-around" }}>
                        {
                            userRedux?.isAuthenticated
                                ?
                                <Box>
                                    <IconButton
                                        // onClick={handleClick}
                                        size="small"
                                        sx={{ ml: 2 }}
                                        aria-haspopup="true"
                                    >
                                        <Avatar sx={{ width: 32, height: 32 }}>{userRedux?.user?.email.slice(0, 2).toUpperCase()}</Avatar>
                                    </IconButton>
                                    <span
                                        style={{ color: "black", fontSize: "17px", fontWeight: 500 }}
                                    >
                                        {userRedux?.user?.email}
                                    </span>
                                </Box>
                                :
                                <>
                                    <AccountCircleIcon fontSize='large' sx={{ color: "white" }} />
                                    <Box
                                        onClick={() => navigate("/login")}
                                    >
                                        <Typography sx={{ color: "white" }} variant='h6'>
                                            Đăng nhập
                                        </Typography>
                                        <Typography sx={{ color: "white" }} variant='caption'>
                                            Nhận nhiều ưu đãi hơn
                                        </Typography>
                                    </Box>
                                </>

                        }
                        <IconButton>
                            <ChevronRightIcon sx={{ color: "white" }} />
                        </IconButton>
                    </Box>
                </DrawerHeader>
                <Box sx={{ m: 1 }}>
                    <List>
                        <ListItem disablePadding>
                            <ListItemButton onClick={() => handleBackHome()}>
                                <ListItemIcon>
                                    <HomeIcon />
                                </ListItemIcon>
                                <ListItemText primary="Trang chủ" />
                            </ListItemButton>
                        </ListItem>
                        <ListItem disablePadding>
                            <ListItemButton onClick={() => handleLogin()}>
                                <ListItemIcon>
                                    <AccountCircleIcon />
                                </ListItemIcon>
                                <ListItemText primary="Tài khoản" />
                            </ListItemButton>
                        </ListItem>
                        {
                            userRedux?.isAuthenticated && userRedux?.user?.role?.name !== "CUSTOMER"
                                ?
                                <ListItem disablePadding>
                                    <ListItemButton onClick={() => navigate("/admin")}>
                                        <ListItemIcon>
                                            <Settings />
                                        </ListItemIcon>
                                        <ListItemText primary="Trang quản lý" />
                                    </ListItemButton>
                                </ListItem>
                                : <></>
                        }

                        {
                            userRedux?.isAuthenticated
                                ?
                                <ListItem disablePadding>
                                    <ListItemButton onClick={() => handleLogout()}>
                                        <ListItemIcon>
                                            <LogoutIcon />
                                        </ListItemIcon>
                                        <ListItemText primary="Đăng xuất" />
                                    </ListItemButton>
                                </ListItem>
                                : <></>
                        }

                    </List>
                </Box>
            </Drawer>
        </Box>
    );
};

export default drawerClient;