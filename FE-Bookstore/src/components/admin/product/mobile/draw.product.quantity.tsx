import { Global } from '@emotion/react';
import { styled } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { grey } from '@mui/material/colors';
import { Box } from '@mui/material';
import Typography from '@mui/material/Typography';
import SwipeableDrawer from '@mui/material/SwipeableDrawer';
import { Button, Divider, IconButton, InputBase } from '@mui/material';
import CancelIcon from '@mui/icons-material/Cancel';
import { IProduct } from '@/types/backend';
import RemoveIcon from '@mui/icons-material/Remove';
import AddIcon from '@mui/icons-material/Add';
import { useState } from 'react';
import { useAppDispatch } from '@/redux/hook';
import { setItemInCart } from '@/redux/slide/cartSlide';
import { message } from 'antd';

const drawerBleeding = 56;

interface IProps {
    window?: () => Window;
    open: boolean;
    setopen: (v: boolean) => void;
    book: IProduct | null
}

const Root = styled('div')(({ theme }) => ({
    height: '100%',
    backgroundColor:
        theme.palette.mode === 'light' ? grey[100] : theme.palette.background.default,
}));

const StyledBox = styled('div')(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'light' ? '#fff' : grey[800],
}));

const Puller = styled('div')(({ theme }) => ({
    width: 30,
    height: 6,
    backgroundColor: theme.palette.mode === 'light' ? grey[300] : grey[900],
    borderRadius: 3,
    position: 'absolute',
    top: 8,
    left: 'calc(50% - 15px)',
}));

const drawProductQuantity = (props: IProps) => {

    const { window, open, setopen, book } = props;

    const container = window !== undefined ? () => window().document.body : undefined;
    const dispatch = useAppDispatch()

    const toggleDrawer = (newOpen: boolean) => () => {
        setopen(newOpen);
    };
    const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }
    const price = book && book.prices ? book?.prices.find(items => items.active === true)?.price : 0
    const [quantity, setquantity] = useState<number>(1);

    const handleAddCart = () => {
        const arrLocal = localStorage?.getItem("arrProduct")
        if (arrLocal) {
            const arrCart = [...JSON.parse(arrLocal)];
            const objIndex = arrCart.findIndex(item => item.id === book?.id);
            if (objIndex !== -1) {
                arrCart[objIndex].quantity += quantity;
                arrCart[objIndex].price += price ? (price as number) * quantity : 0;
                localStorage.setItem("arrProduct", JSON.stringify(arrCart));
                dispatch(setItemInCart(arrCart))
                message.success("đã thêm sản phẩm vào giỏ")
                setopen(false)
            } else {
                const product = {
                    id: book?.id,
                    img: book?.image,
                    name: book?.name,
                    total: price ? (price as number) * quantity : 0,
                    quantity: quantity,
                    price: price
                }
                arrCart.push(product);
                localStorage.setItem("arrProduct", JSON.stringify(arrCart));
                dispatch(setItemInCart(arrCart))
                message.success("đã thêm sản phẩm vào giỏ")
                setopen(false)
            }
        }
        else {
            const arrCart = [];
            const product = {
                id: book?.id,
                img: book?.image,
                name: book?.name,
                total: price ? (price as number) * quantity : 0,
                quantity: quantity,
                price: price
            }

            arrCart.push(product)

            localStorage.setItem("arrProduct", JSON.stringify(arrCart));
            dispatch(setItemInCart(arrCart))
            message.success("đã thêm sản phẩm vào giỏ")
            setopen(false)
        }

    }

    return (
        <Root>
            <CssBaseline />
            <Global
                styles={{
                    '.MuiDrawer-root > .MuiPaper-root': {
                        height: `calc(50% - ${drawerBleeding}px)`,
                        overflow: 'visible',
                    },
                }}
            />
            <SwipeableDrawer
                container={container}
                anchor="bottom"
                open={open}
                onClose={toggleDrawer(false)}
                onOpen={toggleDrawer(true)}
                swipeAreaWidth={drawerBleeding}
                disableSwipeToOpen={false}
                ModalProps={{
                    keepMounted: true,
                }}
            >
                <StyledBox
                    sx={{
                        position: 'absolute',
                        top: -drawerBleeding,
                        borderTopLeftRadius: 8,
                        borderTopRightRadius: 8,
                        visibility: open ? 'visible' : 'hidden',
                        right: 0,
                        left: 0,
                    }}
                >
                    <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                        <Typography fontSize={16} fontWeight={600}
                            sx={{ p: 2, color: 'text.secondary' }}>
                            Chọn thuộc tính & số lượng
                        </Typography>
                        <IconButton sx={{ mr: 1 }}
                            onClick={() => setopen(false)}
                        >
                            <CancelIcon />
                        </IconButton>
                    </Box>
                </StyledBox>
                <StyledBox
                    sx={{
                        px: 2,
                        pb: 2,
                        height: '100%',
                        overflow: 'auto',
                    }}
                >
                    <Box sx={{ height: "50%", p: 1, display: 'flex', alignItems: 'center' }}>
                        <Box
                            sx={{
                                width: "30%",
                                height: "100%",
                                backgroundImage: `url(${import.meta.env.VITE_BACKEND_URL_UPLOAD}/storage/product/${book?.image})`,
                                backgroundSize: 'cover',
                                backgroundRepeat: 'no-repeat',
                                backgroundPosition: 'center',
                            }}
                        >
                        </Box>
                        <Box>
                            <Typography sx={{
                                fontSize: 24,
                                fontWeight: 600
                            }} >
                                {new Intl.NumberFormat('vi-VN', config).format(
                                    price ? (price as number) * quantity : 0
                                )}
                            </Typography>
                        </Box>
                    </Box>
                    <Divider />
                    <Box sx={{ height: "35%" }}>
                        <Box>
                            <Typography variant="caption" fontSize={17} fontWeight={600}>
                                Số lượng
                            </Typography>
                            <Box sx={{ display: 'flex', alignItems: 'center' }}>
                                <IconButton
                                    disabled={quantity <= 1 ? true : false}
                                    onClick={() => setquantity((pre) => pre - 1)}
                                >
                                    <RemoveIcon />
                                </IconButton>
                                <InputBase
                                    type="number"
                                    value={quantity}
                                    onChange={(e) => { +e.target.value < 1 ? 1 : setquantity(+e.target.value) }}
                                    sx={{
                                        border: "1px solid rgb(166 166 176)", m: 1, borderRadius: 1, textAlign: 'center',
                                        width: "15%",
                                        '& input': {
                                            textAlign: 'center',
                                        },
                                    }}
                                />
                                <IconButton
                                    onClick={() => setquantity((pre) => pre + 1)}
                                >
                                    <AddIcon />
                                </IconButton>
                            </Box>
                        </Box>
                    </Box>
                    <Divider />
                    <Box sx={{ height: "15%", display: 'flex', alignItems: 'center' }}
                    >
                        <Button variant='contained' sx={{ height: '100%', width: '100%' }}
                            onClick={() => handleAddCart()}
                        >
                            Thêm vào giỏ hàng
                        </Button>
                    </Box>
                </StyledBox>
            </SwipeableDrawer>
        </Root >
    );
};

export default drawProductQuantity;