import { IProduct, IPublisher } from "@/types/backend";
import { DrawerForm, ProCard, ProForm, ProFormDateRangePicker, ProFormDigit, ProFormSelect, ProFormSwitch, ProFormText } from "@ant-design/pro-components";
import { Col, ConfigProvider, Form, Row, Upload, message } from "antd";
import enUS from 'antd/lib/locale/en_US';
import { useEffect, useState } from "react";
import ReactQuill from "react-quill";
import { v4 as uuidv4 } from 'uuid';
import { DebounceSelect } from "../users/debouce.select";
import { callFetchAllAuthor, callFetchAllPublisher } from "@/config/api";

interface IProps {
    onClose: (v: boolean) => void;
    open: boolean;
    dataInit: IProduct | null;
    setDataInit: (v: IProduct | null) => void;
}

export interface ISelect {
    label: string;
    value: string;
    key?: string;
}

const drawProduct = (props: IProps) => {

    const { onClose, open, dataInit, setDataInit } = props;
    const [form] = Form.useForm();
    const [value, setValue] = useState<string>("");
    const [author, setAuthor] = useState<ISelect[]>([]);
    const [publisher, setpublisher] = useState<ISelect[]>([]);
    const [price, setprice] = useState<number>();


    useEffect(() => {
        if (dataInit?.id && dataInit?.description) {
            setValue(dataInit.description);
        }
        if (dataInit?.id && dataInit?.prices) {
            let price = dataInit?.prices.filter(item => item.active === true)
                .map(item => item.price).toString();
            setprice(+price)
        }
        if (dataInit?.id) {
            if (dataInit?.author) {
                setAuthor([
                    {
                        label: dataInit.author?.name,
                        value: dataInit.author?.id,
                        key: dataInit.author?.id,
                    }
                ])
            }
            if (dataInit?.publisher) {
                setpublisher([
                    {
                        label: dataInit.publisher?.name,
                        value: dataInit.publisher?.id,
                        key: dataInit.publisher?.id,
                    }
                ])
            }
        }
    }, [dataInit])



    const handleOnclose = () => {
        form.resetFields();
        setDataInit(null);
        onClose(false);
        setValue("");
    }

    const fetchAuthorList = async (name: string): Promise<ISelect[]> => {
        const res = await callFetchAllAuthor(`page=1&size=100&name=/${name}/i`);
        if (res && res.data) {
            const list = res.data.result;
            const temp = list.map(item => {
                return {
                    label: item.name as string,
                    value: item.id as string
                }
            })
            return temp;
        } else return [];
    }

    const fetchPublisherList = async (name: string): Promise<ISelect[]> => {
        const res = await callFetchAllPublisher(`page=1&size=100&name=/${name}/i`);
        if (res && res.data) {
            const list = res.data.result;
            const temp = list.map(item => {
                return {
                    label: item.name as string,
                    value: item.id as string
                }
            })
            return temp;
        } else return [];
    }


    return (
        <DrawerForm
            title="Thông tin tác giả"
            open={open}
            submitter={false}
            drawerProps={{
                destroyOnClose: true,
                onClose: () => handleOnclose(),
                footer: null,
                keyboard: false,
                maskClosable: false,
            }}
            form={form}
            autoFocusFirstInput
            initialValues={dataInit?.id ? dataInit : {}}
            preserve={false}
        >
            <Row gutter={16}>
                <Col span={8}>
                    <Form.Item
                        labelCol={{ span: 24 }}
                        label="Ảnh Logo"
                        name="image"
                        rules={[{
                            required: true,
                            message: 'Vui lòng không bỏ trống',
                            validator: () => {
                                // if (dataLogo.length > 0) return Promise.resolve();
                                // else return Promise.reject(false);
                            }
                        }]}
                    >
                        <ConfigProvider locale={enUS}>
                            <Upload
                                name="logo"
                                listType="picture-card"
                                className="avatar-uploader"
                                maxCount={1}
                                multiple={false}
                                disabled={true}
                                defaultFileList={
                                    dataInit?.id ?
                                        [
                                            {
                                                uid: uuidv4(),
                                                name: dataInit?.image ?? "",
                                                status: 'done',
                                                url: `${import.meta.env.VITE_BACKEND_URL_UPLOAD}/storage/product/${dataInit?.image}`,
                                            }
                                        ] : []
                                }

                            >

                            </Upload>
                        </ConfigProvider>
                    </Form.Item>

                </Col>
                <Col span={16}>
                    <ProFormText
                        label="Tên tác giả"
                        name="name"
                        rules={[{ required: true, message: 'Vui lòng không bỏ trống' }]}
                        placeholder="Nhập tên tác giả"
                        disabled={true}
                    />
                </Col>
                <Col span={12}>
                    <ProFormDigit
                        label="Số lượng"
                        name="quantity"
                        rules={[{ required: true, message: 'Vui lòng không bỏ trống' }]}
                        placeholder="Nhập số lượng"
                        disabled={true}
                    />
                </Col>
                <Col span={12}>
                    <ProFormDigit
                        label="Giá"
                        name="price"
                        rules={[{ required: true, message: 'Vui lòng không bỏ trống' }]}
                        placeholder="Nhập giá tiền"
                        initialValue={dataInit?.prices?.find(item => item.active)?.price}
                        disabled={true}
                    />
                </Col>
                <Col span={12}>
                    <ProFormText
                        label="Danh mục sách"
                        name="category"
                        rules={[{ required: true, message: 'Vui lòng không bỏ trống' }]}
                        placeholder="Nhập danh mục sách"
                        disabled={true}
                    />
                </Col>
                <Col span={12}>
                    <ProFormText
                        label="Thể loại"
                        name="genre"
                        rules={[{ required: true, message: 'Vui lòng không bỏ trống' }]}
                        placeholder="Nhập thể loại"
                        disabled={true}
                    />
                </Col>
                <Col span={9}>
                    <ProForm.Item
                        name="author"
                        label="Tác giả"
                        rules={[{ required: true, message: 'Vui lòng chọn tác giả!' }]}
                    >
                        <DebounceSelect
                            allowClear
                            showSearch
                            defaultValue={dataInit?.author?.name}
                            value={author}
                            placeholder="Chọn công vai trò"
                            fetchOptions={fetchAuthorList}
                            onChange={(newValue: any) => {
                                if (newValue?.length === 0 || newValue?.length === 1) {
                                    setAuthor(newValue as ISelect[]);
                                }
                            }}
                            style={{ width: '100%' }}
                            disabled={true}
                        />
                    </ProForm.Item>
                </Col>
                <Col span={9}>
                    <ProForm.Item
                        name="publisher"
                        label="Nhà xuất bản"
                        rules={[{ required: true, message: 'Vui lòng chọn nhà xuất bản!' }]}
                    >
                        <DebounceSelect
                            allowClear
                            showSearch
                            defaultValue={dataInit?.publisher?.name}
                            value={publisher}
                            placeholder="Chọn công vai trò"
                            fetchOptions={fetchPublisherList}
                            onChange={(newValue: any) => {
                                if (newValue?.length === 0 || newValue?.length === 1) {
                                    setpublisher(newValue as ISelect[]);
                                }
                            }}
                            style={{ width: '100%' }}
                            disabled={true}
                        />
                    </ProForm.Item>
                </Col>
                <Col span={6}>
                    <ProFormSwitch
                        label="Trạng thái"
                        name="active"
                        checkedChildren="ACTIVE"
                        unCheckedChildren="INACTIVE"
                        initialValue={true}
                        fieldProps={{
                            defaultChecked: true,
                        }}
                        disabled={true}
                    />
                </Col>
                <ProCard
                    title="Miêu tả"
                    subTitle="mô tả thông tin tác giả"
                    headStyle={{ color: '#d81921' }}
                    style={{ marginBottom: 20 }}
                    headerBordered
                    size="small"
                    bordered

                >
                    <Col span={24}>
                        <ReactQuill
                            theme="snow"
                            value={value}
                            onChange={setValue}
                            readOnly={true}
                        />
                    </Col>
                </ProCard>
            </Row>
        </DrawerForm>
    );
};

export default drawProduct;