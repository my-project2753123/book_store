import { CheckSquareOutlined, LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import { FooterToolbar, ModalForm, ProCard, ProForm, ProFormDigit, ProFormSwitch, ProFormText, ProFormTextArea } from "@ant-design/pro-components";
import { Col, ConfigProvider, Form, Modal, Row, Upload, message, notification } from "antd";
import { isMobile, isTablet } from "react-device-detect";
import 'react-quill/dist/quill.snow.css';
import enUS from 'antd/lib/locale/en_US';
import ReactQuill from 'react-quill';
import 'styles/reset.scss'
import { IProduct } from "@/types/backend";
import { useEffect, useState } from "react";
import { callCreateAuthor, callCreateProduct, callFetchAllAuthor, callFetchAllPublisher, callUpdateAuthor, callUpdateProduct, callUploadSingleFile } from "@/config/api";
import { v4 as uuidv4 } from 'uuid';
import { DebounceSelect } from "../users/debouce.select";

interface IProps {
    openModal: boolean
    setOpenModal: (v: boolean) => void;
    reloadTable: () => void;
    dataInit?: IProduct | null;
    setDataInit: (v: any) => void;
}

interface IProductImg {
    name: string;
    uid: string;
}
export interface ISelect {
    label: string;
    value: string;
    key?: string;
}

const modalProduct = (props: IProps) => {
    const { openModal, setOpenModal, reloadTable, dataInit, setDataInit } = props;

    const [form] = Form.useForm();
    const [loadingUpload, setLoadingUpload] = useState<boolean>(false);
    const [dataLogo, setDataLogo] = useState<IProductImg[]>([]);
    const [previewOpen, setPreviewOpen] = useState(false);
    const [previewImage, setPreviewImage] = useState('');
    const [previewTitle, setPreviewTitle] = useState('');
    const [value, setValue] = useState<string>("");
    const [author, setAuthor] = useState<ISelect[]>([]);
    const [publisher, setpublisher] = useState<ISelect[]>([]);
    const [price, setprice] = useState<number>();


    useEffect(() => {
        if (dataInit?.id && dataInit?.description) {
            setValue(dataInit.description);
        }
        if (dataInit?.id && dataInit?.image) {
            setDataLogo([{
                name: dataInit?.image,
                uid: uuidv4()
            }])
        }
        if (dataInit?.id && dataInit?.prices) {
            let price = dataInit?.prices.filter(item => item.active === true)
                .map(item => item.price).toString();
            setprice(+price)
        }
        if (dataInit?.id) {
            if (dataInit?.author) {
                setAuthor([
                    {
                        label: dataInit.author?.name,
                        value: dataInit.author?.id,
                        key: dataInit.author?.id,
                    }
                ])
            }
            if (dataInit?.publisher) {
                setpublisher([
                    {
                        label: dataInit.publisher?.name,
                        value: dataInit.publisher?.id,
                        key: dataInit.publisher?.id,
                    }
                ])
            }
        }
    }, [dataInit])

    const handleReset = () => {
        form.resetFields();
        setDataInit(null)
        setOpenModal(false)
        setValue("");
    }

    const handleRemoveFile = (file: any) => {
        setDataLogo([])
    }

    const handlePreview = async (file: any) => {
        if (!file.originFileObj) {
            setPreviewImage(file.url);
            setPreviewOpen(true);
            setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf('/') + 1));
            return;
        }
        getBase64(file.originFileObj, (url: string) => {
            setPreviewImage(url);
            setPreviewOpen(true);
            setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf('/') + 1));
        });
    };

    const getBase64 = (img: any, callback: any) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    };

    const beforeUpload = (file: any) => {
        console.log("check file", file)
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png' || file.type === "image/webp";
        if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
        }
        return isJpgOrPng && isLt2M;
    };

    const handleChange = (info: any) => {
        if (info.file.status === 'uploading') {
            setLoadingUpload(true);
        }
        if (info.file.status === 'done') {
            setLoadingUpload(false);
        }
        if (info.file.status === 'error') {
            setLoadingUpload(false);
            message.error(info?.file?.error?.event?.message ?? "Đã có lỗi xảy ra khi upload file.")
        }
    };

    const handleUploadFileLogo = async ({ file, onSuccess, onError }: any) => {
        const res = await callUploadSingleFile(file, "product");
        if (res && res.data) {
            setDataLogo([{
                name: res.data.fileName,
                uid: uuidv4()
            }])
            if (onSuccess) onSuccess('ok')
        } else {
            if (onError) {
                setDataLogo([])
                const error = new Error(res.message);
                onError({ event: error });
            }
        }
    };

    const submitProducts = async (valueForm: any) => {
        const { name, author, quantity, publisher, price, active, category, genre } = valueForm;
        if (dataLogo.length === 0) {
            message.error('Vui lòng upload ảnh Logo')
            return;
        }

        if (dataInit?.id) {
            const product: IProduct = {
                id: dataInit.id,
                name,
                description: value,
                image: dataLogo[0].name,
                prices: [{
                    price: price
                }],
                quantity,
                category,
                genre,
                author: {
                    id: author?.value ? author?.value : dataInit?.author?.id,
                    name: author?.label ? author?.label : dataInit?.author?.name
                },
                publisher: {
                    id: publisher?.value ? publisher?.value : dataInit?.publisher?.id,
                    name: publisher?.label ? publisher?.label : dataInit?.publisher?.name
                },
                active
            }
            //update
            const res = await callUpdateProduct(product);
            if (res.data) {
                message.success("Cập nhật product thành công");
                handleReset();
                reloadTable();
            } else {
                notification.error({
                    message: 'Có lỗi xảy ra',
                    description: res.message
                });
            }
        }
        else {
            const product: IProduct = {
                name,
                description: value,
                image: dataLogo[0].name,
                prices: [{
                    price: price
                }],
                category,
                genre,
                quantity,
                author: {
                    id: author?.value ? author?.value : dataInit?.author?.id,
                    name: author?.label ? author?.label : dataInit?.author?.name
                },
                publisher: {
                    id: publisher?.value ? publisher?.value : dataInit?.publisher?.id,
                    name: publisher?.label ? publisher?.label : dataInit?.publisher?.name
                },
                active

            }
            //create
            const res = await callCreateProduct(product);
            if (res.data) {
                message.success("Thêm mới product thành công");
                handleReset();
                reloadTable();
            } else {
                notification.error({
                    message: 'Có lỗi xảy ra',
                    description: res.message
                });
            }
        }
    }

    const fetchAuthorList = async (name: string): Promise<ISelect[]> => {
        const res = await callFetchAllAuthor(`page=1&size=100&name=/${name}/i`);
        if (res && res.data) {
            const list = res.data.result;
            const temp = list.map(item => {
                return {
                    label: item.name as string,
                    value: item.id as string
                }
            })
            return temp;
        } else return [];
    }

    const fetchPublisherList = async (name: string): Promise<ISelect[]> => {
        const res = await callFetchAllPublisher(`page=1&size=100&name=/${name}/i`);
        if (res && res.data) {
            const list = res.data.result;
            const temp = list.map(item => {
                return {
                    label: item.name as string,
                    value: item.id as string
                }
            })
            return temp;
        } else return [];
    }

    return (
        <>
            <ModalForm
                title={<>{dataInit?.id ? "Cập nhật book" : "Tạo mới book"}</>}
                open={openModal}
                modalProps={{
                    onCancel: () => { handleReset() },
                    afterClose: () => handleReset(),
                    destroyOnClose: true,
                    width: isMobile || isTablet ? "100%" : 900,
                    footer: null,
                    keyboard: false,
                    maskClosable: false,
                    // className: `modal-company ${animation}`,
                    // rootClassName: `modal-company-root ${animation}`
                }}
                scrollToFirstError={true}
                preserve={false}
                form={form}
                onFinish={submitProducts}
                initialValues={dataInit?.id ? dataInit : {}}
            >
                <Row gutter={16}>
                    <Col span={8}>
                        <Form.Item
                            labelCol={{ span: 24 }}
                            label="Ảnh sản phẩm"
                            name="image"
                            rules={[{
                                required: true,
                                message: 'Vui lòng không bỏ trống',
                                validator: () => {
                                    if (dataLogo.length > 0) return Promise.resolve();
                                    else return Promise.reject(false);
                                }
                            }]}
                        >
                            <ConfigProvider locale={enUS}>
                                <Upload
                                    name="image"
                                    listType="picture-card"
                                    className="avatar-uploader"
                                    maxCount={1}
                                    multiple={false}
                                    customRequest={handleUploadFileLogo}
                                    beforeUpload={beforeUpload}
                                    onChange={handleChange}
                                    onRemove={(file) => handleRemoveFile(file)}
                                    onPreview={handlePreview}
                                    defaultFileList={
                                        dataInit?.id ?
                                            [
                                                {
                                                    uid: uuidv4(),
                                                    name: dataInit?.image ?? "",
                                                    status: 'done',
                                                    url: `${import.meta.env.VITE_BACKEND_URL_UPLOAD}/storage/product/${dataInit?.image}`,
                                                }
                                            ] : []
                                    }

                                >
                                    <div>
                                        {loadingUpload ? <LoadingOutlined /> : <PlusOutlined />}
                                        <div style={{ marginTop: 8 }}>Upload</div>
                                    </div>
                                </Upload>
                            </ConfigProvider>
                        </Form.Item>
                    </Col>
                    <Col span={16}>
                        <ProFormText
                            label="Tên sách"
                            name="name"
                            rules={[{ required: true, message: 'Vui lòng không bỏ trống' }]}
                            placeholder="Nhập tên sách"
                        />
                    </Col>
                    <Col span={12}>
                        <ProFormDigit
                            label="Số lượng"
                            name="quantity"
                            rules={[{ required: true, message: 'Vui lòng không bỏ trống' }]}
                            placeholder="Nhập số lượng"
                        />
                    </Col>
                    <Col span={12}>
                        <ProFormDigit
                            label="Giá"
                            name="price"
                            rules={[{ required: true, message: 'Vui lòng không bỏ trống' }]}
                            placeholder="Nhập giá tiền"
                            initialValue={price}
                        />
                    </Col>
                    <Col span={12}>
                        <ProFormText
                            label="Danh mục sách"
                            name="category"
                            rules={[{ required: true, message: 'Vui lòng không bỏ trống' }]}
                            placeholder="Nhập danh mục sách"
                        />
                    </Col>
                    <Col span={12}>
                        <ProFormText
                            label="Thể loại"
                            name="genre"
                            rules={[{ required: true, message: 'Vui lòng không bỏ trống' }]}
                            placeholder="Nhập thể loại"
                        />
                    </Col>
                    <Col span={9}>
                        <ProForm.Item
                            name="author"
                            label="Tác giả"
                            rules={[{ required: true, message: 'Vui lòng chọn tác giả!' }]}
                        >
                            <DebounceSelect
                                allowClear
                                showSearch
                                defaultValue={author}
                                value={author}
                                placeholder="Chọn công vai trò"
                                fetchOptions={fetchAuthorList}
                                onChange={(newValue: any) => {
                                    if (newValue?.length === 0 || newValue?.length === 1) {
                                        setAuthor(newValue as ISelect[]);
                                    }
                                }}
                                style={{ width: '100%' }}
                            />
                        </ProForm.Item>
                    </Col>
                    <Col span={9}>
                        <ProForm.Item
                            name="publisher"
                            label="Nhà xuất bản"
                            rules={[{ required: true, message: 'Vui lòng chọn nhà xuất bản!' }]}
                        >
                            <DebounceSelect
                                allowClear
                                showSearch
                                defaultValue={publisher}
                                value={publisher}
                                placeholder="Chọn công vai trò"
                                fetchOptions={fetchPublisherList}
                                onChange={(newValue: any) => {
                                    if (newValue?.length === 0 || newValue?.length === 1) {
                                        setpublisher(newValue as ISelect[]);
                                    }
                                }}
                                style={{ width: '100%' }}
                            />
                        </ProForm.Item>
                    </Col>
                    <Col span={6}>
                        <ProFormSwitch
                            label="Trạng thái"
                            name="active"
                            checkedChildren="ACTIVE"
                            unCheckedChildren="INACTIVE"
                            initialValue={true}
                            fieldProps={{
                                defaultChecked: true,
                            }}
                        />
                    </Col>
                    <ProCard
                        title="Miêu tả"
                        subTitle="mô tả thông tin sách"
                        headStyle={{ color: '#d81921' }}
                        style={{ marginBottom: 20 }}
                        headerBordered
                        size="small"
                        bordered
                    >
                        <Col span={24}>
                            <ReactQuill
                                theme="snow"
                                value={value}
                                onChange={setValue}
                            />
                        </Col>
                    </ProCard>
                </Row>
            </ModalForm>
            <Modal
                open={previewOpen}
                title={previewTitle}
                footer={null}
                onCancel={() => setPreviewOpen(false)}
                style={{ zIndex: 1500 }}
            >
                <img alt="example" style={{ width: '100%' }} src={previewImage} />
            </Modal>
        </>
    );
};

export default modalProduct;