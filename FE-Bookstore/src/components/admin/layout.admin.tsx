import { BugOutlined, ContainerOutlined, DashboardOutlined, ExceptionOutlined, LineChartOutlined, PoweroffOutlined, ProductOutlined, UserOutlined } from '@ant-design/icons';
import { Divider, Layout, Menu } from 'antd';
import { useEffect, useState } from 'react';
import { isMobileOnly } from 'react-device-detect';
import HeaderAdmin from 'components/admin/share/header.admin';
import { Outlet, useNavigate } from 'react-router-dom';
import { useAppSelector } from '@/redux/hook';
import { MenuProps } from 'antd/lib';
import { ALL_PERMISSIONS } from '@/config/permission';

const { Header, Sider, Content } = Layout;

const LayoutAdmin = (props: any) => {
    const permissions = useAppSelector(state => state.account.user.role.permissions);
    const ACL_ENABLE = import.meta.env.VITE_ACL_ENABLE;

    const [menu, setmenu] = useState<MenuProps['items']>();

    useEffect(() => {
        if (permissions?.length || !ACL_ENABLE) {

            const viewRole = permissions?.find(item =>
                item.apiPath === ALL_PERMISSIONS.ROLES.GET_PAGINATE.apiPath
                && item.method === ALL_PERMISSIONS.ROLES.GET_PAGINATE.method
            )

            const viewUser = permissions?.find(item =>
                item.apiPath === ALL_PERMISSIONS.USERS.GET_PAGINATE.apiPath
                && item.method === ALL_PERMISSIONS.USERS.GET_PAGINATE.method
            )

            const viewPermission = permissions?.find(item =>
                item.apiPath === ALL_PERMISSIONS.PERMISSIONS.GET_PAGINATE.apiPath
                && item.method === ALL_PERMISSIONS.PERMISSIONS.GET_PAGINATE.method
            )

            const viewAuthor = permissions?.find(item =>
                item.apiPath === ALL_PERMISSIONS.AUTHOR.GET_PAGINATE.apiPath
                && item.method === ALL_PERMISSIONS.AUTHOR.GET_PAGINATE.method
            )

            const viewPublisher = permissions?.find(item =>
                item.apiPath === ALL_PERMISSIONS.PUBLISHER.GET_PAGINATE.apiPath
                && item.method === ALL_PERMISSIONS.PUBLISHER.GET_PAGINATE.method
            )

            const viewProduct = permissions?.find(item =>
                item.apiPath === ALL_PERMISSIONS.BOOKS.GET_PAGINATE.apiPath
                && item.method === ALL_PERMISSIONS.BOOKS.GET_PAGINATE.method
            )

            const viewOrder = permissions?.find(item =>
                item.apiPath === ALL_PERMISSIONS.ORDER.GET_PAGINATE.apiPath
                && item.method === ALL_PERMISSIONS.ORDER.GET_PAGINATE.method
            )

            const menuFull = [
                { key: '/admin', icon: <DashboardOutlined />, label: 'Dashboard' },
                ...(viewUser || ACL_ENABLE === 'false' ?
                    [{ key: '/admin/user', icon: <UserOutlined />, label: 'Users' }] : []),
                ...(viewRole || ACL_ENABLE === 'false' ?
                    [{ key: '/admin/role', icon: <PoweroffOutlined />, label: 'Role' }] : []),
                ...(viewPermission || ACL_ENABLE === 'false' ?
                    [{ key: '/admin/permission', icon: <ContainerOutlined />, label: 'Permission' }] : []),
                ...(viewOrder || ACL_ENABLE === 'false' ?
                    [{ key: '/admin/order', icon: <ExceptionOutlined />, label: 'Orders' }] : []),
                ...(viewAuthor || viewProduct || viewPublisher || ACL_ENABLE === 'false' ?
                    [
                        {
                            key: "/admin/product",
                            label: 'Products',
                            icon: <ProductOutlined />,
                            children: [
                                ...(viewAuthor || ACL_ENABLE === 'false' ?
                                    [{ key: '/author', label: 'Author' }] : []),
                                ...(viewProduct || ACL_ENABLE === 'false' ?
                                    [{ key: '/books', label: 'Books' }] : []),
                                ...(viewPublisher || ACL_ENABLE === 'false' ?
                                    [{ key: '/publisher', label: 'Publisher' }] : [])

                            ],
                        },
                    ] : []),

                {
                    key: '/admin/statistical',
                    label: 'Statistical',
                    icon: <LineChartOutlined />,
                    children: [
                        { key: '/revenue', label: 'Revenue' },
                        { key: '10', label: 'Option 10' },
                        {
                            key: 'sub3',
                            label: 'Submenu',
                            children: [
                                { key: '11', label: 'Option 11' },
                                { key: '12', label: 'Option 12' },
                            ],
                        },
                    ],
                },
            ]

            setmenu(menuFull);

        }
    }, [permissions])

    const [collapsed, setCollapsed] = useState<boolean>(false);
    const navigate = useNavigate();

    const handleChangeMenu = (item: any) => {
        let pathArr = item.keyPath.reverse()
        let path = pathArr.join('');
        navigate(`${path}`)

    }

    return (
        <Layout
            style={{ minHeight: '100vh' }}
        >
            <Sider
                theme='light'
                collapsed={isMobileOnly ? true : collapsed}
                onCollapse={(value) => setCollapsed(value)}
            >
                {
                    isMobileOnly
                        ?
                        <></>
                        :
                        <>
                            <div style={{ height: 23, margin: 16, textAlign: 'center' }}>
                                <BugOutlined />  ADMIN
                            </div>
                            <Divider />
                        </>
                }
                <Menu
                    mode="inline"
                    defaultSelectedKeys={[`${window.location.pathname}`]}
                    defaultOpenKeys={['/admin/product']}
                    items={menu}
                    onClick={(item) => handleChangeMenu(item)}
                />

            </Sider>
            <Layout>
                <Header
                    style={{ backgroundColor: "#f5f5f5", padding: "0 5px" }}
                >
                    <HeaderAdmin
                        collapsed={collapsed}
                        setCollapsed={setCollapsed}
                    />
                </Header>
                <Content
                    style={{ padding: "15px" }}
                >
                    <Outlet />
                </Content>
            </Layout>
        </Layout>
    );
};

export default LayoutAdmin;