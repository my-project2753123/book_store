import { callCreateUser, callFetchAllRole, callUpdateUser } from '@/config/api';
import { IUser } from '@/types/backend';
import { ModalForm, ProForm, ProFormDigit, ProFormSelect, ProFormText } from '@ant-design/pro-components';
import { Form, message, notification } from 'antd';
import { isMobile, isTablet } from 'react-device-detect';
import { DebounceSelect } from "./debouce.select";
import { useEffect, useState } from 'react';


interface IProps {
    openModal: boolean
    setOpenModal: (v: boolean) => void;
    reloadTable: () => void;
    dataInit?: IUser | null;
    setDataInit: (v: any) => void;
}

export interface ISelect {
    label: string;
    value: string;
    key?: string;
}

const modalUser = (props: IProps) => {

    const { openModal, setOpenModal, reloadTable, dataInit, setDataInit } = props;
    const [form] = Form.useForm();
    const [roles, setRoles] = useState<ISelect[]>([]);

    const handleReset = () => {
        form.resetFields();
        setDataInit(null)
        setOpenModal(false)
    }

    useEffect(() => {
        if (dataInit?.id) {
            if (dataInit.role) {
                setRoles([
                    {
                        label: dataInit.role?.name,
                        value: dataInit.role?.id,
                        key: dataInit.role?.id,
                    }
                ])
            }
        }
    }, [dataInit]);

    const handleAddUser = async (values: any) => {
        const { name, email, password, age, gender, address, role } = values;
        if (!dataInit?.id) {
            const user: IUser = {
                name: name,
                email: email,
                password: password,
                age: age,
                gender: gender,
                address: address,
                role: {
                    id: role.value,
                    name: role.label
                }
            }
            const res = await callCreateUser(user);
            if (res?.data?.id) {
                message.success("Tạo mới user thành công!");
                setOpenModal(false)
                reloadTable();
            } else {
                notification.error({
                    message: "có lỗi xảy ra",
                    description:
                        res?.message && Array.isArray(res?.message) ? res?.message[0] : res?.message, duration: 5
                })
            }
        } else {
            const user: IUser = {
                id: dataInit?.id,
                name: name,
                email: email,
                password: password,
                age: age,
                gender: gender,
                address: address,
                role: {
                    id: role.value,
                    name: role.label
                }
            }
            const res = await callUpdateUser(user);
            if (res?.data?.id) {
                message.success("Cập nhật user thành công!");
                setOpenModal(false)
                reloadTable();
            } else {
                notification.error({
                    message: "có lỗi xảy ra",
                    description:
                        res?.message && Array.isArray(res?.message) ? res?.message[0] : res?.message, duration: 5
                })
            }
        }


    }

    const fetchRoleList = async (name: string): Promise<ISelect[]> => {
        const res = await callFetchAllRole(`page=1&size=100&name=/${name}/i`);
        if (res && res.data) {
            const list = res.data.result;
            const temp = list.map(item => {
                return {
                    label: item.name as string,
                    value: item.id as string
                }
            })
            return temp;
        } else return [];
    }

    return (
        <ModalForm
            title={<>{dataInit?.id ? "Cập nhật User" : "Tạo mới User"}</>}
            open={openModal}
            modalProps={{
                onCancel: () => { handleReset() },
                afterClose: () => handleReset(),
                destroyOnClose: true,
                width: isMobile || isTablet ? "100%" : 900,
                keyboard: false,
                maskClosable: false,
                okText: <>{dataInit?.id ? "Cập nhật" : "Tạo mới"}</>,
                cancelText: "Hủy"
            }}
            scrollToFirstError={true}
            preserve={false}
            form={form}
            onFinish={handleAddUser}
            initialValues={dataInit?.id ? dataInit : {}}
        >
            <ProForm.Group>
                <ProFormText
                    width="md"
                    name="email"
                    label="Email"
                    tooltip="Địa chỉ Email"
                    placeholder="Email"
                    rules={[
                        { required: true, message: 'Vui lòng không bỏ trống' },
                        { type: 'email', message: 'Vui lòng nhập email hợp lệ' }
                    ]}
                    disabled={dataInit?.id ? true : false}

                />

                <ProFormText.Password
                    width="md"
                    name="password"
                    label="Password"
                    placeholder="Password"
                    rules={[
                        { required: true, message: 'Vui lòng không bỏ trống' },
                    ]}
                />
            </ProForm.Group>
            <ProForm.Group>
                <ProFormText
                    width="md"
                    name="name"
                    label="User Name"
                    placeholder="User Name"
                    rules={[
                        { required: true, message: 'Vui lòng không bỏ trống' },
                    ]}
                />
                <ProFormText
                    width="md"
                    name="address"
                    label="Address"
                    placeholder="Address"
                    rules={[
                        { required: true, message: 'Vui lòng không bỏ trống' },
                    ]}
                />
            </ProForm.Group>
            <ProForm.Group>
                <ProFormDigit
                    width="md"
                    name="age"
                    label="Age"
                    placeholder="Age"
                    rules={[
                        { required: true, message: 'Vui lòng không bỏ trống' },
                    ]}
                />
                <ProFormSelect
                    options={[
                        {
                            value: 'MALE',
                            label: 'MALE',
                        },
                        {
                            value: 'FEMALE',
                            label: 'FEMALE',
                        },
                        {
                            value: 'OTHER',
                            label: 'OTHER',
                        },
                    ]}
                    width="xs"
                    name="gender"
                    label="Gender"
                    rules={[
                        { required: true, message: 'Vui lòng không bỏ trống' },
                    ]}
                    initialValue={dataInit?.id ? dataInit?.gender : ""}
                />
                <ProForm.Item
                    name="role"
                    label="Vai trò"
                    rules={[{ required: true, message: 'Vui lòng chọn vai trò!' }]}
                >
                    <DebounceSelect
                        allowClear
                        showSearch
                        defaultValue={roles}
                        value={roles}
                        placeholder="Chọn công vai trò"
                        fetchOptions={fetchRoleList}
                        onChange={(newValue: any) => {
                            if (newValue?.length === 0 || newValue?.length === 1) {
                                setRoles(newValue as ISelect[]);
                            }
                        }}
                        style={{ width: '100%' }}
                    />
                </ProForm.Item>
            </ProForm.Group>

        </ModalForm>
    );
};

export default modalUser;