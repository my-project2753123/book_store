import { IUser } from '@/types/backend';
import {
    DrawerForm,
    ProForm,
    ProFormDigit,
    ProFormSelect,
    ProFormText,
} from '@ant-design/pro-components';
import { Form } from 'antd';
import { DebounceSelect } from './debouce.select';
import { useEffect, useState } from 'react';
import { callFetchAllRole } from '@/config/api';

interface IProps {
    onClose: (v: boolean) => void;
    open: boolean;
    dataInit: IUser | null;
    setDataInit: (v: IUser | null) => void;
}

export interface ISelect {
    label: string;
    value: string;
}


const drawUser = (props: IProps) => {

    const { onClose, open, dataInit, setDataInit } = props;
    const [roles, setRoles] = useState<ISelect[]>([]);
    const [form] = Form.useForm();

    useEffect(() => {
        if (dataInit?.id) {
            if (dataInit.role) {
                setRoles([
                    {
                        label: dataInit.role?.name,
                        value: dataInit.role?.id,
                    }
                ])
            }
        }
    }, [dataInit]);

    const fetchRoleList = async (name: string): Promise<ISelect[]> => {
        const res = await callFetchAllRole(`page=1&size=100&name=/${name}/i`);
        if (res && res.data) {
            const list = res.data.result;
            const temp = list.map(item => {
                return {
                    label: item.name as string,
                    value: item.id as string
                }
            })
            return temp;
        } else return [];
    }

    const handleOnclose = () => {
        form.resetFields();
        setDataInit(null);
        onClose(false);
    }

    return (
        <DrawerForm
            title="User detail"
            resize={{
                onResize() {
                },
                maxWidth: window.innerWidth * 0.8,
                minWidth: 400,
            }}
            form={form}
            open={open}
            autoFocusFirstInput
            drawerProps={{
                destroyOnClose: true,
                onClose: () => handleOnclose(),
            }}
            submitter={false}
            initialValues={dataInit?.id ? dataInit : {}}
            preserve={false}
        >
            <ProForm.Group>
                <ProFormText
                    name="email"
                    width="md"
                    label="Email"
                    tooltip="Địa chỉ email"
                    placeholder="Email"
                    disabled={true}
                />
                <ProFormText
                    width="md"
                    name="name"
                    label="User Name"
                    placeholder="Họ và tên"
                    disabled={true}
                />
            </ProForm.Group>
            <ProForm.Group>
                <ProFormDigit
                    width="md"
                    name="age"
                    label="Age"
                    placeholder="Age"
                    disabled={true}
                />
                <ProFormText
                    width="md"
                    name="address"
                    label="Address"
                    placeholder="Address"
                    disabled={true}
                />
            </ProForm.Group>
            <ProForm.Group>
                <ProFormSelect
                    options={[
                        {
                            value: 'MALE',
                            label: 'MALE',
                        },
                        {
                            value: 'FEMALE',
                            label: 'FEMALE',
                        },
                        {
                            value: 'OTHER',
                            label: 'OTHER',
                        },
                    ]}
                    width="md"
                    name="gender"
                    label="Gender"
                    disabled={true}
                />
            </ProForm.Group>
        </DrawerForm>
    );
};

export default drawUser;