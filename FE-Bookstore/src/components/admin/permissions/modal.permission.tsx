import { callCreatePermission, callUpdatePermission } from "@/config/api";
import { IPermission } from "@/types/backend";
import { ModalForm, ProForm, ProFormSelect, ProFormText } from '@ant-design/pro-components';
import { Form, message, notification } from 'antd';
import { useEffect } from "react";

interface IProps {
    openModal: boolean
    setOpenModal: (v: boolean) => void;
    reloadTable: () => void;
    dataInit?: IPermission | null;
    setDataInit: (v: any) => void;
}

const modalPermission = (props: IProps) => {

    const { openModal, setOpenModal, reloadTable, dataInit, setDataInit } = props;
    const [form] = Form.useForm();

    const handleAddPermission = async (value: any) => {
        const { name, apiPath, module, method } = value;
        if (!dataInit?.id) {
            const res = await callCreatePermission(name, apiPath, module, method);
            if (res?.data?.id) {
                message.success("create permission is success!")
                setOpenModal(false);
                reloadTable();
            } else {
                notification.error({
                    message: "Có lỗi xảy ra",
                    description:
                        res?.message && Array.isArray(res?.message) ? res?.message[0] : res?.message, duration: 5
                })
            }
        } else {
            const res = await callUpdatePermission(parseInt(dataInit?.id), name, apiPath, module, method);
            if (res?.data?.id) {
                message.success("Cập nhật permission thành công!");
                setOpenModal(false)
                reloadTable();
            } else {
                notification.error({
                    message: "có lỗi xảy ra",
                    description:
                        res?.message && Array.isArray(res?.message) ? res?.message[0] : res?.message, duration: 5
                })
            }
        }

    }

    const handleReset = () => {
        form.resetFields();
        setDataInit(null);
        setOpenModal(false);
    };

    return (
        <ModalForm
            title={dataInit?.id ? "Update Permission" : "Add Permission"}
            open={openModal}
            modalProps={
                {
                    onCancel: () => handleReset(),
                    afterClose: () => handleReset(),
                    destroyOnClose: true,
                    okText: <>{dataInit?.id ? "Cập nhật" : "Tạo mới"}</>,
                    cancelText: "Hủy"
                }
            }
            form={form}
            preserve={false}
            onFinish={async (values) => {
                handleAddPermission(values)
            }}
            initialValues={dataInit?.id ? dataInit : {}}
        >
            <ProForm.Group>
                <ProFormText
                    width="md"
                    name="name"
                    label="Name"
                    placeholder="Name"
                    rules={[
                        { required: true, message: 'Vui lòng không bỏ trống' },
                    ]}
                />

                <ProFormText
                    width="md"
                    name="apiPath"
                    label="Path"
                    placeholder="Path"
                    rules={[
                        { required: true, message: 'Vui lòng không bỏ trống' },
                    ]}
                />
            </ProForm.Group>
            <ProForm.Group>
                <ProFormText
                    width="md"
                    name="module"
                    label="Module"
                    placeholder="Module"
                    rules={[
                        { required: true, message: 'Vui lòng không bỏ trống' },
                    ]}
                />
                <ProFormSelect
                    options={[
                        {
                            value: 'GET',
                            label: 'GET',
                        },
                        {
                            value: 'POST',
                            label: 'POST',
                        },
                        {
                            value: 'PUT',
                            label: 'PUT',
                        },
                        {
                            value: 'DELETE',
                            label: 'DELETE',
                        },
                    ]}
                    width="md"
                    name="method"
                    label="Method"
                    rules={[
                        { required: true, message: 'Vui lòng không bỏ trống' },
                    ]}
                />
            </ProForm.Group>
        </ModalForm>
    );
};

export default modalPermission;