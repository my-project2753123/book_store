import { callLogout } from '@/config/api';
import { useAppSelector } from '@/redux/hook';
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import { Avatar, Button, Dropdown, MenuProps, Space, message } from 'antd';
import { isMobileOnly } from 'react-device-detect';
import { Link, useNavigate } from 'react-router-dom';

interface IProps {
    collapsed?: boolean
    setCollapsed: (collapsed: boolean) => void;
}


const headerAdmin = (props: IProps) => {

    const { collapsed, setCollapsed } = props;
    const userAccount = useAppSelector(state => state.account.user);
    const { name } = userAccount;
    const navigate = useNavigate();

    const itemsDropdown = [
        {
            label: <label>Trang chủ</label>,
            key: 'home',
        },
        {
            label: <label
                style={{ cursor: 'pointer' }}
            >Đăng xuất
            </label>,
            key: 'logout',
        },
    ];

    const handleMenuClick: MenuProps['onClick'] = (e) => {
        if (e.key === "logout") {
            handleLogout();
        }
        if (e.key === "home") {
            navigate("/")
        }

    };

    const menuProps = {
        items: itemsDropdown,
        onClick: handleMenuClick,
    };

    const handleLogout = async () => {
        const res = await callLogout();
        if (+res?.statusCode === +200) {
            localStorage.removeItem("access_token");
            message.success("Đăng xuất thành công!");
            navigate("/login");
        }
    }

    return (
        <div
            style={{
                display: "flex", alignItems: "center", justifyContent: "space-between"
            }}
        >
            {
                !isMobileOnly
                    ?
                    <div>
                        <Button
                            type="text"
                            icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
                            onClick={() => setCollapsed(!collapsed)}
                            style={{
                                fontSize: '16px',
                                width: 64,
                                height: 64,
                            }}
                        />
                    </div>
                    :
                    <></>
            }

            <div
                style={{
                    padding: "0 20px",
                    marginLeft: isMobileOnly ? "auto" : ""
                }}
            >
                <Dropdown
                    menu={menuProps}
                    trigger={['click']}
                >
                    <Space style={{ cursor: "pointer" }}>
                        {
                            !isMobileOnly
                                ?
                                <>
                                    Welcome ,{name}
                                </>
                                :
                                <></>
                        }
                        <Avatar> {name?.substring(0, 2)?.toUpperCase()} </Avatar>
                    </Space>
                </Dropdown>
            </div>
        </div>
    );
};

export default headerAdmin;