import { MenuProps } from "antd";
import { ContainerOutlined, DashboardOutlined, LineChartOutlined, PoweroffOutlined, ProductOutlined, UserOutlined } from '@ant-design/icons';
import { useAppSelector } from "@/redux/hook";
import { useEffect } from "react";
import { ALL_PERMISSIONS } from "@/config/permission";

type MenuItem = Required<MenuProps>['items'][number];

const permissions = useAppSelector(state => state.account.user.role.permissions);
const ACL_ENABLE = import.meta.env.VITE_ACL_ENABLE;

useEffect(() => {
    if (permissions?.length && ACL_ENABLE === false) {

        const viewRole = permissions?.find(item =>
            item.apiPath === ALL_PERMISSIONS.ROLES.GET_PAGINATE.apiPath
            && item.method === ALL_PERMISSIONS.ROLES.GET_PAGINATE.method
        )

        const viewUser = permissions?.find(item =>
            item.apiPath === ALL_PERMISSIONS.USERS.GET_PAGINATE.apiPath
            && item.method === ALL_PERMISSIONS.USERS.GET_PAGINATE.method
        )

        const viewPermission = permissions?.find(item =>
            item.apiPath === ALL_PERMISSIONS.PERMISSIONS.GET_PAGINATE.apiPath
            && item.method === ALL_PERMISSIONS.PERMISSIONS.GET_PAGINATE.method
        )

        const menuFull = [
            { key: '/admin', icon: <DashboardOutlined />, label: 'Dashboard' },
            viewUser ? { key: '/admin/user', icon: <UserOutlined />, label: 'Users' } : {},
            viewRole ? { key: '/admin/role', icon: <PoweroffOutlined />, label: 'Role' } : {},
            viewPermission ? { key: '/admin/permission', icon: <ContainerOutlined />, label: 'Permission' } : {},
            {
                key: 'sub1',
                label: 'Products',
                icon: <ProductOutlined />,
                children: [
                    { key: '5', label: 'Option 5' },
                    { key: '6', label: 'Option 6' },
                    { key: '7', label: 'Option 7' },
                    { key: '8', label: 'Option 8' },
                ],
            },
            {
                key: 'sub2',
                label: 'Statistical',
                icon: <LineChartOutlined />,
                children: [
                    { key: '9', label: 'Option 9' },
                    { key: '10', label: 'Option 10' },
                    {
                        key: 'sub3',
                        label: 'Submenu',
                        children: [
                            { key: '11', label: 'Option 11' },
                            { key: '12', label: 'Option 12' },
                        ],
                    },
                ],
            },
        ]

    }
}, [permissions])


// export const items: MenuItem[] = [
//     { key: '/admin', icon: <DashboardOutlined />, label: 'Dashboard' },
//     { key: '/admin/user', icon: <UserOutlined />, label: 'Users' },
//     { key: '/admin/role', icon: <PoweroffOutlined />, label: 'Role' },
//     { key: '/admin/permission', icon: <ContainerOutlined />, label: 'Permission' },
//     {
//         key: 'sub1',
//         label: 'Products',
//         icon: <ProductOutlined />,
//         children: [
//             { key: '5', label: 'Option 5' },
//             { key: '6', label: 'Option 6' },
//             { key: '7', label: 'Option 7' },
//             { key: '8', label: 'Option 8' },
//         ],
//     },
//     {
//         key: 'sub2',
//         label: 'Statistical',
//         icon: <LineChartOutlined />,
//         children: [
//             { key: '9', label: 'Option 9' },
//             { key: '10', label: 'Option 10' },
//             {
//                 key: 'sub3',
//                 label: 'Submenu',
//                 children: [
//                     { key: '11', label: 'Option 11' },
//                     { key: '12', label: 'Option 12' },
//                 ],
//             },
//         ],
//     },
// ];