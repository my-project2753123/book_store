import { IAuthor } from "@/types/backend";
import { DrawerForm, ProCard, ProForm, ProFormDateRangePicker, ProFormSelect, ProFormText } from "@ant-design/pro-components";
import { Col, ConfigProvider, Form, Row, Upload, message } from "antd";
import enUS from 'antd/lib/locale/en_US';
import { useEffect, useState } from "react";
import ReactQuill from "react-quill";
import { v4 as uuidv4 } from 'uuid';

interface IProps {
    onClose: (v: boolean) => void;
    open: boolean;
    dataInit: IAuthor | null;
    setDataInit: (v: IAuthor | null) => void;
}

interface IAuthorLogo {
    name: string;
    uid: string;
}

const drawAuthor = (props: IProps) => {

    const { onClose, open, dataInit, setDataInit } = props;
    const [form] = Form.useForm();
    const [value, setValue] = useState<string>("");

    useEffect(() => {
        if (dataInit?.id && dataInit?.description) {
            setValue(dataInit.description);
        }
    }, [dataInit])

    const handleOnclose = () => {
        form.resetFields();
        setDataInit(null);
        onClose(false);
        setValue("");
    }

    return (
        <DrawerForm
            title="Thông tin tác giả"
            open={open}
            submitter={false}
            drawerProps={{
                destroyOnClose: true,
                onClose: () => handleOnclose(),
            }}
            form={form}
            autoFocusFirstInput
            initialValues={dataInit?.id ? dataInit : {}}
            preserve={false}
        >
            <Row gutter={16}>
                <Col span={8}>
                    <Form.Item
                        labelCol={{ span: 24 }}
                        label="Ảnh Logo"
                        name="logo"
                        rules={[{
                            required: true,
                            message: 'Vui lòng không bỏ trống',
                            validator: () => {
                                // if (dataLogo.length > 0) return Promise.resolve();
                                // else return Promise.reject(false);
                            }
                        }]}
                    >
                        <ConfigProvider locale={enUS}>
                            <Upload
                                name="logo"
                                listType="picture-card"
                                className="avatar-uploader"
                                maxCount={1}
                                multiple={false}
                                disabled={true}
                                defaultFileList={
                                    dataInit?.id ?
                                        [
                                            {
                                                uid: uuidv4(),
                                                name: dataInit?.logo ?? "",
                                                status: 'done',
                                                url: `${import.meta.env.VITE_BACKEND_URL_UPLOAD}/storage/author/${dataInit?.logo}`,
                                            }
                                        ] : []
                                }

                            >
                                <div>
                                    <div style={{ marginTop: 8 }}>Upload</div>
                                </div>
                            </Upload>
                        </ConfigProvider>
                    </Form.Item>

                </Col>
                <Col span={16}>
                    <ProFormText
                        label="Tên tác giả"
                        name="name"
                        rules={[{ required: true, message: 'Vui lòng không bỏ trống' }]}
                        placeholder="Nhập tên tác giả"
                        disabled={true}
                    />
                </Col>
                <ProCard
                    title="Miêu tả"
                    subTitle="mô tả thông tin tác giả"
                    headStyle={{ color: '#d81921' }}
                    style={{ marginBottom: 20 }}
                    headerBordered
                    size="small"
                    bordered

                >
                    <Col span={24}>
                        <ReactQuill
                            theme="snow"
                            value={value}
                            onChange={setValue}
                            readOnly={true}
                        />
                    </Col>
                </ProCard>
            </Row>
        </DrawerForm>
    );
};

export default drawAuthor;