import { CheckSquareOutlined, LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import { FooterToolbar, ModalForm, ProCard, ProFormText, ProFormTextArea } from "@ant-design/pro-components";
import { Col, ConfigProvider, Form, Modal, Row, Upload, message, notification } from "antd";
import { isMobile, isTablet } from "react-device-detect";
import 'react-quill/dist/quill.snow.css';
import enUS from 'antd/lib/locale/en_US';
import ReactQuill from 'react-quill';
import 'styles/reset.scss'
import { IAuthor } from "@/types/backend";
import { useEffect, useState } from "react";
import { callCreateAuthor, callUpdateAuthor, callUploadSingleFile } from "@/config/api";
import { v4 as uuidv4 } from 'uuid';

interface IProps {
    openModal: boolean
    setOpenModal: (v: boolean) => void;
    reloadTable: () => void;
    dataInit?: IAuthor | null;
    setDataInit: (v: any) => void;
}

interface IAuthorLogo {
    name: string;
    uid: string;
}

interface IAuthorForm {
    name: string;
    description: string;
}

const modalAuthor = (props: IProps) => {

    const { openModal, setOpenModal, reloadTable, dataInit, setDataInit } = props;

    const [form] = Form.useForm();
    const [loadingUpload, setLoadingUpload] = useState<boolean>(false);
    const [dataLogo, setDataLogo] = useState<IAuthorLogo[]>([]);
    const [previewOpen, setPreviewOpen] = useState(false);
    const [previewImage, setPreviewImage] = useState('');
    const [previewTitle, setPreviewTitle] = useState('');
    const [value, setValue] = useState<string>("");

    useEffect(() => {
        if (dataInit?.id && dataInit?.description) {
            setValue(dataInit.description);
        }
        if (dataInit?.id && dataInit?.logo) {
            setDataLogo([{
                name: dataInit?.logo,
                uid: uuidv4()
            }])
        }
    }, [dataInit])

    const handleReset = () => {
        form.resetFields();
        setDataInit(null)
        setOpenModal(false)
        setValue("");
    }

    const handleRemoveFile = (file: any) => {
        setDataLogo([])
    }

    const handlePreview = async (file: any) => {
        if (!file.originFileObj) {
            setPreviewImage(file.url);
            setPreviewOpen(true);
            setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf('/') + 1));
            return;
        }
        getBase64(file.originFileObj, (url: string) => {
            setPreviewImage(url);
            setPreviewOpen(true);
            setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf('/') + 1));
        });
    };

    const getBase64 = (img: any, callback: any) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    };

    const beforeUpload = (file: any) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
        }
        return isJpgOrPng && isLt2M;
    };

    const handleChange = (info: any) => {
        if (info.file.status === 'uploading') {
            setLoadingUpload(true);
        }
        if (info.file.status === 'done') {
            setLoadingUpload(false);
        }
        if (info.file.status === 'error') {
            setLoadingUpload(false);
            message.error(info?.file?.error?.event?.message ?? "Đã có lỗi xảy ra khi upload file.")
        }
    };

    const handleUploadFileLogo = async ({ file, onSuccess, onError }: any) => {
        const res = await callUploadSingleFile(file, "author");
        if (res && res.data) {
            setDataLogo([{
                name: res.data.fileName,
                uid: uuidv4()
            }])
            if (onSuccess) onSuccess('ok')
        } else {
            if (onError) {
                setDataLogo([])
                const error = new Error(res.message);
                onError({ event: error });
            }
        }
    };

    const submitAuthor = async (valuesForm: IAuthorForm) => {
        const { name, description } = valuesForm;

        if (dataLogo.length === 0) {
            message.error('Vui lòng upload ảnh Logo')
            return;
        }

        if (dataInit?.id) {
            const author: IAuthor = {
                id: dataInit.id, name, description: value, logo: dataLogo[0].name
            }
            //update
            const res = await callUpdateAuthor(author);
            if (res.data) {
                message.success("Cập nhật author thành công");
                handleReset();
                reloadTable();
            } else {
                notification.error({
                    message: 'Có lỗi xảy ra',
                    description: res.message
                });
            }
        } else {
            const author: IAuthor = {
                name, description: value, logo: dataLogo[0].name
            }
            //create
            const res = await callCreateAuthor(author);
            if (res.data) {
                message.success("Thêm mới author thành công");
                handleReset();
                reloadTable();
            } else {
                notification.error({
                    message: 'Có lỗi xảy ra',
                    description: res.message
                });
            }
        }
    }

    return (
        <>
            <ModalForm
                title={<>{dataInit?.id ? "Cập nhật Author" : "Tạo mới Author"}</>}
                open={openModal}
                modalProps={{
                    onCancel: () => { handleReset() },
                    afterClose: () => handleReset(),
                    destroyOnClose: true,
                    width: isMobile || isTablet ? "100%" : 900,
                    footer: null,
                    keyboard: false,
                    maskClosable: false,
                    // className: `modal-company ${animation}`,
                    // rootClassName: `modal-company-root ${animation}`
                }}
                scrollToFirstError={true}
                preserve={false}
                form={form}
                onFinish={submitAuthor}
                initialValues={dataInit?.id ? dataInit : {}}
            >
                <Row gutter={16}>
                    <Col span={8}>
                        <Form.Item
                            labelCol={{ span: 24 }}
                            label="Ảnh Logo"
                            name="logo"
                            rules={[{
                                required: true,
                                message: 'Vui lòng không bỏ trống',
                                validator: () => {
                                    if (dataLogo.length > 0) return Promise.resolve();
                                    else return Promise.reject(false);
                                }
                            }]}
                        >
                            <ConfigProvider locale={enUS}>
                                <Upload
                                    name="logo"
                                    listType="picture-card"
                                    className="avatar-uploader"
                                    maxCount={1}
                                    multiple={false}
                                    customRequest={handleUploadFileLogo}
                                    beforeUpload={beforeUpload}
                                    onChange={handleChange}
                                    onRemove={(file) => handleRemoveFile(file)}
                                    onPreview={handlePreview}
                                    defaultFileList={
                                        dataInit?.id ?
                                            [
                                                {
                                                    uid: uuidv4(),
                                                    name: dataInit?.logo ?? "",
                                                    status: 'done',
                                                    url: `${import.meta.env.VITE_BACKEND_URL_UPLOAD}/storage/author/${dataInit?.logo}`,
                                                }
                                            ] : []
                                    }

                                >
                                    <div>
                                        {loadingUpload ? <LoadingOutlined /> : <PlusOutlined />}
                                        <div style={{ marginTop: 8 }}>Upload</div>
                                    </div>
                                </Upload>
                            </ConfigProvider>
                        </Form.Item>

                    </Col>
                    <Col span={16}>
                        <ProFormText
                            label="Tên tác giả"
                            name="name"
                            rules={[{ required: true, message: 'Vui lòng không bỏ trống' }]}
                            placeholder="Nhập tên tác giả"
                        />
                    </Col>
                    <ProCard
                        title="Miêu tả"
                        subTitle="mô tả thông tin tác giả"
                        headStyle={{ color: '#d81921' }}
                        style={{ marginBottom: 20 }}
                        headerBordered
                        size="small"
                        bordered
                    >
                        <Col span={24}>
                            <ReactQuill
                                theme="snow"
                                value={value}
                                onChange={setValue}
                            />
                        </Col>
                    </ProCard>
                </Row>
            </ModalForm>
            <Modal
                open={previewOpen}
                title={previewTitle}
                footer={null}
                onCancel={() => setPreviewOpen(false)}
                style={{ zIndex: 1500 }}
            >
                <img alt="example" style={{ width: '100%' }} src={previewImage} />
            </Modal>
        </>
    );
};

export default modalAuthor;