
import { IOrderDetailAD } from '@/types/backend';
import { Card, Col, Collapse, Image, Row, Tooltip } from 'antd';

const { Panel } = Collapse;

interface IProps {
    tabRef: IOrderDetailAD[]
};

const modalAPI = (props: IProps) => {

    const { tabRef } = props;
    const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }

    return (
        <Card size="small" bordered={false}>
            <Collapse>
                <Panel
                    header={<div>{"Sản phẩm đã hủy chọn"}</div>}
                    key={'sp-unchecked'}
                    forceRender
                >
                    <Row gutter={10}>
                        <Col span={15}>
                            <span
                                style={{
                                    fontSize: 16,
                                    fontWeight: 600
                                }}
                            >
                                Thông tin sản phẩm
                            </span>
                        </Col>
                        <Col span={4}>
                            <span
                                style={{
                                    fontSize: 16,
                                    fontWeight: 600
                                }}
                            >
                                Số lượng
                            </span>
                        </Col>
                        <Col span={5}>
                            <span
                                style={{
                                    fontSize: 16,
                                    fontWeight: 600
                                }}
                            >
                                Thành tiền
                            </span>
                        </Col>
                    </Row>
                    {
                        tabRef?.length > 0 &&
                        tabRef?.filter(item => item.active === false)?.map((item, index) => {
                            return (
                                <Row gutter={10} key={`index-sp-unchecked-${index}`}>
                                    <Col span={15}>

                                        <div style={{ display: 'flex', alignItems: 'center' }}>
                                            <Image
                                                width={70}
                                                src={`${import.meta.env.VITE_BACKEND_URL_UPLOAD}/storage/product/${item?.book?.image}`}
                                            />
                                            <div style={{
                                                display: "flex", flexDirection: "column"
                                            }}>
                                                {item?.book?.name}
                                            </div>
                                        </div>
                                    </Col>
                                    <Col span={4}>
                                        <div style={{ width: "100%", height: "90px", display: 'flex', alignItems: 'center' }}>
                                            {+item?.quantity}
                                        </div>
                                    </Col>
                                    <Col span={5}>
                                        <div style={{ height: "90px", display: 'flex', alignItems: 'center' }}>
                                            <span
                                                style={{
                                                    fontSize: 17,
                                                    fontWeight: 600,
                                                    color: "#ff7043"
                                                }}
                                            >
                                                {
                                                    new Intl.NumberFormat('vi-VN', config).format(+(item?.amount))
                                                }
                                            </span>
                                        </div>
                                    </Col>
                                </Row>
                            )
                        })
                    }

                </Panel>
                <Panel
                    header={<div>{"Sản phẩm đã được chọn"}</div>}
                    key={'sp-checked'}
                    forceRender //force to render form item (with collapse mode)
                >
                    <Row gutter={10}>
                        <Col span={15}>
                            <span
                                style={{
                                    fontSize: 16,
                                    fontWeight: 600
                                }}
                            >
                                Thông tin sản phẩm
                            </span>
                        </Col>
                        <Col span={4}>
                            <span
                                style={{
                                    fontSize: 16,
                                    fontWeight: 600
                                }}
                            >
                                Số lượng
                            </span>
                        </Col>
                        <Col span={5}>
                            <span
                                style={{
                                    fontSize: 16,
                                    fontWeight: 600
                                }}
                            >
                                Thành tiền
                            </span>
                        </Col>
                    </Row>
                    {
                        tabRef?.length > 0 &&
                        tabRef?.filter(item => item.active === true)?.map((item, index) => {
                            return (
                                <Row gutter={10} key={`index-sp-checked-${index}`}>
                                    <Col span={15}>

                                        <div style={{ display: 'flex', alignItems: 'center' }}>
                                            <Image
                                                width={70}
                                                src={`${import.meta.env.VITE_BACKEND_URL_UPLOAD}/storage/product/${item?.book?.image}`}
                                            />
                                            <div style={{
                                                display: "flex", flexDirection: "column"
                                            }}>
                                                {item?.book?.name}
                                            </div>
                                        </div>
                                    </Col>
                                    <Col span={4}>
                                        <div style={{ height: "90px", display: 'flex', alignItems: 'center' }}>
                                            {+item?.quantity}
                                        </div>
                                    </Col>
                                    <Col span={5}>
                                        <div style={{ height: "90px", display: 'flex', alignItems: 'center' }}>
                                            <span
                                                style={{
                                                    fontSize: 17,
                                                    fontWeight: 600,
                                                    color: "#ff7043"
                                                }}
                                            >
                                                {
                                                    new Intl.NumberFormat('vi-VN', config).format(+(item?.amount))
                                                }
                                            </span>
                                        </div>
                                    </Col>
                                </Row>
                            )
                        })
                    }
                </Panel>
            </Collapse>
        </Card>
    );
};

export default modalAPI;