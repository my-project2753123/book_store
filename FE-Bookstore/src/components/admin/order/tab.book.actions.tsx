import { MutableRefObject, useEffect, useMemo, useRef, useState } from "react";
import { IProduct, IRole } from "@/types/backend";
import { ActionType, ProColumns } from "@ant-design/pro-components";
import DataTable from "@/components/admin/share/data-table";
import Access from "@/components/share/protect.route/permission.route";
import { ALL_PERMISSIONS } from "@/config/permission";
import { Col, Divider, Popconfirm, Space } from "antd";
import InputNumber from "rc-input-number";
import 'styles/input.number.less'
import { DeleteOutlined } from "@ant-design/icons";

interface IProps {
    tabRef: any[]
    settabRef: (v: any[]) => void
}

const tabBookActions = (props: IProps) => {

    const { tabRef, settabRef } = props;
    const tableRef = useRef<ActionType>();
    const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }

    const columns: ProColumns<IProduct>[] = [
        {
            title: 'Id',
            dataIndex: 'id',
            render: (text, record, index, action) => {
                return (
                    <span>
                        {record.id}
                    </span>
                )
            },
            hideInSearch: true,
        },
        {
            title: 'Name',
            dataIndex: 'name',
            sorter: true,
            hideInSearch: true,
        },
        {
            title: 'quantity purchased',
            dataIndex: 'quantityPurchased',
            sorter: true,
            render(dom, entity, index, action, schema) {
                return (
                    <InputNumber
                        style={{ width: 100 }}
                        onChange={(val) => onChange(val, entity)}
                        min={1}
                        max={+entity?.quantity}
                        defaultValue={entity.quantityPurchased}
                    />
                )
            },
            valueType: "digit",
            hideInSearch: true,
        },
        {
            title: 'Price',
            dataIndex: 'price',
            sorter: true,
            render(dom, entity, index, action, schema) {
                let price = entity?.prices.filter(item => item.active === true)
                    .map(item => item.price).toString();
                const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }
                const formated = new Intl.NumberFormat('vi-VN', config).format(+price);
                return <p key={`key-price-${index}`}>
                    {
                        formated
                    }
                </p>
            },
            valueType: "digit",
            hideInSearch: true,
        },
        {

            title: 'Actions',
            hideInSearch: true,
            width: 50,
            render: (_value, entity, _index, _action) => (
                <Space>
                    <Access
                        permission={ALL_PERMISSIONS.ORDER.DELETE}
                        hideChildren
                    >
                        <Popconfirm
                            placement="leftTop"
                            title={"Xác nhận xóa book"}
                            description={"Bạn có chắc chắn muốn xóa book này ?"}
                            onConfirm={() => handleDeleteBook(entity.id)}
                            okText="Xác nhận"
                            cancelText="Hủy"
                        >
                            <span style={{ cursor: "pointer", margin: "0 10px" }}>
                                <DeleteOutlined
                                    style={{
                                        fontSize: 20,
                                        color: '#ff4d4f',
                                    }}
                                />
                            </span>
                        </Popconfirm>
                    </Access>
                </Space>
            ),

        },
    ];

    const handleDeleteBook = (id: any) => {
        if (tabRef?.length > 0) {
            const cptabref = [...tabRef]

            let index = cptabref.filter(items => items.id !== id)
            settabRef(index)
            tableRef?.current?.reload();
        }
    }

    const onChange = (val: any, item: IProduct) => {
        if (tabRef?.length > 0) {
            const cptabref = [...tabRef]

            let index = cptabref.findIndex(items => items.id === item.id)
            if (index !== -1) {
                cptabref[index].quantityPurchased = val;
            }
            settabRef(cptabref)

        }

    }

    return (
        <div>
            {
                useMemo(() => (
                    <>
                        <Access
                            permission={ALL_PERMISSIONS.ORDER.GET_PAGINATE}
                            hideChildren
                        >
                            <DataTable<IProduct>
                                actionRef={tableRef}
                                headerTitle="Danh sách sản phẩm đã chọn"
                                rowKey="id"
                                columns={columns}
                                dataSource={tabRef}
                                scroll={{ x: true }}
                                search={false}
                                pagination={
                                    {
                                        defaultPageSize: 10, showSizeChanger: true
                                    }
                                }
                            />
                        </Access>

                        <Col span={24}>
                            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                <span>Tạm tính</span>
                                <span>
                                    {
                                        new Intl.NumberFormat('vi-VN', config).format(
                                            +tabRef.reduce((sum, item) =>
                                                +sum + (+item.prices.filter((item: any) => item.active === true)
                                                    .map((item: any) => item.price) *
                                                    +item.quantityPurchased as number), 0)
                                        )
                                    }
                                </span>
                            </div>
                            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                <span>Giảm giá</span>
                                <span>
                                    {
                                        new Intl.NumberFormat('vi-VN', config).format(
                                            0
                                        )
                                    }
                                </span>
                            </div>
                            <Divider />
                            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                <span>Tổng tiền</span>
                                <span>
                                    <span
                                        style={{
                                            fontSize: 17,
                                            fontWeight: 600,
                                            color: "#ff7043"
                                        }}
                                    >
                                        {
                                            new Intl.NumberFormat('vi-VN', config).format(
                                                +tabRef.reduce((sum, item) =>
                                                    +sum + (+item.prices.filter((item: any) => item.active === true)
                                                        .map((item: any) => item.price) *
                                                        +item.quantityPurchased as number), 0)
                                                - +tabRef.reduce((sum, item) =>
                                                    +sum + (+item.prices.filter((item: any) => item.active === true)
                                                        .map((item: any) => item.price) *
                                                        +item.quantityPurchased as number), 0) * 0
                                            )
                                        }
                                    </span>
                                </span>
                            </div>
                        </Col>
                    </>
                ), [tabRef])
            }

        </div>
    );
};

export default tabBookActions;