import { FooterToolbar, ModalForm, ProCard, ProForm, ProFormSelect, ProFormSwitch, ProFormText, ProFormTextArea } from '@ant-design/pro-components';
import { Col, ConfigProviderProps, Divider, Form, RadioChangeEvent, Row, Tabs, message, notification } from 'antd';
import { useEffect, useRef, useState } from 'react';
import { isMobile, isTablet } from 'react-device-detect';
import TabBookActions from './tab.book.actions';
import TabListBook from './tab.listbook';
import { callCreateOrderAD, callFetchAllUser, callFetchProductById, callUpdateOrderAD } from '@/config/api';
import { IOrder, IOrderAD, IOrderDetail } from '@/types/backend';
import { DebounceSelect } from '../users/debouce.select';

interface IProps {
    openModal: boolean
    setOpenModal: (v: boolean) => void;
    reloadTable: () => void;
    dataInit?: IOrderAD | null;
    setDataInit: (v: any) => void;
}

export interface ISelect {
    label: string;
    value: string;
    key?: string;
}

type SizeType = ConfigProviderProps['componentSize'];

export const waitTime = (time: number = 100) => {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve(true);
        }, time);
    });
};

const modalOrder = (props: IProps) => {

    const { openModal, setOpenModal, reloadTable, dataInit, setDataInit } = props;
    const [user, setuser] = useState<ISelect>();
    const [tabRef, settabRef] = useState<any[]>([]);

    const [size, setSize] = useState<SizeType>('small');

    const [form] = Form.useForm();

    useEffect(() => {
        if (dataInit?.user) {
            setuser(
                {
                    label: dataInit?.user?.email as string,
                    value: dataInit?.user?.id as string,
                }
            )
        }
        if (dataInit?.details && dataInit?.details?.length > 0) {
            const fetchAllBooks = async () => {
                const arr = await Promise.all(dataInit.details.map(async item => {
                    const objapi = await fetchBookById(item.book.id);
                    const obj = { ...objapi, quantityPurchased: item.quantity };
                    return obj;
                }));

                settabRef(arr)
            };

            fetchAllBooks();
        }
    }, [dataInit])

    const fetchBookById = async (id: string) => {
        const res = await callFetchProductById(id);
        if (res?.data) {
            const obj = {
                id: res?.data?.id,
                name: res?.data?.name,
                prices: res?.data?.prices
            }
            return obj;
        }
    }

    const submitOrder = async (valuesForm: any) => {

        const { user, status, address, payment, active, discount } = valuesForm;

        if (tabRef?.length > 0) {
            if (!dataInit?.id) {
                let detailsArr: IOrderDetail[] = tabRef?.map(item => {
                    return {
                        quantity: item.quantityPurchased as number,
                        book: {
                            id: item.id as string
                        }
                    }
                })

                const obj: IOrder = {
                    user: { id: user?.value },
                    status: status,
                    address: address,
                    payment: payment,
                    details: detailsArr,
                    discount: discount,
                    active: active
                }
                const res = await callCreateOrderAD(obj);
                if (res?.data) {
                    message.success("Thêm mới order thành công");
                    handleReset();
                    reloadTable();
                    settabRef([])
                } else {
                    notification.error({
                        message: 'Có lỗi xảy ra',
                        description: res.message
                    });
                }
            } else {
                let detailsArr: IOrderDetail[] = tabRef?.map(item => {
                    return {
                        quantity: item.quantityPurchased as number,
                        book: {
                            id: item.id as string
                        }
                    }
                })

                const obj: IOrder = {
                    id: dataInit?.id,
                    user: { id: user?.value },
                    status: status,
                    address: address,
                    payment: payment,
                    details: detailsArr,
                    discount: discount,
                    active: active
                }
                const res = await callUpdateOrderAD(obj);
                if (res?.data) {
                    message.success("Cập nhật order thành công");
                    handleReset();
                    reloadTable();
                    settabRef([])
                } else {
                    notification.error({
                        message: 'Có lỗi xảy ra',
                        description: res.message
                    });
                }
            }

        }
    }

    const handleReset = async () => {
        form.resetFields();
        setOpenModal(false);
        setDataInit(null);
        settabRef([]);
    }

    const fetchUserList = async (name: string): Promise<ISelect[]> => {
        const res = await callFetchAllUser(`page=1&size=100&name=/${name}/i`);
        if (res && res.data) {
            const list = res.data.result;
            const temp = list.map(item => {
                return {
                    label: item.email as string,
                    value: item.id as string
                }
            })
            return temp;
        } else return [];
    }


    return (
        <>
            <ModalForm
                title={<>{dataInit?.id ? "Cập nhật Order" : "Thêm mới Order"}</>}
                open={openModal}
                modalProps={{
                    onCancel: () => { handleReset() },
                    afterClose: () => handleReset(),
                    destroyOnClose: true,
                    width: isMobile || isTablet ? "100%" : 900,
                    keyboard: false,
                    maskClosable: false,

                }}
                scrollToFirstError={true}
                preserve={false}
                form={form}
                onFinish={submitOrder}
                initialValues={dataInit?.id ? dataInit : {}}
            >
                <Row gutter={16}>
                    <Col lg={12} md={12} sm={24} xs={24}>
                        <ProForm.Item
                            name="user"
                            label="Khách hàng"
                            rules={[{ required: true, message: 'Vui lòng chọn khách hàng!' }]}
                        >
                            <DebounceSelect
                                disabled={dataInit?.id ? true : false}
                                allowClear
                                showSearch
                                defaultValue={user}
                                value={user}
                                placeholder="Chọn công vai trò"
                                fetchOptions={fetchUserList}
                                onChange={(newValue: any) => {
                                    if (newValue?.length === 0 || newValue?.length === 1) {
                                        setuser(newValue as ISelect);
                                    }
                                }}
                                style={{ width: '100%' }}
                            />
                        </ProForm.Item>
                    </Col>
                    <Col lg={12} md={12} sm={24} xs={24}>
                        <ProFormSwitch
                            label="Trạng thái"
                            name="active"
                            checkedChildren="ACTIVE"
                            unCheckedChildren="INACTIVE"
                            fieldProps={{
                                defaultChecked: true,
                            }}
                        />
                    </Col>
                    <Col lg={12} md={12} sm={24} xs={24}>
                        <ProFormText
                            label="Địa chỉ"
                            name="address"
                            rules={[
                                { required: true, message: 'Vui lòng không bỏ trống' },
                            ]}
                        />
                    </Col>
                    <Col lg={12} md={12} sm={24} xs={24}>
                        <ProFormText
                            label="Giảm giá"
                            name="discount"
                            placeholder="Nhập mã giảm giá"
                        />
                    </Col>
                    <Col lg={12} md={12} sm={24} xs={24}>
                        <ProFormSelect
                            name="status"
                            label="Trạng thái"
                            valueEnum={{
                                PAID: 'PAID',
                                UNPAID: 'UNPAID',
                            }}
                            placeholder="chọn trạng thái hóa đơn"
                            rules={[{ required: true, message: 'trạng thái không bỏ trống' }]}
                        />
                    </Col>
                    <Col lg={12} md={12} sm={24} xs={24}>
                        <ProFormSelect
                            name="payment"
                            label="Phương thức thanh toán"
                            valueEnum={{
                                TRANSFER: 'TRANSFER',
                                DIRECT: 'DIRECT',
                            }}
                            placeholder="chọn phương thức thanh toán"
                            rules={[{ required: true, message: 'phương thức thanh toán không bỏ trống' }]}
                        />
                    </Col>
                    <Col span={24}>
                        <ProCard
                            title="Sản phẩm"
                            subTitle="Các sản phẩm muốn thêm"
                            headStyle={{ color: '#d81921' }}
                            style={{ marginBottom: 20 }}
                            headerBordered
                            size="small"
                            bordered
                        >
                            <Tabs
                                defaultActiveKey="1"
                                type="card"
                                size={size}
                                destroyInactiveTabPane={true}
                                items={new Array(2).fill(null).map((_, i) => {
                                    const id = String(i + 1);
                                    return {
                                        label: +id === 1 ? "Sản phẩm đã chọn" : "Danh sách sản phẩm",
                                        key: id,
                                        children: +id === 1
                                            ?
                                            <TabBookActions
                                                tabRef={tabRef}
                                                settabRef={settabRef}
                                            />
                                            :
                                            <TabListBook
                                                tabRef={tabRef}
                                                settabRef={settabRef}
                                            />
                                    };
                                })}
                            />
                        </ProCard>
                    </Col>
                </Row>
            </ModalForm>
        </>
    )
};

export default modalOrder;