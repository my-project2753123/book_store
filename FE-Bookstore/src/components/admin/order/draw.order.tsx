import { IOrderAD, IOrderDetailAD } from "@/types/backend";
import { DrawerForm, ProCard, ProForm, ProFormSelect, ProFormSwitch, ProFormText } from "@ant-design/pro-components";
import { Col, Divider, Form, Row } from "antd";
import { useEffect, useState } from "react";
import { DebounceSelect } from "../users/debouce.select";
import { callFetchAllUser, callFetchOrderDetailByOrder, callFetchProductById } from "@/config/api";
import ModalAPI from "./modal.api";

interface IProps {
    onClose: (v: boolean) => void;
    open: boolean;
    dataInit: IOrderAD | null;
    setDataInit: (v: IOrderAD | null) => void;
}

export interface ISelect {
    label: string;
    value: string;
    key?: string;
}
const drawOrder = (props: IProps) => {
    const { onClose, open, dataInit, setDataInit } = props;
    const [form] = Form.useForm();
    const [tabRef, settabRef] = useState<IOrderDetailAD[]>([]);
    const [user, setuser] = useState<ISelect>();
    const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }

    useEffect(() => {
        if (dataInit?.user) {
            setuser(
                {
                    label: dataInit?.user?.email as string,
                    value: dataInit?.user?.id as string,
                }
            )
        }
        if (dataInit?.id) {
            fetchOrderDetailByOrder();
        }
    }, [dataInit])

    const fetchOrderDetailByOrder = async () => {
        const res = await callFetchOrderDetailByOrder(dataInit?.id as string);
        if (res?.data) {
            settabRef(res?.data)
        }
    }

    const handleOnclose = () => {
        form.resetFields();
        setDataInit(null);
        onClose(false);
    }

    const fetchUserList = async (name: string): Promise<ISelect[]> => {
        const res = await callFetchAllUser(`page=1&size=100&name=/${name}/i`);
        if (res && res.data) {
            const list = res.data.result;
            const temp = list.map(item => {
                return {
                    label: item.email as string,
                    value: item.id as string
                }
            })
            return temp;
        } else return [];
    }

    return (
        <DrawerForm
            title="Thông tin order"
            open={open}
            submitter={false}
            drawerProps={{
                destroyOnClose: true,
                onClose: () => handleOnclose(),
                footer: null,
                keyboard: false,
                maskClosable: false,
            }}
            form={form}
            autoFocusFirstInput
            initialValues={dataInit?.id ? dataInit : {}}
            preserve={false}
        >
            <Row gutter={16}>
                <Col lg={12} md={12} sm={24} xs={24}>
                    <ProForm.Item
                        name="user"
                        label="Khách hàng"
                        rules={[{ required: true, message: 'Vui lòng chọn khách hàng!' }]}
                    >
                        <DebounceSelect
                            disabled={dataInit?.id ? true : false}
                            allowClear
                            showSearch
                            defaultValue={dataInit?.user?.email}
                            placeholder="Chọn công vai trò"
                            fetchOptions={fetchUserList}
                            onChange={(newValue: any) => {
                                if (newValue?.length === 0 || newValue?.length === 1) {
                                    setuser(newValue as ISelect);
                                }
                            }}
                            style={{ width: '100%' }}
                        />
                    </ProForm.Item>
                </Col>
                <Col lg={12} md={12} sm={24} xs={24}>
                    <ProFormSwitch
                        label="Trạng thái"
                        name="active"
                        checkedChildren="ACTIVE"
                        unCheckedChildren="INACTIVE"
                        fieldProps={{
                            defaultChecked: true,
                        }}
                        disabled={dataInit?.id ? true : false}
                    />
                </Col>
                <Col lg={12} md={12} sm={24} xs={24}>
                    <ProFormText
                        label="Địa chỉ"
                        name="address"
                        rules={[
                            { required: true, message: 'Vui lòng không bỏ trống' },
                        ]}
                        disabled={dataInit?.id ? true : false}
                    />
                </Col>
                <Col lg={12} md={12} sm={24} xs={24}>
                    <ProFormText
                        label="Giảm giá"
                        name="discount"
                        placeholder="Nhập mã giảm giá"
                        disabled={dataInit?.id ? true : false}
                    />
                </Col>
                <Col lg={12} md={12} sm={24} xs={24}>
                    <ProFormSelect
                        name="status"
                        label="Trạng thái"
                        valueEnum={{
                            PAID: 'PAID',
                            UNPAID: 'UNPAID',
                        }}
                        placeholder="chọn trạng thái hóa đơn"
                        disabled={dataInit?.id ? true : false}
                        rules={[{ required: true, message: 'trạng thái không bỏ trống' }]}
                    />
                </Col>
                <Col lg={12} md={12} sm={24} xs={24}>
                    <ProFormSelect
                        name="payment"
                        label="Phương thức thanh toán"
                        valueEnum={{
                            TRANSFER: 'TRANSFER',
                            DIRECT: 'DIRECT',
                        }}
                        disabled={dataInit?.id ? true : false}
                        placeholder="chọn phương thức thanh toán"
                        rules={[{ required: true, message: 'phương thức thanh toán không bỏ trống' }]}
                    />
                </Col>
                <Col span={24}>
                    <ProCard
                        title="Sản phẩm"
                        subTitle="Các sản phẩm đã chọn"
                        headStyle={{ color: '#d81921' }}
                        style={{ marginBottom: 20 }}
                        headerBordered
                        size="small"
                        bordered
                    >
                        <ModalAPI
                            tabRef={tabRef}
                        />
                    </ProCard>
                </Col>
                <Col span={24}>
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                        <span>Tạm tính</span>
                        <span>
                            {
                                new Intl.NumberFormat('vi-VN', config).format(
                                    dataInit?.totalAmount ?? 0
                                )
                            }
                        </span>
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                        <span>Giảm giá</span>
                        <span>
                            {
                                new Intl.NumberFormat('vi-VN', config).format(
                                    dataInit?.discount ?? 0
                                )
                            }
                        </span>
                    </div>
                    <Divider />
                    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                        <span>Tổng tiền</span>
                        <span>
                            <span
                                style={{
                                    fontSize: 17,
                                    fontWeight: 600,
                                    color: "#ff7043"
                                }}
                            >
                                {
                                    new Intl.NumberFormat('vi-VN', config).format(
                                        dataInit?.totalAmountPlayable ?? 0
                                    )
                                }
                            </span>
                        </span>
                    </div>
                </Col>
            </Row>
        </DrawerForm>
    );
};

export default drawOrder;