import { IProduct, IRole } from "@/types/backend";
import { ActionType, ProColumns } from "@ant-design/pro-components";
import queryString from "query-string";
import { MutableRefObject, useMemo, useRef, useState } from "react";
import { sfLike } from "spring-filter-query-builder";
import { Popconfirm, Space, Table } from "antd";
import DataTable from "@/components/admin/share/data-table";
import { useAppDispatch, useAppSelector } from "@/redux/hook";
import Access from "@/components/share/protect.route/permission.route";
import { ALL_PERMISSIONS } from "@/config/permission";
import { fetchProduct } from "@/redux/slide/productSlide";
import InputNumber from "rc-input-number";
import 'styles/input.number.less'

interface IProps {
    tabRef: any[]
    settabRef: (v: any[]) => void
}

const tabListBook = (props: IProps) => {

    const { tabRef, settabRef } = props;
    const tableRef = useRef<ActionType>();

    const isFetching = useAppSelector(state => state.product.isFetching);
    const meta = useAppSelector(state => state.product.meta);
    const books = useAppSelector(state => state.product.result).map(item => {
        if (tabRef?.length > 0) {
            const cptabref = [...tabRef]
            let index = tabRef.findIndex(items => items.id === item?.id)
            if (index !== -1) {
                return {
                    ...item,
                    quantityPurchased: cptabref[index].quantityPurchased
                }
            } else {
                return {
                    ...item,
                    quantityPurchased: 1
                };
            }
        }
        return {
            ...item,
            quantityPurchased: 1
        };
    });
    const dispatch = useAppDispatch();

    const columns: ProColumns<IProduct>[] = [
        {
            title: 'Id',
            dataIndex: 'id',
            render: (text, record, index, action) => {
                return (
                    <span>
                        {record.id}
                    </span>
                )
            },
            hideInSearch: true,
        },
        {
            title: 'Name',
            dataIndex: 'name',
            sorter: true,
        },
        {
            title: 'Quantity',
            dataIndex: 'quantity',
            sorter: true,
            hideInSearch: true,
        },
        {
            title: 'Price',
            dataIndex: 'price',
            sorter: true,
            render(dom, entity, index, action, schema) {
                let price = entity?.prices.filter(item => item.active === true)
                    .map(item => item.price).toString();
                const config = { style: 'currency', currency: 'VND', maximumFractionDigits: 9 }
                const formated = new Intl.NumberFormat('vi-VN', config).format(+price);
                return <p key={`key-price-${index}`}>
                    {
                        formated
                    }
                </p>
            },
            valueType: "digit",
            hideInSearch: true,
        },
        {
            title: 'quantity purchased',
            dataIndex: 'quantityPurchased',
            sorter: true,
            render(dom, entity, index, action, schema) {
                return (
                    <InputNumber
                        style={{ width: 100 }}
                        onChange={(val) => onChange(val, entity)}
                        min={1}
                        max={+entity?.quantity}
                        defaultValue={entity.quantityPurchased}
                    />
                )
            },
            valueType: "digit",
            hideInSearch: true,
        },

    ];

    const onChange = (val: any, item: IProduct) => {
        if (tabRef?.length > 0) {
            const cptabref = [...tabRef]

            let index = cptabref.findIndex(items => items.id === item.id)
            if (index !== -1) {
                cptabref[index].quantityPurchased = val;
            }

            settabRef(cptabref)
        } else {
            let index = books.findIndex(items => items.id === item.id)
            if (index !== -1) {
                books[index].quantityPurchased = val;
            }
        }

    }

    const buildQuery = (params: any, sort: any, filter: any) => {
        const q: any = {
            page: params.current,
            size: params.pageSize,
            filter: ""
        }

        const clone = { ...params };
        if (clone.name) q.filter = `${sfLike("name", clone.name)}`;

        if (!q.filter) delete q.filter;
        let temp = queryString.stringify(q);

        let sortBy = "";
        if (sort && sort.name) {
            sortBy = sort.name === 'ascend' ? "sort=name,asc" : "sort=name,desc";
        }
        if (sort && sort.createdAt) {
            sortBy = sort.createdAt === 'ascend' ? "sort=createdAt,asc" : "sort=createdAt,desc";
        }
        if (sort && sort.updatedAt) {
            sortBy = sort.updatedAt === 'ascend' ? "sort=updatedAt,asc" : "sort=updatedAt,desc";
        }

        //mặc định sort theo updatedAt
        if (Object.keys(sortBy).length === 0) {
            temp = `${temp}&sort=updatedAt,desc`;
        } else {
            temp = `${temp}&${sortBy}`;
        }

        return temp;
    }

    return (
        <div>
            <Access
                permission={ALL_PERMISSIONS.ORDER.GET_PAGINATE}
                hideChildren
            >
                {
                    useMemo(() => (
                        <DataTable<IProduct>
                            actionRef={tableRef}
                            headerTitle="Danh sách Sản phẩm"
                            rowKey="id"
                            loading={isFetching}
                            columns={columns}
                            dataSource={books}
                            request={async (params, sort, filter): Promise<any> => {
                                const query = buildQuery(params, sort, filter);
                                dispatch(fetchProduct({ query }))
                            }}
                            scroll={{ x: true }}
                            pagination={
                                {
                                    current: meta?.page,
                                    pageSize: meta?.pageSize,
                                    showSizeChanger: true,
                                    total: meta?.total,
                                    showTotal: (total, range) => { return (<div> {range[0]}-{range[1]} trên {total} rows</div>) }
                                }
                            }

                            rowSelection={{
                                selections: [Table.SELECTION_ALL, Table.SELECTION_INVERT],
                                defaultSelectedRowKeys: tabRef?.map(item => item.id) ?? [],
                                getCheckboxProps: (record: IProduct) => ({
                                    disabled: tabRef?.map(item => item.id).includes(record.id),
                                }),
                                onChange: (selectedRowKeys, selectedRows) => {
                                    const newSelect = selectedRows.map(item => {
                                        return {
                                            ...item,
                                            quantityPurchased: item.quantityPurchased ? item.quantityPurchased : 1
                                        }
                                    })

                                    const updateRef = [...tabRef];
                                    newSelect.forEach(newItem => {
                                        const index = updateRef.findIndex(oldItem => oldItem.id === newItem.id);
                                        if (index !== -1) {
                                            updateRef[index] = newItem;
                                        } else {
                                            updateRef.push(newItem);
                                        }
                                    });

                                    settabRef(updateRef)
                                },
                            }}
                        />
                    ), [books])
                }
            </Access>
        </div>
    );
};

export default tabListBook;