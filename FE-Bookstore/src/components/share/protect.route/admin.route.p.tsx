import { useAppDispatch, useAppSelector } from '@/redux/hook';
import { fetchAccount } from '@/redux/slide/authSlide';
import React, { useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import Loading from '../loading';

interface IProps {
    children: React.ReactNode
}

const adminPRoute = (props: IProps) => {

    const dispatch = useAppDispatch();
    const auth = useAppSelector((state) => state.account)

    useEffect(() => {
        if (window.location.pathname === '/login'
            || window.location.pathname === '/register'
        )
            return;
        dispatch(fetchAccount())
    }, [])

    if (auth.isLoading === true) {
        return (
            <Loading />
        )
    }
    else {
        if (auth && auth.isAuthenticated === true) {
            return (
                <>
                    {props.children}
                </>
            );
        }
        else {
            return <Navigate to="/login" replace />
        }
    }

};

export default adminPRoute;