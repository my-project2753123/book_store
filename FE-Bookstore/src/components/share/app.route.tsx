import Login from '@/pages/auth/Login';
import Register from '@/pages/auth/Register';
import { Route, Routes } from 'react-router-dom';
import LayoutAdmin from 'components/admin/layout.admin';
import UserAdmin from '@/pages/admin/user.admin';
import RoleAdmin from '@/pages/admin/role.admin';
import PermissionAdmin from '@/pages/admin/permission.admin';
import AdminPRoute from 'components/share/protect.route/admin.route.p';
import DashboardAdmin from '@/pages/admin/dashboard.admin';
import AuthorProducts from '@/pages/admin/products/author.products';
import BooksProducts from '@/pages/admin/products/books.products';
import PublisherProducts from '@/pages/admin/products/publisher.products';
import LayoutClient from 'components/client/layout.client';
import HomeClient from '@/pages/client/home/home.client';
import BookDetail from '@/pages/client/book/detail';
import CartIndex from '@/pages/client/cart';
import OrderAdmin from '@/pages/admin/order.admin';
import Revenue from '@/pages/admin/statistical/revenue';
import AccountClient from '@/pages/client/account';
import OrderIndex from '@/pages/client/order/order.index';


const AppRoutes = () => {
    return (
        <Routes>
            <Route path='/login' element={<Login />} />
            <Route path='/register' element={<Register />} />

            <Route path='/admin' element={<AdminPRoute><LayoutAdmin /></AdminPRoute>}>
                <Route path='user' element={<UserAdmin />} />
                <Route path='order' element={<OrderAdmin />} />
                <Route path='role' element={<RoleAdmin />} />
                <Route path='permission' element={<PermissionAdmin />} />
                <Route path='product/author' element={<AuthorProducts />} />
                <Route path='product/books' element={<BooksProducts />} />
                <Route path='product/publisher' element={<PublisherProducts />} />
                <Route path='statistical/revenue' element={<Revenue />} />
                <Route path='' element={<DashboardAdmin />} />
            </Route>

            <Route path='' element={<LayoutClient />}>
                <Route path='' element={<HomeClient />} />
                <Route path='book/:id' element={<BookDetail />} />
                <Route path='cart' element={<CartIndex />} />
                <Route path='account' element={<AdminPRoute><AccountClient /></AdminPRoute>} />
                <Route path='order' element={<AdminPRoute><OrderIndex /></AdminPRoute>} />
            </Route>

        </Routes>
    );
};

export default AppRoutes;